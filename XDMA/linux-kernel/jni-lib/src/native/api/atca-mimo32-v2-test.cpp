/*
 * atca-mimo32-v2-test.cpp
 *
 * Copyright (c) 2020-present,  IPFN-IST / IPP-MPG
 * All rights reserved.
 *
 * This source code is licensed under BSD-style license (found in the
 * LICENSE file in the root directory of this source tree)
 */


#include <atomic>
#include <csignal> /* sigaction */
#include <cstdio>  /* for printf */
#include <cstring> /* strerror*/
#include <getopt.h> /* getopt */
#include <iostream>
#include <string>
#include <time.h>

#include "atca-mimo32-v2-device.h"

namespace atca_test
{

    int numberOfAcquisitionRounds = 0;
    int deviceNumber = 0;
    int dataMbSize = 128;
    int dmaBufferSize = DMA_SIZE_DEFAULT;
    bool softTrigger = false;
    unsigned int chopperPeriod = 2000;
    std::string outputFileName = "dataDMA";
    uint32_t channelMask = 0xFFFFFFFF;
    int nSamples = 32768; //65536;
    int holdSamples = -1;
    int timeoutMiliseconds = 5000;
    bool use32bits = false;
    bool interlockMode = false;
    int32_t electricalOffsets[ADC_CHANNELS] = {0};
    float wiringOffsets[ADC_CHANNELS] = {0};
    float iparameters[ILOCK_PARAMS] = {0};
    bool decoupleData = false;
    bool binaryFile = false;
    float temperaturesTMP101[TMP101_SENSORS];
    float temperaturesMCP9808[MCA9808_SENSORS];
    FILE* outputBinFile = NULL;

    std::atomic_bool endReadLoop(false);
    void sigint_handler(int signo)
    {
        endReadLoop = true;
    }

    void writeBinaryFile(FILE* outputFile, uint8_t* dataBuffer, uint64_t size)
    {
        // Write all the channel data, in binary format
        fwrite(dataBuffer, 4096, size/4096, outputFile);
    }
    void writeDecoupledSampleToFile(FILE* outputFile, atca::AtcaMimo32V2& device, uint32_t* sampleBuffer)
    {
        // Write all the channel data, cut off interlock data
        for (int j = 0; j < 32; ++j) {
            fprintf(outputFile, "%d ", ((int32_t*)sampleBuffer)[j] & 0xFFFFF000);
        }

        fprintf(outputFile, "%u ", device.getSampleChopperPhase(sampleBuffer));
        fprintf(outputFile, "%u ", device.getSampleFifoDmaStatus(sampleBuffer));
        fprintf(outputFile, "%u ", device.getSampleFifoCounter(sampleBuffer));
        fprintf(outputFile, "%lu ", device.getSampleCounter64Bit(sampleBuffer));
        fprintf(outputFile, "%f %f %f %f", device.getSampleFloatValue(sampleBuffer, 0), device.getSampleFloatValue(sampleBuffer, 1), 
                device.getSampleFloatValue(sampleBuffer, 2), device.getSampleFloatValue(sampleBuffer, 3));
        fprintf(outputFile, "\n");
    }

    void writeSampleToFile(FILE* outputFile, uint8_t* sampleBuffer)
    {
        for (int j = 0; j < 32; ++j) {
            if(use32bits) {
                fprintf(outputFile, "%d ", ((int32_t*)sampleBuffer)[j]); // Cast to signed int instead of unsigned and write as decimal
            }
            else {
                fprintf(outputFile, "%d ", ((int16_t*)sampleBuffer)[j]);
            }
        }
        fprintf(outputFile, "\n");
    }

    int readData(atca::AtcaMimo32V2& device, FILE* outputFile)
    {
        const int numOfChannels = ADC_CHANNELS;
        uint8_t* buffer;
        size_t buffSize = nSamples * numOfChannels;
        if(use32bits) {
            buffSize *= sizeof(uint32_t);
        }
        else {
            buffSize *= sizeof(uint16_t);
        }
        buffer = (uint8_t*) malloc(buffSize);
        if(buffer == NULL) {
            std::cerr << "Failed to allocate sufficient read buffer" << std::endl;
            return EXIT_FAILURE;
        }

        int round = 0;
        int rc = 0;
        while (!endReadLoop && round < numberOfAcquisitionRounds)
        {
            round++; // Increment round at start after condition has been check for proper print
            printf("Reading round %d of %d\n", round, numberOfAcquisitionRounds);
            rc = device.read(buffer, nSamples, timeoutMiliseconds);
            if (rc != EXIT_SUCCESS) {
                std::cerr << "Failed to acquire requested number of samples." << std::endl;
                continue;
            }

            //if (binaryFile){
            //std::string outputFileBN = outputFileName + ".bin";
            //FILE* outputBinFile = fopen(outputFileBN.c_str(), "wb");
            if (outputBinFile != NULL)
                writeBinaryFile(outputBinFile, buffer, buffSize);

            // printf("Write Bin File ended.\n");
            for (int i = 0; i < nSamples; ++i) {
                if(decoupleData ) {
                    if(use32bits) {
                        // If using 32 bit acquisition and decoupling then split data from interlock float data
                        writeDecoupledSampleToFile(outputFile, device, (uint32_t*)buffer + i * numOfChannels);
                    }
                    else {
                        // Write a single sample to file without decoupling
                        writeSampleToFile(outputFile, buffer + i * numOfChannels * (use32bits ? sizeof(uint32_t) : sizeof(int16_t)));
                    }
                }
            }
        }
        rc = device.stopAcquisition();
        if (rc != EXIT_SUCCESS) {
            std::cerr << "Failed to stop acquisition: " << strerror(rc) << std::endl;
            return EXIT_FAILURE;
        }
        free(buffer);
        return EXIT_SUCCESS;
        }

        int parseStringToIntArray(std::string stringArray, int32_t* destination, int length)
        {
            std::string s = stringArray;
            size_t pos = 0;
            int i = 0;
            while ((pos = s.find(";")) != std::string::npos) {
                if(i >= length) {
                    std::cerr << "To many elements for the given array." << std::endl;
                    return EXIT_FAILURE;
                }
                destination[i] = atoi(s.substr(0, pos).c_str());
                i++;
                s.erase(0, pos + 1);
            }
            destination[i] = atoi(s.c_str()); // Get the last element
            return EXIT_SUCCESS;
        }

        int parseStringToFloatArray(std::string stringArray, float* destination, int length)
        {
            std::string s = stringArray;
            size_t pos = 0;
            int i = 0;
            while ((pos = s.find(";")) != std::string::npos) {
                if(i >= length) {
                    std::cerr << "To many elements for the given array." << std::endl;
                    return EXIT_FAILURE;
                }
                destination[i] = atof(s.substr(0, pos).c_str());
                i++;
                s.erase(0, pos + 1);
            }
            destination[i] = atoi(s.c_str()); // Get the last element
            return EXIT_SUCCESS;
        }

        int processInputs(int argc, char* argv[])
        {
            int c;
            while ((c = getopt(argc, argv, "a:b:d:e:g:k:o:m:n:t:lisw:p:xyz:h")) != -1) {
                switch (c) {
                    case 'a':
                        numberOfAcquisitionRounds = atoi(optarg);
                        break;
                    case 'd':
                        deviceNumber = atoi(optarg);
                        break;
                    case 'b':
                        dataMbSize = atoi(optarg); 
                        break;
                    case 'g':
                        holdSamples = atoi(optarg); 
                        break;
                    case 's':
                        softTrigger = true;
                        break;
                    case 'p':
                        chopperPeriod = atoi(optarg);
                        break;
                    case 'o':
                        outputFileName = optarg;
                        break;
                    case 'm':
                        channelMask = strtoul(optarg, NULL, 0);
                        break;
                    case 'n':
                        nSamples = atoi(optarg);
                        break;
                    case 't':
                        timeoutMiliseconds = atoi(optarg);
                        break;
                    case 'z':
                        dmaBufferSize = atoi(optarg);
                        break;
                    case 'l':
                        use32bits = true;
                        break;
                    case 'i':
                        interlockMode = true;
                        break;
                    case 'e':
                        if(parseStringToIntArray(optarg, electricalOffsets, ADC_CHANNELS) != EXIT_SUCCESS) {
                            std::cerr << "Error parsing electrical offsets." << std::endl;
                            exit(EXIT_FAILURE);
                        }
                        break;
                    case 'w':
                        if(parseStringToFloatArray(optarg, wiringOffsets, ADC_CHANNELS) != EXIT_SUCCESS) {
                            std::cerr << "Error parsing wiring offsets." << std::endl;
                            exit(EXIT_FAILURE);
                        }
                        break;
                    case 'k':
                        if(parseStringToFloatArray(optarg, iparameters, ILOCK_PARAMS) != EXIT_SUCCESS) {
                            std::cerr << "Error parsing interlock parameters." << std::endl;
                            exit(EXIT_FAILURE);
                        }
                        break;
                    case 'x':
                        decoupleData = true;
                        break;
                    case 'y':
                        binaryFile = true;
                        break;
                    case 'h':
                        printf("All parameters are optional. If parameter is not specified then the default values will be used.\n \
                                -a [INT] - number of acquisition rounds\n \
                                -b [INT] - buffer size in MB\n \
                                -d [INT] - device number \n \
                                -g [INT] - hold samples. (mas 63) \n \
                                -s - use software trigger\n \
                                -p [UINT] - chopper period, set to 0 to disable\n \
                                -o [STRING] - output file name\n \
                                -m [HEX] - channel mask\n \
                                -n [INT] - number of samples\n \
                                -t [LONG] - timeout miliseconds\n \
                                -l - use 32 bit acquisition\n \
                                -i - use interlock mode\n \
                                -e [INT[32]] - interlock electrical offsets\n \
                                -w [FLOAT[8]] - interlock wiring offsets\n \
                                -k [FLOAT[10]] - interlock parameters\n \
                                -x - decouple meta and interlock data\n \
                                -y - write binary file\n \
                                -z [INT] - DMA buffer size in kB\n \
                                -h - print this help\n");
                        printf("e.g. %s -a 2 -s -m 0xE -l -i -w '0.1;1.0;2.0;1.0;1.0;1.0,2.0' \
                                -k '0.0;1.0;2.0;1.0;1.0;1.0;2'\n", argv[0]);
                        exit(EXIT_SUCCESS);
                    default:
                        fprintf(stderr, "Unknown command '%s'.\n",  argv[0]);
                        abort();
                }
            }
            return EXIT_SUCCESS;
        }

        int runTest(int argc, char* argv[]) 
        {
            struct sigaction sa;
            sa.sa_handler = sigint_handler;
            sigaction(SIGINT, &sa, NULL);

            processInputs(argc, argv);

            atca::AtcaMimo32V2 device(deviceNumber, dataMbSize, dmaBufferSize);
            int rc = device.open(use32bits, chopperPeriod, channelMask);
            if (rc != EXIT_SUCCESS) {
                std::cerr << "Failed to open device: " << strerror(rc) << std::endl;
                return EXIT_FAILURE;
            }
            uint32_t version, fwTimestamp;
            rc = device.readFirmwareVersion(&version);
            if (rc != EXIT_SUCCESS) {
                std::cerr << "Failed to read FW version: " << strerror(rc) << std::endl;
                return EXIT_FAILURE;
            }
            printf("FPGA FW Version 0x%08X  ", version);
            rc = device.readFirmwareTimestamp(&fwTimestamp);
            if (rc != EXIT_SUCCESS) {
                std::cerr << "Failed to read FW Timestamp: " << strerror(rc) << std::endl;
                return EXIT_FAILURE;
            }
            printf("Timestamp %8u  ", fwTimestamp);
            if(interlockMode) {
                int rc = device.setInterlockParameters(electricalOffsets, wiringOffsets, iparameters);
                if (rc != EXIT_SUCCESS) {
                    std::cerr << "Failed to set interlock parameters: " << strerror(rc) << std::endl;
                    return EXIT_FAILURE;
                }
            }
            if(holdSamples >= 0) {
                int rc = device.setHoldSamples(holdSamples);
                if (rc != EXIT_SUCCESS) {
                    std::cerr << "Failed to set Hold Samples: " << strerror(rc) << std::endl;
                    return EXIT_FAILURE;
                }
            }
            rc = device.readIpmcReg0(&version);
            if (rc != EXIT_SUCCESS) {
                std::cerr << "Failed to read IpmcReg0: " << strerror(rc) << std::endl;
                return EXIT_FAILURE;
            }
            printf("IPMC Reg 0 0x%08X\n", version);
            uint32_t status;
            rc = device.readStatus(&status);
            if (rc != EXIT_SUCCESS) {
                std::cerr << "Failed to read FW status: " << strerror(rc) << std::endl;
                return EXIT_FAILURE;
            }
            printf("FPGA Status 0x%08X\n", status);
            if( numberOfAcquisitionRounds == 0){
                char outstr[80];
                FILE* outputBinFile = NULL;
                time_t rawtime = (time_t) fwTimestamp;
                struct tm *ptm = localtime(&rawtime);

                strftime(outstr, 80, "FW compilation date is %A, %b %d. Time:  %r", ptm);

                printf("\n%s\n", outstr);
                for(int i=0; i < 32; i++)
                    wiringOffsets[i] = 0.0;
                for(int i=0; i < 10; i++)
                    iparameters[i] = 0.0;
                rc = device.getInterlockParameters(electricalOffsets, wiringOffsets, iparameters);
                printf("Wiring: ");
                for(int i=0; i< 8; i++)
                    printf("%g, ", wiringOffsets[i]);
                printf("\nStatBits Master:%2u CLK_LOCKS ATCA:%2u TE741:%2u RTM:%2u SlotId: 0x%02X \n",
                        (status & 0x400000)>>22,
                        (status &  0x40000)>>18,
                        (status &  0x20000)>>17,
                        (status &  0x10000)>>16,
                        status & 0xCF);
                printf("ILock: ");
                for(int i=0; i< 8; i++)
                    printf("%g ", iparameters[i]);
                printf("\nchannelMask: 0x%08X\n", channelMask);
                rc = device.getTemperaturesTMP101(temperaturesTMP101);
                if(rc)
                    printf("Error getting TMP101 values\n");
                else
                    printf("TMP101 %g ºC, %g ºC\n", temperaturesTMP101[0], temperaturesTMP101[1]);

                rc = device.getTemperaturesMCA9808(temperaturesMCP9808);
                if(rc)
                    printf("Error getting MCP9808 values\n");
                else {
                    printf("MCP9808 ");
                    for(int i=0; i < 4; i++)
                        printf(" %6.2f ºC %6.2f ºC ",
                                temperaturesMCP9808[i*2],
                                temperaturesMCP9808[i*2 +1]);
                    printf("\n");
                }
                rc = device.close();
                return EXIT_SUCCESS;
            }

            rc = device.armAcquisition();
            if (rc != EXIT_SUCCESS) {
                std::cerr << "Failed to arm acquisition: " << strerror(rc) << std::endl;
                return EXIT_FAILURE;

            }

            if(softTrigger) {
                device.sendSoftwareTrigger();
            }

            std::string outputFileN;
            if(use32bits)
                outputFileN = outputFileName + "32.dat";
            else
                outputFileN = outputFileName + "16.dat";

            std::string outputFileBN = outputFileName + ".bin";
            //fopen(outputFileBN.c_str(), "wb");
            if (binaryFile){
                outputBinFile = fopen(outputFileBN.c_str(), "wb");
                if (outputBinFile == NULL) {
                    std::cerr << "Failed to open " <<  outputFileBN << " bin output file: " << strerror(errno) << std::endl;
                }
            }
            FILE* outputFile = fopen(outputFileN.c_str(), "w");
            if (outputFile != NULL) {
                bool success = readData(device, outputFile);
                if (success != false) {
                    std::cerr << "Error acquiring samples" << std::endl;
                }
                fclose(outputFile);
            }
            else {
                std::cerr << "Failed to open " <<  outputFileN << " output file: " << strerror(errno) << std::endl;
            }

            rc = device.close();
            if (rc != EXIT_SUCCESS) {
                std::cerr << "Failed to close device: " << strerror(rc) << std::endl;
                return EXIT_FAILURE;
            }
            // printf("device close OK\n");
            if (outputBinFile != NULL)
                fclose(outputBinFile);

            return EXIT_SUCCESS;
        }

    } // namespace atca_test

    int main(int argc, char* argv[])
    {
        return atca_test::runTest(argc, argv);
    }

    /*
     * vim: syntax=cpp ts=4 sw=4 sts=4 sr et 
     */
