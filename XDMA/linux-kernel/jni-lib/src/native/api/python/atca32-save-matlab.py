#!/usr/bin/env python3
# vim: sta:et:sw=4:ts=4:sts=4
"""
Extract data from binary files and save in MATLAB format
"""

import sys
import numpy as np
#from scipy.io import savemat

import scipy.io as sio
from atca_data_functions import get_data32_raw, get_data32, get_float, get_decim_float, get_interlock_status, get_count64

if len(sys.argv) > 1:
    filename = str(sys.argv[1])
else:
    filename = 'dataDMA.bin'

# SCALE_32 = 2**14 # Raw Data is shifted 14 bits right
data_raw = get_data32_raw(filename)
data, chopp = get_data32(data_raw)
#data = data_raw // SCALE_32
F = get_float(data_raw, 8)
dF = get_float(data_raw, 12)
absF = get_float(data_raw, 16)
absdF = get_float(data_raw, 20)
fDecim, idx = get_decim_float(data_raw)
count64 = get_count64(data_raw)

statusF, statusDF = get_interlock_status(data_raw)
mdic = {'data32': data, 'chopp': chopp, 'F':F, 'dF': dF,
         'statusF': statusF, 'statusDF': statusDF,
        'absF':absF, 'absdF': absdF, 'fDecim':fDecim,
        'idx':idx, 'count64': count64}

mat_fname = filename[:-4] + '.mat'
sio.savemat(mat_fname, mdic)
