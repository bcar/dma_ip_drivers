#!/usr/bin/env python3
# vim: sta:et:sw=4:ts=4:sts=4
"""
Extract embedded data from 32 bit packets
"""

import numpy as np

SCALE_32 = 2**14 # Data was shifted 14 bits left in FPGA

# ‘F’ means to readthe elements using Fortran-like index order,
# with the first index changing fastest,
# filename = self.line_edit.text()
# ‘F’ means to readthe elements using Fortran-like index order,
# with the first index changing fastest,
def get_data16(filename):
    data = np.fromfile(filename, dtype='int16')
    data_mat = np.reshape(data, (32, -1), order='F')
    return data_mat

def get_data32_raw(filename):
    data = np.fromfile(filename, dtype='int32')
    data_raw = np.reshape(data, (32, -1), order='F')
    return data_raw

#def get_data32_decim(filename):
#    data = np.fromfile(filename, dtype='int32')
#    data_decim = np.reshape(data, (32*32, -1), order='F')
#    return data_decim

# Functions to extract embedded data on 32-bit packets

def get_data32(data_raw):
    data_scaled = data_raw // SCALE_32
    chopp = data_raw[31] & 0x0001
    return data_scaled, chopp

def get_interlock_status(data_raw):
    statusF =  (data_raw[31] & 0x00000040) >> 6
    statusDF = (data_raw[31] & 0x00000080) >> 7
    return statusF, statusDF

def get_count64(data_raw):
    count = (data_raw[0] & 0xFF) | ((data_raw[1] & 0xFF) << 8)
    count = count | ((data_raw[3] & 0xFF) << 24) | ((data_raw[2] & 0xFF) << 16)
    count = count | ((data_raw[5] & 0xFF) << 40) | ((data_raw[4] & 0xFF) << 32)
    count = count | ((data_raw[7] & 0xFF) << 56) | ((data_raw[6] & 0xFF) << 48)
    return count

def get_int32(data_raw, first):
    valI  = ((data_raw[first+1] & 0xFF) << 8) | (data_raw[first] & 0xFF)
    valI |= ((data_raw[first+3] & 0xFF) << 24) | ((data_raw[first+2] & 0xFF) << 16)
    return valI

def get_float(data_raw, first):
    valFloat = ((data_raw[first+1] & 0xFF) << 8) | (data_raw[first] & 0xFF)
    valFloat = valFloat | ((data_raw[first+3] & 0xFF) << 24) | ((data_raw[first+2] & 0xFF) << 16)
    tobytes = valFloat.tobytes()
    return np.frombuffer(tobytes,dtype='f')

def get_decim_float(data_raw):
    first = 24
    valFloat = ((data_raw[first+1] & 0xFF) << 8) | (data_raw[first] & 0xFF)
    valFloat = valFloat | ((data_raw[first+3] & 0xFF) << 24) | ((data_raw[first+2] & 0xFF) << 16)
    tobytes = valFloat.tobytes()
    idxCh  = (data_raw[30] & 0x1F)
    data = np.frombuffer(tobytes,dtype='f')
    decim_float = np.reshape(data, (32, -1), order='F')
    return decim_float, idxCh[:32]

def get_decim_idx(data_raw):
    idxCh  = (data_raw[30] & 0x1F)
    return idxCh

