#!/usr/bin/env python3
# vim: sta:et:sw=4:ts=4:sts=4
"""
Extract data from binary files and save in MATLAB format
"""

import sys
import numpy as np
# import matplotlib as mpl
import matplotlib.pyplot as plt

import scipy.io as sio

if len(sys.argv) > 1:
    fname = str(sys.argv[1])
else:
    fname = 'dataDMA.mat'

# mat_fname = filename[:-4] + '.mat'
mat_data = sio.loadmat(fname)
fig, ax = plt.subplots()  # Create a figure containing a single axes.
#  ax.plot([1, 2, 3, 4], [1, 4, 2, 3])  # Plot some data on the axes.
ch = mat_data['data32']
fDecim = mat_data['fDecim']
fDecim = mat_data['fDecim']
F = mat_data['F']
F = F[0]
cnt64 = mat_data['count64']
cnt64 = cnt64[0]
mCh=np.mean(ch,1)
#mfD=np.mean(fDecim,1)
print(mCh.astype('int32'))
print('WO ')
print(fDecim[:,-1])
# ax.plot(d[0])
# plt.show()

