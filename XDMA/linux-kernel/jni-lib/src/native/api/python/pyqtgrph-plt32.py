#!/usr/bin/env python3
"""
Plot 32 bit Data
"""

import sys
import numpy as np

import pyqtgraph as pg
#from pyqtgraph.Qt import QtGui
from pyqtgraph.dockarea import *
from atca_data_functions import get_data32_raw, get_data32, get_count64
from datetime import datetime
from pyqtgraph.Qt import QtWidgets
win = pg.GraphicsLayoutWidget(show=True, title="Basic plotting examples")

# FILENAME = 'dataDMA_ch_on.bin'
if len(sys.argv) > 1:
    FILENAME = str(sys.argv[1])
else:
    FILENAME = 'dataDMA.bin'
NUMCHANNELS = 8

# DECIMATION = 20

pg.setConfigOption('background', 'w')  # before loading widget
## Always start by initializing Qt (only once per application)
#app = QtWidgets.QApplication([])
app = pg.mkQApp("Plotting 32 raw Channels")

#app = QtGui.QApplication([])


#win = QtGui.QMainWindow()
win = QtWidgets.QMainWindow()
area = DockArea()
win.setCentralWidget(area)
win.resize(1000,500)

dt = datetime.today().replace(microsecond=0)
title = 'ATCA v2 32 data. ' + str(dt)

win.setWindowTitle(title)

## Create docks, place them into the window one at a time.
## Note that size arguments are only a suggestion; docks will still have to
## fill the entire dock area and obey the limits of their internal widgets.
# d1 = Dock("Dock1", size=(1, 1))     ## give this dock the minimum possible size
d1 = Dock("Dock1", size=(500, 400) , closable=True)     ## give this dock the minimum possible size
d2 = Dock("Dock2 - Console", size=(500,400), closable=True)
d3 = Dock("Dock3", size=(500,400), closable=True)
d4 = Dock("Dock4 (tabbed) - Plot", size=(500,400), closable=True)
# d5 = Dock("Dock5 - Image", size=(500,200))
# d6 = Dock("Dock6 (tabbed) - Plot", size=(500,200))
area.addDock(d1, 'left')      ## place d1 at left edge of dock area (it will fill the whole space since there are no other docks yet)
area.addDock(d2, 'right')     ## place d2 at right edge of dock area
area.addDock(d3, 'bottom', d1)## place d3 at bottom edge of d1
area.addDock(d4, 'bottom', d2)     ## place d4 at right edge of dock area
# area.addDock(d5, 'left', d1)  ## place d5 at left edge of d1
# area.addDock(d6, 'top', d4)   ## place d5 at top edge of d4

## Test ability to move docks programatically after they have been placed
# area.moveDock(d4, 'top', d2)     ## move d4 to top edge of d2
# area.moveDock(d6, 'above', d4)   ## move d6 to stack on top of d4
# area.moveDock(d5, 'top', d2)     ## move d5 to top edge of d2

def draw_plot32(pltWdgt, data_mat, first):
    # w = pg.PlotWidget(title="Dock 2 plot")
    pltWdgt.clear()
    pltWdgt.setYRange(-32768*4, 32768*4, padding=0.01)
    pltWdgt.addLegend()
    pltWdgt.setLabels(title='ch {}-{}'.format(first, first + 7), bottom='Sample',
        left='LSB', right='Chopp')
    # data_scaled = data_mat // SCALE_32
    for i in range(NUMCHANNELS):
        pltWdgt.plot(data_mat[i + first], pen=({'color': (i, NUMCHANNELS*1.3),
            'width': 1}), name="ch{}".format(i + first))
    return


# d1.hideTitleBar()
# w = create_plot32(data_mat, 0)
w1 = pg.PlotWidget(title="Dock 2 plot")
wl = pg.LayoutWidget()
# w1 = pg.PlotWidget(title="Plot inside dock with no title bar")
saveBtn = QtWidgets.QPushButton('Draw plot')
line_edit = QtWidgets.QLineEdit(FILENAME)
wl.addWidget(w1, row=0, col=0, colspan=2)
wl.addWidget(saveBtn, row=1, col=0)
wl.addWidget(line_edit, row=1, col=1)
# w1.addWidget(restoreBtn, row=2, col=0)
d1.addWidget(wl)

# w2 = create_plot32(data_mat, 8)
w2 = pg.PlotWidget(title="Dock 2 plot")
# w1.addWidget(restoreBtn, row=2, col=0)
d2.addWidget(w2)


## Hide title bar on dock 3
# d3.hideTitleBar()
# w3 = create_plot32(data_mat, 16)
w3 = pg.PlotWidget(title="Dock 3 plot")
# w3 = pg.PlotWidget(title="Plot inside dock with no title bar")
# w3.plot(np.random.normal(size=100))
d3.addWidget(w3)
# data_mat = get_data32(line_edit)
# draw_plot32(w1, data_mat, 0)
# draw_plot32(w2, data_mat, 8)
# draw_plot32(w3, data_mat, 16)


# d4.hideTitleBar()
w4 = pg.PlotWidget(title="Dock 4 plot")
# w4 = create_plot32(data_mat, 24)

p2 = pg.ViewBox()
w4.scene().addItem(p2)
w4.getAxis('right').linkToView(p2)
p2.setXLink(w4)

pltCh = pg.PlotCurveItem([0], pen=pg.mkPen(color='k', width=2))
# pltCh = pg.PlotCurveItem((data_mat[31] & 0x01), pen=pg.mkPen(color='r', width=3))
p2.setYRange(-1, 8)

p2.addItem(pltCh)

d4.addWidget(w4)

def updateViews():
    global p2
    p2.setGeometry(w4.getViewBox().sceneBoundingRect())
    p2.linkedViewChanged(w4.getViewBox(), p2.XAxis)

def draw_all():
    global w1, w2, w3, w4, pltCh, line_edit
    filen = line_edit.text()
    data_mat = get_data32_raw(filen)
    data, chopp = get_data32(data_mat)
    draw_plot32(w1, data, 0)
    draw_plot32(w2, data, 8)
    draw_plot32(w3, data, 16)
    draw_plot32(w4, data, 24)
    #pltCh.setData((data_mat[31] & 0x0001))
    pltCh.setData(chopp)
    updateViews()
    w4.getViewBox().sigResized.connect(updateViews)

saveBtn.clicked.connect(draw_all)

draw_all()

## Add widgets into each dock

win.show()

if __name__ == '__main__':
    ## Start the Qt event loop
    pg.exec()
    #app.exec_()

# vim: syntax=python ts=4 sw=4 sts=4 sr et
