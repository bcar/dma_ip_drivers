/*
 * atca-mimo32-v2-device.cpp
 *
 * Copyright (c) 2016-present,  IPFN-IST / IPP-MPG
 * All rights reserved.
 *
 * This source code is licensed under BSD-style license (found in the
 * LICENSE file in the root directory of this source tree)
*/

#ifndef ATCA_MIMO32_V2_DEVICE_H_
#define ATCA_MIMO32_V2_DEVICE_H_

#include <mutex>
#include <semaphore.h>
#include <thread>
#include <atomic>

#define DEBUG

#ifdef DEBUG
#include <time.h>
#endif

#define DMA_SIZE_DEFAULT 4096  // in kB

#define ADC_CHANNELS    32
#define INT_CHANNELS    8
#define ILOCK_PARAMS    10 // (INT_CHANNELS + 2)    // Interlock algortithm parameters
#define OVERFLOW_MASK  0x0000FC00

#define TMP101_SENSORS  2
#define MCA9808_SENSORS 8

namespace atca {

    /**
     * @brief ATCA MIMO32 V2 device driver wrapper. Provides controllables that provide configuration and sampling functionality.
     */
    class AtcaMimo32V2 {

        public:
            /**
             * @brief Construct the AtcaMimo32V2 object and initialize its internal data buffer.
             * @param bufferSize of the buffer to read to. Size is in MB.
             * @param dmaBufferSize of the buffer to read to. Size is in kB.
             *
             */
            AtcaMimo32V2(int deviceNumber, uint32_t bufferSize, uint32_t dmaBufferSize);

            /**
             * @brief Clear buffer and destruct the AtcaMimo32V2 object.
             */
            virtual ~AtcaMimo32V2();

            /**
             * @brief Open the connection to the device, configure chopper and start acquisition via soft trigger.
             * @param use32bits If set to true the 32 bit acquisition will be used and if set to false the 16 bit 
             *                  acquisition will be used. Data in the 32 bit acquisition is left aligned. When using
             *                  interlock mode the acquisition mode has to be 32 bit.
             * @param chopped Set to true to enable chopper.
             * @param chopperPeriod Period of the chopper.
             * @param channelMask of channels to be returned in buffer. For channels not selected the 0 value will be used. 
             *                    Note: channel mask is inverted where LEAST significant bit represents first channel 0.
             * @return EXIT_SUCCESS (0) if successful, EXIT_FAILURE (1) otherwise
             */
            int open(bool use32bits, uint16_t chopperPeriod, uint32_t channelMask);

            /**
             * @brief Reads FPGA Firmware Version
             * @param Pointer to version register
             * @return EXIT_SUCCESS (0) if successful, EXIT_FAILURE (1) otherwise
             */
            int readFirmwareVersion(uint32_t* version);

            /**
             * @brief Reads FPGA Firmware compile Timestamp
             * @param Pointer to version register
             * @return EXIT_SUCCESS (0) if successful, EXIT_FAILURE (1) otherwise
             */
            int readFirmwareTimestamp(uint32_t* timestamp);

            /**
             * @brief Reads IPMC register
             * @param Pointer to register
             * @return EXIT_SUCCESS (0) if successful, EXIT_FAILURE (1) otherwise
             */
            int readIpmcReg0(uint32_t* reg0);

            /**
             * @brief Reads FPGA Status register
             * @param Pointer to register
             * @return EXIT_SUCCESS (0) if successful, EXIT_FAILURE (1) otherwise
             */
            int readStatus(uint32_t* version);

            /**
             * @brief sets number of Hold samples in de-chopping algorithm
             * @param electricalOffsets to be used by the board.
             * @return EXIT_SUCCESS (0) if successful, EXIT_FAILURE (1) otherwise
             */
            int setHoldSamples(uint32_t nSamples);

            /**
             * @brief Set interlock parameters on the board.
             * @param electricalOffsets to be used by the board.
             * @param wiringOffsets to set on the board.
             * @param iParameters used by interlock. Element at index 8 and 9 correspond to threshold.
             * @return EXIT_SUCCESS (0) if successful, EXIT_FAILURE (1) otherwise
             */
            int setInterlockParameters(int32_t electricalOffsets[], float wiringOffsets[], float iParameters[]);

            /**
             * @brief Get interlock parameters on the board.
             * @param electricalOffsets used by the board.
             * @param wiringOffsets on the board.
             * @param iParameters used by interlock. Element at index 8 and 9 correspond to threshold.
             * @return EXIT_SUCCESS (0) if successful, EXIT_FAILURE (1) otherwise
             */
            int getInterlockParameters(int32_t electricalOffsets[], float wiringOffsets[], float iParameters[]);

            /**
             * @brief Get y with temperature in ºCelsius of the TMP101 sensors installed in FPGA board.
             * @param temperatures on the PCB board.
             * @return EXIT_SUCCESS (0) if successful, EXIT_FAILURE (1) otherwise
             */
            int getTemperaturesTMP101(float temperatures[]);

            int getTemperaturesMCA9808(float temperatures[]);
            /**
             * @brief Arms for acquisition. Acquisition will start once software or hardware trigger is received.
             */
            int armAcquisition();

            /**
             * @brief Stops acquisition. Acquisition can be restarted by arming it and sending another trigger.
             */
            int stopAcquisition();

            /**
             * @brief Send software trigger to start acquisition. Device should be armed before sending the trigger.
             */
            int sendSoftwareTrigger();

            /**
             * @brief Clears all registers which stops the acquisition and chopper and then close the connection to the device.
             * @return EXIT_SUCCESS (0) if successful, EXIT_FAILURE (1) otherwise
             */
            int close();

            /**
             * @brief Read the data from the device. Device will first acquire all the samples and only then copy values to 
             *        given buffer. Buffer has to be sufficiently large.
             * @param buffer to read to. Buffer allocation has to be big enough to accomodate 16 or 32 bit acquisition.
             * @param nSamples Number of samples to read.
             * @return EXIT_SUCCESS (0) if successful, EXIT_FAILURE (1) or 2 for timeout
             */
            int read(uint8_t* buffer, uint32_t nSamples, uint32_t timeoutMiliseconds);

            /**
             * @brief Returns the fill level of the internal DMA buffer as value between 0 and 1.
             */
            float getDMABufferFillLevel();

            /**
             * @brief Extract the sample's chopper phase out of the sample.
             * @param sampleBuffer Pointer to the buffer containing one full sample.
             * @return chopper phase of the sample.
             */
            static bool getSampleChopperPhase(uint32_t* sampleBuffer);

            /**
             * @brief Extract the FIFO DMA status at the time of the sample.
             * @param sampleBuffer Pointer to the buffer containing one full sample.
             * @return FIFO DMA status.
             */
            static uint8_t getSampleFifoDmaStatus(uint32_t* sampleBuffer);

            /**
             * @brief Extract the FIFO counter at the time of the sample.
             * @param sampleBuffer Pointer to the buffer containing one full sample.
             * @return FIFO counter at the time of the sample.
             */
            static uint16_t getSampleFifoCounter(uint32_t* sampleBuffer);

            /**
             * @brief Extract the sample's chopper phase out of the sample.
             * @param sampleBuffer Pointer to the buffer containing one full sample.
             * @return chopper phase of the sample.
             */
            static uint64_t getSampleCounter64Bit(uint32_t* sampleBuffer);

            /**
             * @brief Extract the interlock float value of the sample.
             * @param sampleBuffer Pointer to the buffer containing one full sample.
             * @param floatNumber of the float to extract. There are 4 floats related to the interlock, starting with float 0.
             * @return requested interlock float number.
             */
            static float getSampleFloatValue(uint32_t* sampleBuffer, int floatNumber);

            /**
             * @brief Extract the index of the integrated channel value encoded in this sample.
             * @param sampleBuffer Pointer to the buffer containing one full sample.
             * @return integrated channel index
             */
            static int getSampleIntegratedChannelIndex(uint32_t* sampleBuffer);

            /**
             * @brief Extract the value of the decimated integrated channel encoded in this sample.
             * @param sampleBuffer Pointer to the buffer containing one full sample.
             * @return integrated value
             */
            static float getSampleIntegratedValue(uint32_t* sampleBuffer);

        private:
            /**
             * @brief Read loop constantly reading incoming DMA data and pushing it to ring buffer. This method must be 
             *        executed in a dedicated high-priority thread. Ring buffer needs to be periodically emptied otherwise 
             *        the buffer will overflow (DMA buffer overflow error) which will stop any further acquisition.
             * @param device Void pointer to AtcaMimo32V2 device which handels thread and processing of the received data.
             * @return Not used
             */
            static void* dmaReadLoop(void* device);

            /**
             * @brief Add number of miliseconds to the given time.
             * @param a Time structure to add time to.
             * @param ms Miliseconds to add to current time. 
             */
            static inline void timespec_add_ms(struct timespec *a, uint64_t ms)
            {
                uint64_t sum = a->tv_nsec + ms * 1000000L;  // Convert miliseconds to nanoseconds
                while (sum >= 1000000000L) // Nanoseconds per second
                {
                    a->tv_sec ++;
                    sum -= 1000000000L; // Nanoseconds per second
                }
                a->tv_nsec = sum;
            }

            int deviceNumber;
            uint8_t* mapBase;
            int deviceHandle;
            int deviceDmaHandle;
            uint8_t* readBuffer;
            uint64_t readBufferSize;    /** Size of the whole DMA buffer */
            uint64_t readBufferOffset;  /** Offset of first unread source data */
            size_t  dmaSize;            /** Size of the XDMA buffer used in ::read()*/
            uint8_t* dmaBuffer = NULL;  /** real XDMA buffer  **/
            std::atomic_uint64_t readBufferFillSize;
            std::atomic_bool dmaReadLoopActive;
            std::atomic_bool isDeviceOpen;
            bool use32bits; /** True if 32 bit acquisition is used */


            std::thread dmaReadThread;
            std::mutex readMutex;
            std::mutex fpgaMutex;
            sem_t readSemaphore;

            void writeFpgaReg(off_t target, uint32_t val);
            void writeControlReg(uint32_t val);
            uint32_t readFpgaReg(off_t target);
            uint32_t readControlReg();
            int softReset();
            int hardReset();

#ifdef DEBUG
            static struct timespec diff(struct timespec start, struct timespec end);
            uint64_t counter;
            struct timespec readTime;
            FILE* readTimesFile;
#endif

    };
}

#endif // ATCA_MIMO32_V2_DEVICE_H_
//  vim: syntax=cpp ts=4 sw=4 sts=4 sr et
