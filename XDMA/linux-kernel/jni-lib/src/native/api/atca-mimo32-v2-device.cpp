/*
 * atca-mimo32-v2-device.cpp
 *
 * Copyright (c) 2016-present,  IPFN-IST / IPP-MPG
 * All rights reserved.
 *
 * This source code is licensed under BSD-style license (found in the
 * LICENSE file in the root directory of this source tree)
*/

#include <cstring>
#include <iostream>
#include <string>
#include <unistd.h>
//#include <sys/stat.h>
#include <fcntl.h>

#include <sys/mman.h>

#include "atca-mimo32-v2-device.h"

#define CURRENT_FW_VERSION  0x02030004   // MAJOR/MINOR/PATCH Version numbers

#define MAPSIZE  32768UL //  (32*1024UL)
#define MAP_MASK (MAPSIZE - 1)

#define XDMA_NODE_NAME  "adc_xdma"
#define DEVICE_C2H_0_FMT "/dev/%s%d_c2h_0"
#define DEVICE_USER_FMT "/dev/%s%d_user"

#define SHAPI_MAGIC 0x53480100U // Magic Word/SHAPI Version

/*  Registers address offset in BAR0 space */
#define FW_VERSION_REG_OFF   0x10
#define TIME_STAMP_REG_OFF   0x14
#define SHAPI_DEV_CAP           0X24
#define SHAPI_DEV_STATUS        0X28
#define SHAPI_DEV_CONTROL       0X2C

#define STATUS_REG_OFF          0x80
#define CONTROL_REG_OFF         0x84
#define CHOPP_PERIOD_REG_OFF    0x88
#define CHAN_MASK_OFF           0x94  // rw
#define I2C_REG_0_OFF           0x98  // ro
#define TMP101_0_1_OFF          0x9C  // ro
#define MCP9808_OFF             0xA0  // ro  U19 / U20
#define MCP9808_0_1_OFF         0xA0  // ro  U19 / U20
#define MCP9808_2_3_OFF         0xA4  // ro  U21 / U22
#define MCP9808_4_5_OFF         0xA8  // ro  U23 / U24
#define MCP9808_6_7_OFF         0xAC  // ro  U25 / U26

#define EO_REGS_OFF             0x100 // rw
#define WO_REGS_OFF             0x180 // rw
#define ILCK_REGS_OFF           0x200 // rw

#define CONTROL_REG_HOLD_SMP_MASK   0x0000003F  // Max 63 Hold samples. default is 3

/*   ----Device Control bits-----   */
#define SHAPI_DEV_ENDIAN_DMA_BIT   0 //Endianness of DMA data words  (0:little , 1: Big Endian)
#define SHAPI_SOFT_RESET_DMA_BIT  30
#define SHAPI_FULL_RESET_DMA_BIT  31

/* Bit position in "Control Reg" */
#define ENDIAN_DMA_BIT      8 // Endianness of DMA data words  (0:little , 1: Big Endian)
#define CHOP_EN_BIT         10 // State of Chop, if equals 1 chop is ON if 0 it is OFF
#define CHOP_DEFAULT_BIT    11 // Value of Chop case CHOP_STATE is 0
#define ILCK_F_OUT_EN_BIT   13
#define ILCK_QF_OUT_EN_BIT  14
#define DMA_DATA32_BIT      15 // 0: 16 bit, 1: 32 Bit data

#define STREAM_EN_BIT       20 // Streaming enable
#define ACQ_EN_BIT          23
#define STRG_BIT            24 // Soft Trigger

/* Special channels with embedded float data */
#define SAMPLE_FLOATS_CHA                 8
#define SAMPLE_INTEGRATED_VALUES_CHA     24
#define SAMPLE_FIFO_COUNTER_CHA          28
#define SAMPLE_INTEGRATED_VALUES_IDX_CHA 30
#define SAMPLE_CHOPPER_CHA               31

namespace atca
{

    AtcaMimo32V2::AtcaMimo32V2(int deviceNumber, uint32_t bufferSize , uint32_t dmaBufferSize) :
        deviceNumber(deviceNumber), mapBase(NULL), deviceHandle(0), deviceDmaHandle(0),
        readBuffer(NULL), dmaBuffer(NULL), readBufferSize(0), readBufferOffset(0),
        readBufferFillSize(0), dmaReadLoopActive(false),
        isDeviceOpen(false), use32bits(false)
    {
        sem_init(&readSemaphore, 0, 10);

        readBufferSize = bufferSize * 1024LL * 1024LL;
        readBuffer = (uint8_t*) malloc(readBufferSize);
        if (readBuffer == NULL) {
            fprintf(stderr, "Failed to allocate memory (%lu bytes)\n", readBufferSize);
        }
        dmaSize = dmaBufferSize * 1024LL;
        posix_memalign((void **) &dmaBuffer, 4096 /*alignment */ , dmaSize + 4096);
        if (!dmaBuffer) {
            fprintf(stderr, "Failed to alocate DMA memory %lu.\n", dmaSize + 4096);
            //	return ENOMEM;
        }


#ifdef DEBUG
        readTimesFile = NULL;
#endif
    }

    AtcaMimo32V2::~AtcaMimo32V2()
    {
        free(readBuffer);
        free(dmaBuffer);
        sem_destroy(&readSemaphore);
    }

    int AtcaMimo32V2::open(bool use32bits, uint16_t chopperPeriod, uint32_t channelMask)
    {
        if(isDeviceOpen) {
            std::cerr << "Device already opened, closing device." << std::endl;
            close();
        }

        this->use32bits = use32bits;
        char devname[64];
        snprintf(devname, 64, DEVICE_USER_FMT, XDMA_NODE_NAME, deviceNumber);
#ifdef DEBUG
        printf("Opening Device name = %s,\t", devname);
#endif
        deviceHandle = ::open(devname, O_RDWR | O_SYNC);
        if (deviceHandle == -1){
            return EXIT_FAILURE;
        }

        snprintf(devname, 64, DEVICE_C2H_0_FMT, XDMA_NODE_NAME, deviceNumber);
#ifdef DEBUG
        printf("DMA Device name = %s\n", devname);
#endif
        deviceDmaHandle = ::open(devname, O_RDONLY);
        //map_base=atca_init_device(deviceNumber, chopperPeriod, &deviceHandle, &deviceDmaHandle);
        if (deviceDmaHandle == -1){
            ::close(deviceHandle);
#ifdef DEBUG
            fprintf(stderr, "Error Openning DMA Device name = %s\n", devname);
#endif
            return EXIT_FAILURE;
        }
        mapBase = (uint8_t *) ::mmap(0, MAPSIZE, PROT_READ | PROT_WRITE, MAP_SHARED, deviceHandle, 0);
        if (mapBase == MAP_FAILED){
            ::close(deviceHandle);
            ::close(deviceDmaHandle);
            deviceHandle = -1;
#ifdef DEBUG
            fprintf(stderr, "Error Mapping Device name = %s\n", devname);
#endif
            return EXIT_FAILURE; //MAP_FAILED;
        }
        if(readFpgaReg(0) != SHAPI_MAGIC){
            fprintf(stderr, "Device is not SHAPI compatible, exiting.\nIs FW loaded?\n");
            ::close(deviceHandle);
            ::close(deviceDmaHandle);
            deviceHandle = -1;
#ifdef DEBUG
            fprintf(stderr, "Error SHAPI Reg. Device name = %s\n", devname);
#endif
            return EXIT_FAILURE;
        }

        writeFpgaReg(CHAN_MASK_OFF, channelMask);

        //        write_use_data32(mapBase, use32bits);
        uint32_t controlReg = 0;
        if(use32bits){
            controlReg |= (1 << DMA_DATA32_BIT);
        }
        /* Set the Chopping period if necessary */
        if (chopperPeriod> 0) {
            uint32_t tmp = chopperPeriod/2;  // Set Duty cycle at 50%
            tmp |= ((uint32_t) chopperPeriod)<< 16;
            writeFpgaReg(CHOPP_PERIOD_REG_OFF, tmp);
            //write_chopp_period(map_base, chopper_period);
            controlReg |= (1 << CHOP_EN_BIT);
        }

        writeControlReg(controlReg);

#ifdef DEBUG
        readTimesFile = fopen("dmaReadTimes.txt", "w");
        if (readTimesFile == NULL)
            fprintf(stderr, "Failed to open file to store DMA read times (dmaReadTimes.txt): %s\n", strerror(errno));
        else
            printf("Writing DMA readTimes to dmaReadTimes.txt\n");
        counter = 0;
        clock_gettime(CLOCK_REALTIME, &readTime);
#endif

        // Start acquisition thread but do not do any acquisition yet
        isDeviceOpen = true;
        dmaReadLoopActive = false;

        return EXIT_SUCCESS;
    }

    int AtcaMimo32V2::readFirmwareVersion(uint32_t* version)
    {
        if(!isDeviceOpen) {
            return EXIT_FAILURE;
        }
        uint8_t* virt_addr = mapBase + FW_VERSION_REG_OFF;
        uint32_t ver =  *((uint32_t*) virt_addr);
        /* check for outdated FW version */
        if (ver < CURRENT_FW_VERSION){
            fprintf(stderr, "The FPGA FW 0x%08X in the device %d is no longer compatible with this SW\n",
                    ver, deviceNumber);
            return -1;
        }
        /* check for future major/minor version */
        else if (ver >= (CURRENT_FW_VERSION + 0x00010000)){
            fprintf(stderr, "The FPGA FW 0x%08X in the device %d is newer than this SW\n", 
                    ver, deviceNumber);

            return -1;
        }

        *version = ver;

        return 0;

    }

    int AtcaMimo32V2::readFirmwareTimestamp(uint32_t* timestamp)
    {
        if(!isDeviceOpen) {
            return EXIT_FAILURE;
        }
        *timestamp = readFpgaReg(TIME_STAMP_REG_OFF);
        return EXIT_SUCCESS;
    }

    int AtcaMimo32V2::readIpmcReg0(uint32_t* reg0)
    {
        if(!isDeviceOpen) {
            return EXIT_FAILURE;
        }
        *reg0 = readFpgaReg(I2C_REG_0_OFF);
        return EXIT_SUCCESS;
    }

    int AtcaMimo32V2::readStatus(uint32_t* status)
    {
        if(!isDeviceOpen) {
            return EXIT_FAILURE;
        }
        *status = readFpgaReg(STATUS_REG_OFF);
        return EXIT_SUCCESS;
    }

    int AtcaMimo32V2::getTemperaturesMCA9808(float temperatures[MCA9808_SENSORS])
    {
        if(!isDeviceOpen) {
            return EXIT_FAILURE;
        }
        uint32_t raw32b;
        uint8_t* bytes = (uint8_t *) &raw32b;
        int16_t tval;

        for(int i = 0; i < 4; i++) {
            raw32b = readFpgaReg(MCP9808_OFF + i * 4);
#ifdef DEBUGG
            fprintf(stderr, "MCP9808 regs %d:  0x%08X\t", i, raw32b);
#endif
            tval = (0x1F & bytes[0]) << 8  | bytes[1];
            if(tval > 0xFFF)
                tval -= 8192;
            temperatures[i * 2] = ((float) tval )/16.0;
#ifdef DEBUGG
            fprintf(stderr, " %f\n", temperatures[i*2]);
#endif
            tval = (0x1F & bytes[2]) << 8  | bytes[3];
            if(tval > 0xFFF) 
                tval -= 8192;
            temperatures[i * 2 + 1] = ((float) tval)/ 16.0;
        }
        if((temperatures[0] < 0.0) || (temperatures[0] > 80.0))
            return EXIT_FAILURE;
        if((temperatures[1] < 0.0) || (temperatures[1] > 80.0))
            return EXIT_FAILURE;

        return EXIT_SUCCESS;
    }
    int AtcaMimo32V2::getTemperaturesTMP101(float temperatures[TMP101_SENSORS])
    {
        if(!isDeviceOpen) {
            return EXIT_FAILURE;
        }

        int16_t ival[TMP101_SENSORS];

        uint32_t raw = readFpgaReg(TMP101_0_1_OFF);
        uint8_t* bytes = (uint8_t*) &raw;

        // Check if i2c values ae OK for the tmp101 chip
        if(raw & 0x0F000F00)
            return EXIT_FAILURE;

        ival[0] = (bytes[0] << 8 ) | (bytes[1] & 0xF0);
        temperatures[0] = (float) ival[0];
        temperatures[0] /= 256.0; // >> 4

        if((temperatures[0] < 0.0) || (temperatures[0] > 80.0))
            return EXIT_FAILURE;

        ival[1] = (bytes[2] << 8 ) | (bytes[3] & 0xF0);
        temperatures[1] = (float) ival[1];
        temperatures[1] /= 256.0; // >> 4

        if((temperatures[1] < 0.0) || (temperatures[1] > 80.0))
            return EXIT_FAILURE;

        return EXIT_SUCCESS;
    }

    int AtcaMimo32V2::getInterlockParameters(int32_t electricalOffsets[ADC_CHANNELS],
            float wiringOffsets[ADC_CHANNELS],
            float iParameters[ILOCK_PARAMS])
    {
        if(!isDeviceOpen || dmaReadLoopActive) {
            std::cerr << "Device is not in configurable state." << std::endl;
            return EXIT_FAILURE;
        }

        for(int i=0; i < ADC_CHANNELS; i++)
            electricalOffsets[i] = (int32_t) readFpgaReg(EO_REGS_OFF + (i * sizeof(uint32_t)));

        for(int i=0; i < ADC_CHANNELS; i++){
            int32_t valInt = readFpgaReg(WO_REGS_OFF + (i * sizeof(uint32_t)));
            wiringOffsets[i] = valInt / 65536.0;
        }

        uint32_t* rawVal = (uint32_t*) iParameters;
        for(int i=0; i < ILOCK_PARAMS; i++){
            rawVal[i] = readFpgaReg(ILCK_REGS_OFF + (i * sizeof(uint32_t)));
        }
        return EXIT_SUCCESS;
    }

    int AtcaMimo32V2::setInterlockParameters(int32_t electricalOffsets[ADC_CHANNELS],
            float wiringOffsets[ADC_CHANNELS],
            float iParameters[ILOCK_PARAMS])
    {
        if(!isDeviceOpen || dmaReadLoopActive) {
            std::cerr << "Device is not in configurable state." << std::endl;
            return EXIT_FAILURE;
        }

        for(int i=0; i < ADC_CHANNELS; i++)
            writeFpgaReg(EO_REGS_OFF + (i * sizeof(uint32_t)), electricalOffsets[i]);

        int32_t  valInt;
        for(int i=0; i < ADC_CHANNELS; i++){
            valInt =  (int32_t) (wiringOffsets[i] * 65536);
            writeFpgaReg(WO_REGS_OFF + (i * sizeof(uint32_t)), valInt);
        }

        uint32_t* rawVal = (uint32_t*) iParameters;
        for(int i=0; i < ILOCK_PARAMS; i++){
            writeFpgaReg(ILCK_REGS_OFF + (i * sizeof(uint32_t)), rawVal[i]);
        }

        return EXIT_SUCCESS;
    }

    int AtcaMimo32V2::setHoldSamples(uint32_t nSamples)
    {
        if(!isDeviceOpen) {
            return EXIT_FAILURE;
        }
        uint32_t controlReg = readControlReg();
        controlReg &= ~(CONTROL_REG_HOLD_SMP_MASK);
        controlReg |= (nSamples & CONTROL_REG_HOLD_SMP_MASK);
        writeControlReg(controlReg);
        return EXIT_SUCCESS;
    }

    int AtcaMimo32V2::armAcquisition()
    {
        if(!isDeviceOpen) {
            return EXIT_FAILURE;
        }
        //     atca_arm_acq(map_base);
        uint32_t controlReg = readControlReg();
        controlReg &= ~(1 << STRG_BIT);
        controlReg |= (1 << ACQ_EN_BIT);
        writeControlReg(controlReg);

        dmaReadLoopActive = true;
        dmaReadThread = std::thread(&AtcaMimo32V2::dmaReadLoop, this);

        return EXIT_SUCCESS;
    }

    int AtcaMimo32V2::stopAcquisition()
    {
        if(!isDeviceOpen) {
            return EXIT_FAILURE;
        }
        dmaReadLoopActive = false;
        /* Don't touch FPGA while performing DMAs */
        fpgaMutex.lock();
        uint32_t controlReg = readControlReg();
        controlReg &= ~(1 << STRG_BIT);
        controlReg &= ~(1 << ACQ_EN_BIT);
        writeControlReg(controlReg);
        fpgaMutex.unlock();

        if (dmaReadThread.joinable()) {
            dmaReadThread.join();
        }

        // Discard any remaining data
        readBufferOffset = 0;
        readBufferFillSize = 0;

        return EXIT_SUCCESS;
    }

    int AtcaMimo32V2::sendSoftwareTrigger()
    {
        if(!isDeviceOpen) {
            return EXIT_FAILURE;
        }
        uint32_t controlReg = readControlReg();
        controlReg |= (1 << STRG_BIT);
        writeControlReg(controlReg);
        return EXIT_SUCCESS;
    }

    int AtcaMimo32V2::close()
    {
        if(!isDeviceOpen) {
            return EXIT_FAILURE;
        }

        isDeviceOpen = false;
        //dmaReadLoopActive = false;
        stopAcquisition();

#ifdef DEBUG
        //fprintf(stderr, "Joined Threads.\n");
        if (readTimesFile != NULL)
            fclose(readTimesFile);
#endif

        // Clears whole control register which stops acquisition and chopper
        //writeControlReg(0);
        softReset();
        ::close(deviceHandle);
        ::close(deviceDmaHandle);
        mapBase = 0;
        deviceHandle = 0;
        deviceDmaHandle = 0;

        return EXIT_SUCCESS;
    }

#ifdef DEBUG
    struct timespec AtcaMimo32V2::diff(struct timespec start, struct timespec end)
    {
        struct timespec temp;
        if ((end.tv_nsec-start.tv_nsec)<0)
        {
            temp.tv_sec = end.tv_sec-start.tv_sec-1;
            temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
        }
        else
        {
            temp.tv_sec = end.tv_sec-start.tv_sec;
            temp.tv_nsec = end.tv_nsec-start.tv_nsec;
        }
        return temp;
    }
#endif

    void* AtcaMimo32V2::dmaReadLoop(void* device)
    {
        AtcaMimo32V2* dev = (AtcaMimo32V2*) device;
        while (dev->isDeviceOpen && dev->dmaReadLoopActive)
        {
            if (dev->readBufferFillSize > dev->readBufferSize - dev->dmaSize)
            {
                // overflow error
                fprintf(stderr, "DMA BUFFER OVERFLOW - aborting DMA read loop. read Buffer size: %ldMB\t", dev->readBufferSize/1024/1024);

                fprintf(stderr, " Read Fill: %ld\n", dev->readBufferFillSize.load());

                // If DMA overflows then the acquisition will stop, in that case also stop reading of data.
                dev->stopAcquisition();

                sem_post(&dev->readSemaphore);
                break;
            }
            else
            {
#ifdef DEBUG
                if (dev->readTimesFile != NULL)
                {
                    struct timespec time;
                    clock_gettime(CLOCK_REALTIME, &time);
                    struct timespec dt = diff(dev->readTime, time);
                    fprintf(dev->readTimesFile, "%ld %ld.%09ld\n", dev->counter++, dt.tv_sec, dt.tv_nsec);
                    dev->readTime = time;
                }
#endif
                dev->readMutex.lock();
                uint64_t off = dev->readBufferOffset + dev->readBufferFillSize;
                dev->readMutex.unlock();

                //readBuffer is circular
                if (off >= dev->readBufferSize)
                    off -= dev->readBufferSize;

                // TODO: first read will not read all requested samples, is this acceptable? Could be firmware bug
                // Solved? XDMA Xilinx can not handle very big buffers, so a memcopy was implemented
                // Don't write FPGA regs while reading 
                dev->fpgaMutex.lock();
                int rc = ::read(dev->deviceDmaHandle, dev->dmaBuffer, dev->dmaSize);
                dev->fpgaMutex.unlock();
                if (rc != dev->dmaSize) {
                    if(rc>0)
                        memcpy(dev->readBuffer + off, dev->dmaBuffer, rc);
                    fprintf(stderr,
                            "Failed to read DMA: rc=%d, bufferOffset=%lu, fillSize=%lu, read=%d/%ld\n",
                            rc, dev->readBufferOffset, dev->readBufferFillSize.load(), rc, dev->dmaSize);
                    // TODO: do something if error occurs?
                }
                else
                {
#ifdef DEBUG
                    uint64_t val = dev->readBufferFillSize;
                    fprintf(stderr, "::read*() OK %lu, Off: %lu\r", val, off);
#endif
                    memcpy(dev->readBuffer + off, dev->dmaBuffer, dev->dmaSize);
                    dev->readBufferFillSize += dev->dmaSize;
                    sem_post(&dev->readSemaphore);
                }
            }
        }
        sem_post(&dev->readSemaphore);

        return 0;
    }

    int AtcaMimo32V2::read(uint8_t* buffer, uint32_t nSamples, uint32_t timeoutMiliseconds)
    {
        if (!isDeviceOpen)
            return EXIT_FAILURE;

#ifdef DEBUG
        fprintf(stderr, "DMA Buffer Level: %.03f\n", 100.0f * readBufferFillSize / readBufferSize);
#endif

        // Set timeout as an offset of the current absolute time
        //
        //NOTE ::read() System Timeout should now be programmed  via "sys" filesystem: or  xdma/adc_xdma.conf file
        struct timespec maxTime;
        clock_gettime(CLOCK_REALTIME, &maxTime);
        timespec_add_ms(&maxTime, timeoutMiliseconds);

        const int totalBlockSize = nSamples * ADC_CHANNELS *  (use32bits ? sizeof(uint32_t) : sizeof(int16_t));
        while (readBufferFillSize < totalBlockSize) {
            if (sem_timedwait(&readSemaphore, &maxTime) == -1) {
                if(errno == ETIMEDOUT)
                    return 2;
                else if (errno == EINTR)
                    return 0;
                else {
                    std::cerr << "Error while waiting for data. Error: " << errno << std::endl;
                    return EXIT_FAILURE;
                }
            }
#ifdef DEBUG
            uint64_t val = readBufferFillSize;
            fprintf(stderr, "Wait Sem Level: %lu, T: %d\r", val, totalBlockSize);
#endif
        }

        readMutex.lock();
        const int remaining = readBufferSize-readBufferOffset;
        if (totalBlockSize <= remaining)
            memcpy(buffer, readBuffer + readBufferOffset, totalBlockSize);
        else
        {
            memcpy(buffer, readBuffer + readBufferOffset, remaining);
            memcpy(buffer+remaining, readBuffer, totalBlockSize-remaining);
        }
        readBufferOffset += totalBlockSize;
        if (readBufferOffset >= readBufferSize)
            readBufferOffset -= readBufferSize;
        readBufferFillSize -= totalBlockSize;
        readMutex.unlock();

        return EXIT_SUCCESS;
    }

    float AtcaMimo32V2::getDMABufferFillLevel() {
        return (float) readBufferFillSize / (float) readBufferSize;
    }

    bool AtcaMimo32V2::getSampleChopperPhase(uint32_t* sampleBuffer)
    {
        //return get_chopper_phase(sampleBuffer);
        bool phase = false;
        if(sampleBuffer[SAMPLE_CHOPPER_CHA] & 0x01U) // bit 0
            phase = true;
        return phase;
    }

    uint8_t AtcaMimo32V2::getSampleFifoDmaStatus(uint32_t* sampleBuffer)
    {
        //return get_fifo_dma_status(sampleBuffer);
        uint8_t val = (sampleBuffer[SAMPLE_CHOPPER_CHA]) & 0x06; // check bits 1 and  2
        return val;
    }

    uint16_t AtcaMimo32V2::getSampleFifoCounter(uint32_t* sampleBuffer)
    {
        //return get_fifo_cnt(sampleBuffer);
        uint16_t val = sampleBuffer[SAMPLE_FIFO_COUNTER_CHA] & 0xFFU;
        val += ((sampleBuffer[SAMPLE_FIFO_COUNTER_CHA + 1] & 0xFFU) << 8);
        return val;
    }

    uint64_t AtcaMimo32V2::getSampleCounter64Bit(uint32_t* sampleBuffer)
    {
        //return get_counter_64bit(sampleBuffer);
        uint64_t val = 0;
        val = sampleBuffer[0] & 0xFFUL;
        val += ((((uint64_t)sampleBuffer[1]) & 0xFFUL) << 8);
        val += ((((uint64_t)sampleBuffer[2]) & 0xFFUL) << 16);
        val += ((((uint64_t)sampleBuffer[3]) & 0xFFUL) << 24);
        val += ((((uint64_t)sampleBuffer[4]) & 0xFFUL) << 32);
        val += ((((uint64_t)sampleBuffer[5]) & 0xFFUL) << 40);
        val += ((((uint64_t)sampleBuffer[6]) & 0xFFUL) << 48);
        val += ((((uint64_t)sampleBuffer[7]) & 0xFFUL) << 56);
        return val;
    }

    float AtcaMimo32V2::getSampleFloatValue(uint32_t* sampleBuffer, int floatNumber)
    {
        // TODO: until this gets changed in the lib we calculate the offset ourselves
        //return get_float_val(sampleBuffer, SAMPLE_FLOATS_CHA+(floatNumber*4));
        uint32_t offset =  SAMPLE_FLOATS_CHA + (floatNumber * sizeof(uint32_t));

        uint32_t ival = (sampleBuffer[offset]) & 0xFFU;
        ival += (( sampleBuffer[offset + 1]) & 0xFFU) << 8;
        ival += (( sampleBuffer[offset + 2]) & 0xFFU) << 16;
        ival += (( sampleBuffer[offset + 3]) & 0xFFU) << 24;
        float* fval = (float*) & ival;
        return *fval;
    }

    int AtcaMimo32V2::getSampleIntegratedChannelIndex(uint32_t* sampleBuffer)
    {
        return sampleBuffer[SAMPLE_INTEGRATED_VALUES_IDX_CHA] & 0x1FU;
    }

    float AtcaMimo32V2::getSampleIntegratedValue(uint32_t* sampleBuffer)
    {
        uint32_t ival = (sampleBuffer[SAMPLE_INTEGRATED_VALUES_CHA]) & 0xFFU;
        ival += (( sampleBuffer[SAMPLE_INTEGRATED_VALUES_CHA + 1]) & 0xFFU) << 8;
        ival += (( sampleBuffer[SAMPLE_INTEGRATED_VALUES_CHA + 2]) & 0xFFU) << 16;
        ival += (( sampleBuffer[SAMPLE_INTEGRATED_VALUES_CHA + 3]) & 0xFFU) << 24;
        return *((float*) &ival);
    }

    // private:

    void AtcaMimo32V2::writeFpgaReg(off_t target, uint32_t val)
    {
        uint8_t* virt_addr = mapBase + target;
        *((uint32_t*) virt_addr) = val;
    }
    void AtcaMimo32V2::writeControlReg(uint32_t val)
    {
        writeFpgaReg(CONTROL_REG_OFF, val);
    }
    uint32_t AtcaMimo32V2::readFpgaReg(off_t target)
    {
        uint8_t* virt_addr = mapBase + target;
        return *((uint32_t*) virt_addr);
    }
    uint32_t AtcaMimo32V2::readControlReg()
    {
        uint8_t* virt_addr = mapBase + CONTROL_REG_OFF;
        return *((uint32_t*) virt_addr);
    }

    int AtcaMimo32V2::softReset()
    {
        uint32_t deviceReg;
        if(isDeviceOpen) {
            return EBADF;
        }
        /*
           if(disableAcquisition() != 0)
           return -1;
           uint32 raw;
           */
        deviceReg = (1 << SHAPI_SOFT_RESET_DMA_BIT);
        writeFpgaReg(SHAPI_DEV_CONTROL, deviceReg);
        usleep(1000);
        // No need to re-write old deviceReg, as it will be reset.

        return 0;
    }

    int AtcaMimo32V2::hardReset()
    {
        if(isDeviceOpen) {
            return EBADF;
        }
        /* Attention: this may loose PCIe connection  */
        uint32_t deviceReg = (1 << SHAPI_FULL_RESET_DMA_BIT);
        writeFpgaReg(SHAPI_DEV_CONTROL, deviceReg);
        usleep(1000);
        return 0;
    }

} // namespace atca

//  vim: syntax=cpp ts=4 sw=4 sts=4 sr et
