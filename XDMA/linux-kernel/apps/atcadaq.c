/**
 * @file atcadaq.c
 * @brief Example application for the ATCA-MIMO-V2 boards
 * @author Bernardo Carvalho
 * @date 01/06/2021
 *
 * @copyright Copyright 2016 - 2021 IPFN-Instituto Superior Tecnico, Portugal
 * Licensed under the EUPL, Version 1.2 only (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence, available in 23 official languages of
 * the European Union, at:
 * https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12
 *
 * @warning Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 *
 * @depends ATCA V2 PCI ADC xdma  device driver
 * @details
 */

/*---------------------------------------------------------------------------*/
/*                        Standard header includes                           */
/*---------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <stdint.h>
#include <signal.h>

#include <sys/types.h>

/*---------------------------------------------------------------------------*/
/*                        Project header includes                            */
/*---------------------------------------------------------------------------*/
#include "atca-v2-daq.h"
#include "atca-v2-daq-lib.h"

//#define N_PACKETS 2  // size in full buffers (2MB) of data to save to disk. 2M=64 kSample =16 ms
//Global vars
int run=1;

#define DMA_SIZE 0x200000  // 2 MB

#define acq_time_524ms  32
#define acq_time_1048ms 64
#define acq_time_2096ms  128

void intHandler(int dummy) {
    run=0;
    printf("INT received!\n");
}

void getkey ()
{
    printf("Press Return to continue ...\n");
    getchar();
}



int main(int argc, char *argv[])
{
    int n_packets = 1;
    int dev_num = 0;
    int i,  k, loops;  					// loop iterators i
    int rc;
    //int fd;
    char *arg = "-sw";
    unsigned int fposition;				// position of read() in target array
    int hw_trigger = 0; 				// trigger scheme to start acquisition
    unsigned int oldtimer;				// value of last FPGA counter in buffer
    unsigned int old_loss_hits;
    unsigned int chopper_period;
    void *map_base;//, *virt_addr;
    int fd_bar_0, fd_bar_1;   // file descriptor of device2
    short * acqData; //[ N_AQS * N_PACKETS * 2 *NUM_CHAN];	// large buffer to store data for 2096 ms which can be saved to hard drive
    uint32_t fw_version;

    // Std ADC unchopped modules are used
    chopper_period = 2000;  // chopper_period is in Samples; dfault is 2000 = 1KHz; 1000 = 2KHz

    signal(SIGINT, intHandler);

    // Check command line
    if(argc < 3) {
        printf("Usage: atcadaq -524 |-1048 |-2096 -hw|-sw  -ch_on |-ch_off [dev_name]\n");
        exit(-1);
    }
    arg = argv[1];
    if (!strcmp(arg, "-524")) {
        n_packets = acq_time_524ms;
    }
    if (!strcmp(arg, "-1048")) {
        n_packets = acq_time_1048ms;
    }
    if (!strcmp(arg, "-2096")) 	{
        n_packets = acq_time_2096ms;
    }
    printf("arg1 = %s\n", arg);

    arg = argv[2];
    if (!strcmp(arg, "-hw")){
        hw_trigger = 1;   
    }
    printf("arg2 = %s\n", arg);

    if (argc > 3) {
        arg = argv[3];
        if (!strcmp(arg, "-ch_on")){
            chopper_period = 2000;
        } 
        else 
            chopper_period = 0;
        printf("arg3 = %s\n", arg);
    }

    if(argc > 4){
        arg = argv[4];
        printf("arg4 = %s\n", arg);
        //dev_name = argv[3];
        dev_num = atoi(arg);
        printf("arg4n = %d\n", dev_num);
    }

    // chopper_period is in Samples; e.g.2000 = 1KHz


    map_base = atca_init_device(dev_num, chopper_period, &fd_bar_0, &fd_bar_1);
    if (fd_bar_0 < 0)  {
        //if (map_base == MAP_FAILED){ //  (void *) -1
        fprintf (stderr,"Error: cannot open device %d \n", dev_num);
        fprintf (stderr," errno = %i\n",errno);
        printf ("open error : %s\n", strerror(errno));
        exit(1);
    }
    atca_arm_acq(map_base);
    printf("CR: 0x%0X\n", read_control_reg(map_base));

    /* Set Channel Mask */
    // write_channel_mask (map_base, 0xFFFFFFFF); // All channels enabled
    write_channel_mask (map_base, 0x00FF0055); // Only  channels 1,3,5,7 and 17-24 are enabled
    printf("Channel Mask: 0x%08x\n", read_channel_mask(map_base));

    acqData = (short *) malloc(DMA_SIZE * n_packets);
    i = 0;
    loops = 0;
    printf("FW_TS: %ld\n", read_fw_timestamp(map_base));
    printf("%s\n", read_fw_timestamp_str(map_base));
    fw_version = read_fw_version(map_base);
    printf("FW Version: 0x%.8X, MAJOR: %d, MINOR: %d, PATCH:%d\n", fw_version, 
            (fw_version & 0xFF000000)>>24, (fw_version & 0x00FF0000)>>16,
            (fw_version & 0xFFFF) );
    // ADC board is armed and waiting for trigger (hardware or software)
    if (!hw_trigger)
    {
        printf("FPGA Status: 0x%.8X\n", read_status_reg(map_base));
        printf("DMA Size %d\n", DMA_SIZE);
        printf("software trigger mode active\n");
        getkey(); // Press any key to start the acquisition
        // Start the dacq with a software trigger
        atca_soft_trigger(map_base);
        printf("CR: 0x%0X, mb:%p\n", read_control_reg(map_base), map_base);
        printf("FPGA Status: 0x%.8X\n", read_status_reg(map_base));
    }
    else
        printf("hardware trigger mode active\n");

    fflush(stdout);
    fposition = 0;
    while (loops++ < n_packets) {

        rc = 0;

        while (rc == 0) {
            rc = read(fd_bar_1, &acqData[fposition], DMA_SIZE); // loop until there is something to read.
        }

        fposition += (DMA_SIZE/2);

        i++;
    }
    write_control_reg(map_base, 0);  // stop board
    //    rc = read(fd, &acqData[fposition], DMA_SIZE); // loop until there is something to read.
    if (close(fd_bar_1)) {

        printf("Error closing : %s\n", strerror(errno));
    }
    // Check status to see if everything is okay (optional)
    /*rc = ioctl(fd, PCIE_ADC_IOCG_STATUS, &tmp);*/

    printf("Streaming off - FPGA Status: 0x%.8X %d \n", read_status_reg(map_base));
    fflush(stdout);
    save_to_disk2 (n_packets, &acqData[0]);

    close(fd_bar_0);
    close(fd_bar_1);
    free(acqData);
    return 0;
    }
