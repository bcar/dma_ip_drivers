/**
 * @file atca-v2-daq-lib.c
 * @brief Header file for ATCA V2 MIMO board functions
 * @author Bernardo Carvalho 
 * @date 01/06/2021
 *
 * @copyright Copyright 2016 - 2021 IPFN-Instituto Superior Tecnico, Portugal
 * Licensed under the EUPL, Version 1.2 only (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence, available in 23 official languages of
 * the European Union, at:
 * https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12
 *
 * @warning Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 *
 * @details
 * ATCA V2 PCI ADC  device driver
 * Definitions for the Linux Device Driver
 */

/*---------------------------------------------------------------------------*/
/*                        Standard header includes                           */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/*                        Project header includes                            */
/*---------------------------------------------------------------------------*/
#include "atca-v2-daq-lib.h"

/* IMPORTANT: User same name as in device driver file "../xdma/xdma_cdev.h" */
#define XDMA_NODE_NAME	"adc_xdma"

/*#define DEVICE_FMT "/dev/"*/
#define DEVICE_C2H_0_FMT "/dev/%s%d_c2h_0"
#define DEVICE_C2H_1_FMT "/dev/%s%d_c2h_1"
#define DEVICE_USER_FMT "/dev/%s%d_user"

//#define DEVICE_C2H_1_STR "/dev/adc_xdma%d_c2h_1"
//#define DEVICE_USER_STR "/dev/adc_xdma%d_user"

#define MAP_SIZE (32*1024UL) 
#define MAP_MASK (MAP_SIZE - 1)

#define BUF_LEN 256
char buf[BUF_LEN];

typedef struct wo_config_raw{
  int32_t offset[INT_CHANNELS];
} wo_config_raw;

typedef struct ilck_params_raw {
  uint32_t val_uint[N_ILOCK_PARAMS];
} ilck_params_raw;

void save_to_disk2(int nrOfReads, short *acqData){
    FILE *stream_out;
    char file_name[40];
    char dir_name[40];
    char path_name[80];

    int numchan;
    int nrpk;
    time_t now;
    struct tm *t;

    // Build directory name based on current date and time
    now = time(NULL);
    t = localtime(&now);
    strftime(dir_name, sizeof(dir_name)-1, "%Y%m%d%H%M%S", t);

    printf("Saving data to disk ...\n");
    printf("\ndir_name: %s\n", dir_name);

    mkdir(dir_name, 0777); // Create directory

    for (numchan = 0; numchan < 32; numchan++) {
        // Create complete pathname for data file
        sprintf(file_name,"/ch%2.2d.txt", numchan+1);
        strcpy(path_name, dir_name);
        strcat(path_name, file_name);
        //printf("path_name: %s\n", path_name);
        stream_out = fopen(path_name,"wt");
        // Write data of each channel  into a separate file (max 32)
        for (nrpk=0; nrpk < nrOfReads*N_AQS ; nrpk++) {
            fprintf(stream_out, "%.4hd ",acqData[(nrpk*ADC_CHANNELS)+numchan]);
            fprintf(stream_out, "\n");
        }
        printf("%d values for channel %d saved\n", nrOfReads*N_AQS, numchan+1);
    }

    fflush(stream_out);
    fclose(stream_out);
    printf("Saving data to disk finished\n");

}

int get_tmp101_vals (void *map_base, float *temps){
/*int get_tmp101_vals(unsigned int raw, float *temps){*/
    int rc=0, raw;
    uint8_t * bytes =(uint8_t *) & raw;
    int16_t ival[2];
    raw = read_fpga_reg(map_base, TMP101_0_1_OFF);
    // Check if i2c values ae OK for the tmp101 chip
    if(raw & 0x0F000F00)
        rc = -1;
    ival[0] = (bytes[0] << 8 ) | (bytes[1] & 0xF0);
    temps[0] = (float) ival[0];
    temps[0] /= 256.0; // >> 4
    if((temps[0] < 0.0) || (temps[0] > 80.0))
        rc = -1;
    ival[1] = (bytes[2] << 8 ) | (bytes[3] & 0xF0);
    temps[1] = (float) ival[1];
    temps[1] /= 256.0; // >> 4
    if((temps[1] < 0.0) || (temps[1] > 80.0))
        rc = -1;
    return rc;
}

int get_mca9808_vals(void *map_base, int pair, float *temps){
    int rc = 0, raw;
    uint8_t * bytes =(uint8_t *) & raw;
    int16_t ival[2];
    if(pair > 3)
        return -1;
    raw = read_fpga_reg(map_base, MCP9808_0_1_OFF + pair * sizeof(int));
    if(raw & 0xE003E003) // These bits must be 0
        return -1;
    ival[0] = (bytes[0] << 11 ) | (bytes[1] << 3); // MSB is 4bit plus sign
    temps[0] = (float) ival[0];
    temps[0] /= 128.0;
    if((temps[0] < 0.0) || (temps[0] > 80.0))
        rc = -1;
    ival[1] = (bytes[2] << 11 ) | (bytes[3] << 3); // MSB is 4bit plus sign
    temps[1] = (float) ival[1];
    temps[1] /= 128.0; // >> 3 / 16.0
    if((temps[1] < 0.0) || (temps[1] > 80.0))
        rc = -1;
    return rc;
}

int atca_soft_reset(void * map_base){
    uint32_t device_reg, rc;
    device_reg = 1 << SHAPI_SOFT_RESET_DMA_BIT;
    rc = write_fpga_reg(map_base, SHAPI_DEV_CONTROL, device_reg);
    usleep(1000);
    // No need to re-write device_reg as it will reset itself.
    return rc;
}

int atca_full_reset(void * map_base){
    uint32_t device_reg, rc;
    device_reg = 1 << SHAPI_FULL_RESET_DMA_BIT;
    rc = write_fpga_reg(map_base, SHAPI_DEV_CONTROL, device_reg);
    usleep(1000);
    // No need to re-write device_reg as it will reset itself.
    return rc;
}

int atca_open_c2h_1(unsigned int dev_num){
    char devname[80];
    int fd_dma=-1;

    sprintf(devname, DEVICE_C2H_1_FMT, XDMA_NODE_NAME, dev_num);
    if ( (fd_dma= open(devname, O_RDWR )) == -1){
        printf("Device %s C2H_1 not opened.\n", devname );
        return -1;
    }
//    printf("Device %s opened.\n", device_u );
    return fd_dma;
}

void * atca_init_device(unsigned int dev_num, unsigned int chopper_period,
        int *fd_bar0, int *fd_bar1_c2h_0){
    char devname[80];
    void *map_base = MAP_FAILED;
    uint32_t control_reg = 0;
    int i,ver;

    sprintf(devname, DEVICE_C2H_0_FMT, XDMA_NODE_NAME, dev_num);
/*
     * use O_TRUNC to indicate to the driver to flush the data up based on
     * EOP (end-of-packet), streaming mode only
     */
//  if (eop_flush)
//      fpga_fd = open(devname, O_RDWR | O_TRUNC);
//  else

    if ((*fd_bar1_c2h_0= open(devname, O_RDWR)) == -1){
        printf("Device %s CH0 not opened.\n", devname );
        return MAP_FAILED;
    }

    sprintf(devname, DEVICE_USER_FMT, XDMA_NODE_NAME, dev_num);
    if ((*fd_bar0 = open(devname, O_RDWR | O_SYNC)) == -1){
        printf("Device %s not opened.\n", devname );
        close(*fd_bar1_c2h_0);
        return MAP_FAILED;
    }
    /* map one mem page on BAR0 (FPGA registers)*/
    map_base = mmap(0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, *fd_bar0, 0);
    if (map_base == MAP_FAILED){
        close(*fd_bar0);
        close(*fd_bar1_c2h_0);
        return MAP_FAILED;
    }
    if(read_fpga_reg(map_base, 0) != 0x53480100){
        printf("Device is not SHAPI compatible, exiting. (is FW loaded?)\n");
        close(*fd_bar0);
        close(*fd_bar1_c2h_0);
        return MAP_FAILED;
    }
    ver = read_fw_version(map_base);
    if (ver< 0) {
        printf("Device FW (0x%08X) is not valid, exiting.\n", ver);
        close(*fd_bar0);
        close(*fd_bar1_c2h_0);
        return MAP_FAILED;
    }
    else if (ver < 0x01000000){
        printf("Device FW (0x%08X) is not compatible with this API, exiting.\n", ver);
        close(*fd_bar0);
        close(*fd_bar1_c2h_0);
        return MAP_FAILED;
    }
    atca_soft_reset(map_base);
    /*
    write_control_reg(map_base, 0x00);
    for (i=0; i < ADC_CHANNELS ; i++)
        write_fpga_reg(map_base, EO_REGS_OFF + i*sizeof(uint32_t), 0);
    for (i=0; i < INT_CHANNELS; i++)
        write_fpga_reg(map_base, WO_REGS_OFF + i*sizeof(uint32_t), 0);
    for (i=0; i < N_ILOCK_PARAMS ; i++)
        write_fpga_reg(map_base, ILCK_REGS_OFF + i*sizeof(uint32_t), 0);
    */
    /* Set the Chopping period if necessary */
    if (chopper_period > 0) {
        write_chopp_period(map_base, chopper_period);
        control_reg |= (1 << CHOP_EN_BIT);
    }
    //write_channel_mask(map_base, 0xFFFFFFFF); // Enable all channels 
    write_control_reg(map_base, control_reg);

    return map_base;
}

int atca_use_little_endian(void *map_base, int use32bit){
    if((read_fpga_reg(map_base, SHAPI_DEV_CAP) & SHAPI_DEV_ENDIAN_DMA_BIT)!=0)
        return -1;
    write_fpga_reg(map_base, SHAPI_DEV_CONTROL, 0);
    return 0;
}

int atca_use_big_endian(void *map_base, int use32bit){
    if((read_fpga_reg(map_base, SHAPI_DEV_CAP) & SHAPI_DEV_ENDIAN_DMA_BIT)!=0)
        return -1;
    write_fpga_reg(map_base, SHAPI_DEV_CONTROL, SHAPI_DEV_ENDIAN_DMA_BIT);
    return 0;
}

int atca_use_data32(void *map_base, int use32bit){
    uint32_t control_reg = read_control_reg(map_base);
    if(use32bit)
        control_reg |= (1 << DMA_DATA32_BIT);
    else
        control_reg &= ~(1 << DMA_DATA32_BIT);
    return write_control_reg(map_base, control_reg);
}

int write_eo_offsets(void *map_base, struct atca_eo_config *pConfig){
    int i, rc;
    for (i = 0; i < INT_CHANNELS; i++){
        rc = write_fpga_reg(map_base,
                (EO_REGS_OFF + (i * sizeof(uint32_t))), pConfig->offset[i]);
    }
    return rc;
}
int write_wo_offsets(void *map_base, struct atca_wo_config *pConfig){  // Input float values
    int i, rc;
    /*wo_config_raw *pConfRaw = (wo_config_raw *) pConfig;*/
    int32_t valInt;
    for (i = 0; i < INT_CHANNELS; i++){
        valInt =  (int32_t) (pConfig->offset[i]  * pow(2,16));
        rc = write_fpga_reg(map_base,
                (WO_REGS_OFF + (i * sizeof(uint32_t))), valInt);
    }
    return rc;
}
int write_ilck_params(void *map_base, struct atca_ilck_params *pConfig){
    int i, rc;
    ilck_params_raw * pConfRaw  = (ilck_params_raw *) pConfig;
    for (i=0; i < N_ILOCK_PARAMS; i++)
//        printf("%u, %5.3f, 0x%08X\n",i , pConfig->param[i], pConfRaw->val_uint[i]);
        rc = write_fpga_reg(map_base,
                (ILCK_REGS_OFF + (i * sizeof(uint32_t))), pConfRaw->val_uint[i]);
    return rc;
}
int read_ilck_params(void *map_base, struct atca_ilck_params *pConfig){
    int rc=0;
    ilck_params_raw * pConfRaw  = (ilck_params_raw *) pConfig;
    for (int i = 0; i < N_ILOCK_PARAMS; i++)
        pConfRaw->val_uint[i] = read_fpga_reg(map_base,
                (ILCK_REGS_OFF + (i * sizeof(uint32_t))));
    return rc;
}
int write_chopp_period(void *map_base, uint16_t period){
    uint32_t val;
    val = period/2; // Set Duty cycle at 50%
    val |= ((int) period)<< 16;
    return write_fpga_reg(map_base, CHOPP_PERIOD_REG_OFF, val);
}
int enable_chopping(void * map_base){
    uint32_t control_reg;
    control_reg=read_control_reg(map_base);
    control_reg |= (1 << CHOP_EN_BIT);
    return write_control_reg(map_base, control_reg);
}
int disable_chopping(void * map_base){
    uint32_t control_reg;
    control_reg=read_control_reg(map_base);
    control_reg &= ~(1 << CHOP_EN_BIT);
    write_control_reg(map_base, control_reg);
}
int enable_interlockF(void * map_base){
    uint32_t control_reg;
    control_reg=read_control_reg(map_base);
    control_reg |= (1 << ILCK_F_OUT_EN_BIT);
    return write_control_reg(map_base, control_reg);
}
int disable_interlockQF(void * map_base ){
    uint32_t control_reg;
    control_reg=read_control_reg(map_base);
    control_reg &= ~(1 << ILCK_F_OUT_EN_BIT);
    write_control_reg(map_base, control_reg);
}
int enable_interlockQF(void * map_base ){
    uint32_t control_reg;
    control_reg=read_control_reg(map_base);
    control_reg |= (1 << ILCK_QF_OUT_EN_BIT);
    return write_control_reg(map_base, control_reg);
}
int disable_interlockF(void * map_base ){
    uint32_t control_reg;
    control_reg=read_control_reg(map_base);
    control_reg &= ~(1 << ILCK_QF_OUT_EN_BIT);
    write_control_reg(map_base, control_reg);
}
int write_channel_mask(void *map_base, uint32_t val){
         return write_fpga_reg(map_base, CHAN_MASK_OFF, val);
}
int read_channel_mask(void *map_base){
         return read_fpga_reg(map_base, CHAN_MASK_OFF);
}
int write_control_reg(void *map_base, uint32_t val){
         return write_fpga_reg(map_base, CONTROL_REG_OFF, val);
}
int write_fpga_reg(void *map_base, off_t target, uint32_t val){
         void *virt_addr = map_base + target;
         *((uint32_t *) virt_addr) = val;
        return 0;
}

int atca_stop_acq(void * map_base ){
    uint32_t control_reg;
    control_reg=read_control_reg(map_base);
    control_reg &= ~(1 << STRG_BIT);
    control_reg &= ~(1 << ACQ_EN_BIT);
    return write_control_reg(map_base, control_reg);
}
int atca_arm_acq(void * map_base ){
    uint32_t control_reg;
    control_reg=read_control_reg(map_base);
    control_reg &= ~(1 << STRG_BIT);
    control_reg |= (1 << ACQ_EN_BIT);
    return write_control_reg(map_base, control_reg);
}
int atca_soft_trigger(void * map_base ){
    uint32_t control_reg;
    control_reg=read_control_reg(map_base);
    control_reg |= (1 << STRG_BIT);
    return write_control_reg(map_base, control_reg);
}

int read_fw_version(void *map_base){
    uint32_t val;
    val = read_fpga_reg(map_base, FW_VERSION_REG_OFF);
    /* check for older version */
    if (val < CURRENT_FW_VERSION)
        return 1;
    /* check for future major/minor version */
    else if (val >= (CURRENT_FW_VERSION + 0x00010000))
        return 2;
    else
        return val;
}
uint32_t read_fw_timestamp(void *map_base){
        return read_fpga_reg(map_base, TIME_STAMP_REG_OFF);
}
char * read_fw_timestamp_str(void *map_base){
    time_t rawtime = (time_t) read_fw_timestamp(map_base);
    if (rawtime == -1)
        return NULL;
    struct tm *ptm = localtime(&rawtime);
    strftime(buf, BUF_LEN, "FW compilation date is %A, %b %d. Time:  %r", ptm);
    return buf;
}
uint32_t read_control_reg(void *map_base){
        return read_fpga_reg(map_base, CONTROL_REG_OFF);
}
uint32_t read_fpga_reg(void *map_base, off_t target ){
        void *virt_addr = map_base + target;
        uint32_t val;
        val = *((uint32_t *) virt_addr);
        return val;
}
uint32_t read_status_reg(void *map_base){
        uint32_t val;
        val= read_fpga_reg(map_base, STATUS_REG_OFF);
        return val;
}

/**
 * @brief prints a progress indicator
 * @param[in] percentage (0.0 to 1.0)
 * @details taken from
 * https://stackoverflow.com/questions/14539867/how-to-display-a-progress-indicator-in-pure-c-c-cout-printf
 *
 */
#define PBSTR "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"
#define PBWIDTH 40
void printProgress(float percentage) {
    int val = (int) (percentage * 100);
    int lpad = (int) (percentage * PBWIDTH);
    int rpad = PBWIDTH - lpad;
    printf(" %3d%% [%.*s%*s]", val, lpad, PBSTR, rpad, "");
    fflush(stdout);
}

uint64_t get_counter_64bit(uint32_t * pSamp){
    uint64_t val = 0;
    val = pSamp[0] & 0xFFUL;
    val += ((((uint64_t)pSamp[1]) & 0xFFUL) << 8);
    val += ((((uint64_t)pSamp[2]) & 0xFFUL) << 16);
    val += ((((uint64_t)pSamp[3]) & 0xFFUL) << 24);
    val += ((((uint64_t)pSamp[4]) & 0xFFUL) << 32);
    val += ((((uint64_t)pSamp[5]) & 0xFFUL) << 40);
    val += ((((uint64_t)pSamp[6]) & 0xFFUL) << 48);
    val += ((((uint64_t)pSamp[7]) & 0xFFUL) << 56);
    return val;
}

uint16_t get_fifo_cnt(uint32_t * pSamp){
    uint16_t val;
    val = pSamp[28] & 0xFFU;
    val += (((pSamp[29]) & 0xFFU) << 8);
    return val;
}

float get_float_val(uint32_t * pSamp, int offset){
    uint32_t val = 0;
    float *fval = (float *) &val;
    val = (pSamp[offset]) & 0xFFU;
    val += (( pSamp[offset + 1]) & 0xFFU) << 8;
    val += (( pSamp[offset + 2]) & 0xFFU) << 16;
    val += (( pSamp[offset + 3]) & 0xFFU) << 24;
    return *fval;
}

float get_c2h1_float_val(uint32_t * pSamp, int offset){
    float *fval = (float *) &pSamp[offset];
    return *fval;
}

double get_c2h1_double_val(uint32_t * pSamp, int offset){
    double *dval = (double *) &pSamp[offset];
    uint64_t *uval = (uint64_t *) &pSamp[offset];
    //printf("U:0x%016lX ", *uval);
    return *dval;
}

uint8_t get_chopper_phase (uint32_t *pSamp) {
    uint8_t val = 0;
    val = (pSamp[31]) & 0x1; // bit 0
    return val;
}

uint8_t get_fifo_dma_status (uint32_t *pSamp) {
    uint8_t val = 0;
    val = (pSamp[31]) & 0x6; // bits 1+2 (2+4)
    return val;
}
