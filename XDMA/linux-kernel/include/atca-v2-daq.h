/**
 * ATCA V2 PCI ADC  device driver
 * Definitions for the Linux Device Driver
 *
 * Copyright 2016 - 2022 IPFN-Instituto Superior Tecnico, Portugal
 * Creation Date  2016-02-10
 *
 * Licensed under the EUPL, Version 1.2 only (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence, available in 23 official languages of
 * the European Union, at:
 * https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 *
 */
#ifndef _ATCA_V2_DAQ_H_
#define _ATCA_V2_DAQ_H_
/*---------------------------------------------------------------------------*/
/*                        Standard header includes                           */
/*---------------------------------------------------------------------------*/
#include <stdint.h>
/*---------------------------------------------------------------------------*/
/*                        Project header includes                            */
/*---------------------------------------------------------------------------*/
//
/* ltoh: little to host */
/* htol: little to host */
#if __BYTE_ORDER == __LITTLE_ENDIAN
#define ltohl(x)       (x)
#define ltohs(x)       (x)
#define htoll(x)       (x)
#define htols(x)       (x)
#elif __BYTE_ORDER == __BIG_ENDIAN
#define ltohl(x)     __bswap_32(x)
#define ltohs(x)     __bswap_16(x)
#define htoll(x)     __bswap_32(x)
#define htols(x)     __bswap_16(x)
#endif

#define CURRENT_FW_VERSION  0x02020013     // MAJOR/MINOR Version numbers

#define ADC_CHANNELS    32
#define INT_CHANNELS    8
#define N_ILOCK_PARAMS  10 // (INT_CHANNELS + 2)    // Interlock algortithm parameters
#define OVERFLOW_MASK  0x0000FC00
#define PCKT_SIZE_16BIT (ADC_CHANNELS * 2)
#define PCKT_SIZE_32BIT (ADC_CHANNELS * 4)

#define DATA_MODE_16 0
#define DATA_MODE_32 1
#define DATA_MODE_DEC 2 // Not Tested!
#define DATA_MODE_DEFAULT DATA_MODE_16

#define CHANNEL_MASK_DEFAULT  0xFFFFFFFF

#define MAP_SIZE (32*1024UL)
#define MAP_MASK (MAP_SIZE - 1)

#define DEVICE_NUM_DEFAULT 0
#define DEVICE_USER_NAME_DEFAULT "/dev/adc_xdma0_user"

#define SIZE_DEFAULT 0x200000  // 2 MB

//SHAPI PICMG Register Set
struct shapi_device_info {
    uint32_t SHAPI_VERSION;
    uint32_t SHAPI_FIRST_MODULE_ADDRESS;
    uint32_t SHAPI_HW_IDS;
    uint32_t SHAPI_FW_IDS;
    uint32_t SHAPI_FW_VERSION;
    uint32_t SHAPI_FW_TIMESTAMP;
    uint32_t SHAPI_FW_NAME[3];
    uint32_t SHAPI_DEVICE_CAP;
    volatile uint32_t SHAPI_DEVICE_STATUS;
    uint32_t SHAPI_DEVICE_CONTROL;
    uint32_t SHAPI_IRQ_MASK;
    uint32_t SHAPI_IRQ_FLAG;
    volatile uint32_t SHAPI_IRQ_ACTIVE;
    volatile uint32_t SHAPI_SCRATCH_REGISTER;
    char fw_name[13];
};
typedef struct shapi_device_info shapi_device_info;

// TODO: The list_head module_list is temporary commented out to make the code compile.
//       This is a temporary workaround for the missing libaio.h dependency.
struct shapi_module_info {
//    struct list_head module_list;
    uint32_t SHAPI_VERSION;
    uint32_t SHAPI_NEXT_MODULE_ADDRESS;
    uint32_t SHAPI_MODULE_FW_IDS;
    uint32_t SHAPI_MODULE_VERSION;
    uint32_t SHAPI_MODULE_NAME[2];
    uint32_t SHAPI_MODULE_CAP;
    uint32_t SHAPI_MODULE_STATUS;
    uint32_t SHAPI_MODULE_CONTROL;
    uint32_t SHAPI_IRQ_ID;
    uint32_t SHAPI_IRQ_FLAG_CLEAR;
    uint32_t SHAPI_IRQ_MASK  ;
    uint32_t SHAPI_IRQ_FLAG;
    uint32_t SHAPI_IRQ_ACTIVE;
    uint32_t reserved[2];
    volatile uint32_t atca_status; // ro ADDR 0x80
    volatile uint32_t atca_control;         // rw
    volatile uint32_t atca_chop_period;
    uint32_t atca_max_dma_size;
    uint32_t atca_max_tlp_size;    // ADDR 0x90
    volatile uint32_t channel_mask;  // rw     ADDR 0x94
    volatile uint32_t i2c_reg0;      // ro
    volatile uint32_t tmp101;        // ro
    volatile uint32_t mcp9808[4];    // ro  // ADDR 0xA0
    volatile uint32_t i2c_reg1;      // rw  // ADDR 0xB0
    uint32_t reserved2[19]; //
    volatile uint32_t atca_eo_offsets[ADC_CHANNELS];
    //uint32_t reserved21[24]; //
    volatile uint32_t atca_wo_offsets[ADC_CHANNELS];
    //uint32_t reserved3[8];
    volatile uint32_t atca_ilck_params[N_ILOCK_PARAMS];
    char module_name[9];
    int module_num;
};

typedef struct shapi_module_info shapi_module_info;

typedef struct _STATUS_FLDS {
  volatile uint32_t SLOT_ID:8,
    rsv0:1, ACQ_ON:1, DFIEOflw:1, DFAEOflw:1, DFIOOflw:1, DFAOOflw:1, DMAFifoOflw:1, rsv:1,
    RTM_LCK:1, TE741_LCK:1, ATCA_LCK:1, rsv11:1, IDLAY_RDY:1, MSI_EN:1, MASTER:1, rsv2:1, // FSH:1, RST:1
    rsv3:8; // msb
} STATUS_FLDS;

//--      Registers address offsets in BAR0 space
#define FW_VERSION_REG_OFF      0x10
#define SHAPI_DEV_CAP           0X24
#define SHAPI_DEV_STATUS        0X28
#define SHAPI_DEV_CONTROL       0X2C
#define TIME_STAMP_REG_OFF      0x14
#define STATUS_REG_OFF          0x80
#define CONTROL_REG_OFF         0x84
#define CHOPP_PERIOD_REG_OFF    0x88

#define CHAN_MASK_OFF              0x94
#define I2C_REG_0_OFF              0x98  //ro
#define TMP101_0_1_OFF             0x9C  //ro
#define MCP9808_0_1_OFF            0xA0  //ro  U19 / U20
#define MCP9808_2_3_OFF            0xA4  //ro  U21 / U22
#define MCP9808_4_5_OFF            0xA8  //ro  U23 / U24
#define MCP9808_6_7_OFF            0xAC  //ro  U25 / U26

#define I2C_REG_1_OFF               0xB0  //rw

#define EO_REGS_OFF             0x100   // rw
#define WO_REGS_OFF             0x180   // rw
#define ILCK_REGS_OFF           0x1C0   // rw

/*   ****** Device Control bits ******/
#define SHAPI_DEV_ENDIAN_DMA_BIT  0 //Endianness of DMA data words  (0:little , 1: Big Endian)
#define SHAPI_SOFT_RESET_DMA_BIT  30
#define SHAPI_FULL_RESET_DMA_BIT  31

 //#### CONTROL  REG BITS definitions ######//
/* Bit position in "Control Reg" */
//#define ENDIAN_DMA_BIT          8 //Endianness of DMA data words  (0:little , 1: Big Endian)
#define CHOP_EN_BIT             10 // State of Chop, if equals 1 chop is ON if 0 it is OFF
#define CHOP_DEFAULT_BIT        11 // Value of Chop case CHOP_STATE is 0
#define ILCK_F_OUT_EN_BIT       13
#define ILCK_QF_OUT_EN_BIT      14
#define DMA_DATA32_BIT          15  // 0: 16 bit , 1: 32 Bit data

#define STREAM_EN_BIT   20 // Streaming enable
#define ACQ_EN_BIT      23
#define STRG_BIT        24 // Soft Trigger

typedef struct _CONTROL_FLDS {
  volatile uint32_t rsv0:8,
    BIG_ENDIA:1, rsv1:1, CHOPP_EN:1, CHOPP_DEF:1, rsv2:1, ilckF_EN:1, ilckQF_EN:1, data32:1,
    rsv3:7, ACQ_EN:1,
    SFT_TRIG:1, rsv4:7,
    rsv5:8; // msb

} CONTROL_FLDS;

struct atca_eo_config {
  volatile int32_t offset[ADC_CHANNELS];
};

struct atca_wo_config {
  volatile float offset[ADC_CHANNELS];
};

struct atca_ilck_params {
  volatile float fval[N_ILOCK_PARAMS];
};

// TODO : to be used.
#ifdef __BIG_ENDIAN_BTFLD
#define BTFLD(a, b) b, a
#else
#define BTFLD(a, b) a, b
#endif

#ifndef VM_RESERVED
#define VMEM_FLAGS (VM_IO | VM_DONTEXPAND | VM_DONTDUMP)
#else
#define VMEM_FLAGS (VM_IO | VM_RESERVED)
#endif

#undef PDEBUG /* undef it, just in case */
#ifdef ATCA_DEBUG
#ifdef __KERNEL__
/* This one if debugging is on, and kernel space */
#define PDEBUG(fmt, args...) printk(KERN_DEBUG "atcav2: " fmt, ##args)
#else
/* This one for user space */
#define PDEBUG(fmt, args...) fprintf(stderr, fmt, ##args)
#endif
#else
#define PDEBUG(fmt, args...) /* not debugging: nothing */
#endif

#undef PDEBUGG
#define PDEBUGG(fmt, args...) /* nothing: it's a placeholder */

#endif /* _ATCA_V2_DAQ_H_ */
