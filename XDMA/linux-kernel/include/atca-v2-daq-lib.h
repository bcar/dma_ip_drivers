/**
* @file atca-v2-daq-lib.h
* @brief Header file for ATCA V2 MIMO board API functions
* @author Bernardo Carvalho
* @date 01/06/2021
*
* @copyright Copyright 2016 - 2021 IPFN-Instituto Superior Tecnico, Portugal
* Licensed under the EUPL, Version 1.2 only (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence, available in 23 official languages of
* the European Union, at:
* https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12
*
* @warning Unless required by applicable law or agreed to in writing, software
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* @details
* ATCA V2 PCI ADC  device driver
*/

#ifndef _ATCA_V2_DAQ_LIB_H_
#define _ATCA_V2_DAQ_LIB_H_
/*---------------------------------------------------------------------------*/
/*                        Standard header includes                           */
/*---------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>

//#define _BSD_SOURCE // for usleep function
#include <fcntl.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

/*---------------------------------------------------------------------------*/
/*                        Project header includes                            */
/*---------------------------------------------------------------------------*/
#include "atca-v2-daq.h"

//#define DMA_SIZE 2097152 // 2MB 4194304 // 4Mb
//#define DMA_SIZE  4194304 // 4Mb
//#define DMA_SIZE  0x003FF000 // 4190208 B
//#define NUM_CHAN 32      // nr of 16bit data fields per sample. Can be 32 channels or 2 counters and 30 channels, etc.
#define TIMERVALUE 1000000
#define N_AQS SIZE_DEFAULT/(ADC_CHANNELS * 2)  // nr samples per buffer

/**
* @brief Writes the WO Offset parameters
* @param[in] map_base virtual address return from mmap function
* @param[in] pointer to a struct
* @return 0 if OK
*/
int write_wo_offsets(void *map_base, struct atca_wo_config *pConfig);

/**
*@brief Writes the EO Offset parameters
* @param[in] map_base virtual address return from mmap function
* @param[in] pointer to a sctruc
* @return 0 if OK
*/
int write_eo_offsets(void *map_base, struct atca_eo_config *pConfig);

/**
* @brief Writes the Interlock parameters on FPGA
* @param[in] map_base virtual address return from mmap function
* @param[in] pointer to a sctruc with parameter values
* @return 0 if OK
*/
int write_ilck_params(void *map_base, struct atca_ilck_params *pConfig);

/**
* @brief Read the Interlock parameters stored on FPGA
* @param[in] map_base virtual address return from mmap function
* @param[in] pointer to a sctruc with parameter values
* @return 0 if OK
*/
int read_ilck_params(void *map_base, struct atca_ilck_params *pConfig);

/**
* @brief Writes the
* @param[in]
* @param[in]
* @return 0 if OK
*/
void save_to_disk2(int nrOfReads, short *acqData);

// int atcainit_device_c2h_1(unsigned int dev_num);

int atca_open_c2h_1(unsigned int dev_num);
/**
* @brief open C2H DMA CHANNEL 1 for decimated data
* @param[in] dev_num Board Number
* @return file descriptor on BAR1 C2H_1 (DMA reads, decimated data)
*/

/**
* @brief reset ATCA board and prepare acquisition
* @param[in] dev_num Board Number
* @param[in] chopper_period Chopper Period ( Duty Cycle is set to 50 %).
*              Value should be even,  Value  =  0 , stops adc chopping
*              For non -chopped modules,  it should always be zero
* @param[out] fd0 pointer to file descriptor on BAR0 (Fpga regs)
* @param[out] fd_bar1_c2h_0 pointer to file descriptor on BAR1 (DMA reads, raw data)
* @return virtual address of BAR0 to call other API functions
*/
void * atca_init_device(unsigned int dev_num, unsigned int chopper_period,
        int *fd_bar0, int *fd_bar1_c2h_0);

/**
* @brief Perfoms a FPGA "soft" reset (stops aquisition and reset all internals to default
* @param[in] map_base virtual address return from mmap function
* @return 0 if OK
*/
int atca_soft_reset(void * map_base);

/**
* @brief Perfoms a FPGA "hard" reset (stops aquisition and reset all internals to default)
*               Caution: The PCIe host PC complex will loose connectivity
* @param[in] map_base virtual address return from mmap function
* @return 0 if OK (It will not return...)
*/
int atca_full_reset(void * map_base);

/**
* @brief  Enable of interlock "F" output
* @param[in] map_base virtual address return from mmap function
*  This should not be used whith non-chopped ADC modules intalled on board
* @return 0 if OK
*/
int enable_interlockF(void *map_base);

/**
* @brief  Disable of interlock "F" output
* @param[in] map_base virtual address return from mmap function
* @return 0 if OK
*/
int disable_interlockF(void *map_base);

/**
* @brief  Enable of interlock "QF" output
* @param[in] map_base virtual address return from mmap function
*  This should not be used whith non-chopped ADC modules intalled on board
* @return 0 if OK
*/
int enable_interlockQF(void *map_base);

/**
* @brief  Disable of interlock "QF" output
* @param[in] map_base virtual address return from mmap function
* @return 0 if OK
*/
int disable_interlockQF(void *map_base);

/**
* @brief  Enable Chopping on ADC modules
* @param[in] map_base virtual address return from mmap function
*  This should not be used whith non-chopped ADC modules intalled on board
* @return 0 if OK
*/
int enable_chopping(void *map_base);

/**
* @brief  Disable Chopping on ADC modules
* @param[in] map_base virtual address return from mmap function
* @return 0 if OK
*/
int disable_chopping(void *map_base);

/**
* @brief Enable little endian in  DMA data transfer packets (Default setting)
* @param[in] map_base virtual address return from mmap function
* @return 0 if OK
*/
int atca_use_little_endian(void *map_base, int use32bit);

/**
* @brief Enable big endian in  DMA data transfer packets
* @param[in] map_base virtual address return from mmap function
* @return 0 if OK
*/
int atca_use_big_endian(void *map_base, int use32bit);

/**
* @brief Switch between 32 and 16 bit acquisition
* @param[in] map_base virtual address return from mmap function
* @param[in] use32bit True if using 32 bit acquisition, false for 16 bit acquisition
* @return 0 if OK
*/
int atca_use_data32(void *map_base, int use32bit);

/**
* @brief Send a software Trigger to the configured board
* @param[in] map_base virtual address return from mmap function
*/
int  atca_soft_trigger(void *map_base);

/**
* @brief Stops ongoing Data acquisition, streaming and processing
* resets the also Software  Trigger bit
* @param[in] map_base virtual address return from mmap function
*/
int  atca_stop_acq(void *map_base);

/**
* @brief Arms Data acquisition, streaming and processing,
* using previous FPGA configuration
* Board will be waiting for HW/SW Trigger
* @param[in] map_base virtual address return from mmap function
*/
int  atca_arm_acq(void *map_base);

/**
* @brief Writes the chop Period
* @param[in] map_base virtual address return from mmap function
* @param[in] val 16-bit value of the period in (0.5 us)
*
* @return
*/
int write_chopp_period(void *map_base, uint16_t period);

/**
* @brief Reads a 32 bit value on the Channel Mask register in BAR0 space
* @param[in] map_base virtual address return from mmap function
* @return Channel Mask Reg value
*/
int read_channel_mask(void *map_base);

/**
* @brief Reads a 32 bit value on the Channel Mask register in BAR0 space
* @param[in] map_base virtual address return from mmap function
* @param[in] val 32-bit value to write
* @return 0 if OK
*/
int write_channel_mask(void *map_base, uint32_t val);

/**
* @brief Reads a 32 bit value on the control_reg register in BAR0 space
* @param[in] map_base virtual address return from mmap function
* @return Control Reg value
*/
uint32_t read_control_reg(void *map_base);

/**
* @brief Writes a 32 bit value on the control_reg register in BAR0 space
* @param[in] map_base virtual address return from mmap function
* @param[in] val 32-bit value to write
* @return 0 if OK
*/
int write_control_reg(void *map_base, uint32_t val);

/**
* @brief Writes a 32 bit value on a register in BAR0 space
* @param[in] map_base virtual address return from mmap function
* @param[in] target offset register address (must be multiple of 4)
* @param[in] val 32-bit value to write
* @return 0 if OK
*/
int write_fpga_reg(void *map_base, off_t target, uint32_t val);

/**
* @brief Reads a 32 bit value from a register in BAR0 space
* @param[in] map_base virtual address return from mmap function
* @param[in] target offset register address (must be multiple of 4)
* @return read value
*/
uint32_t read_fpga_reg(void *map_base, off_t target);

/**
* @brief Reads a 32 bit value from the status_reg register in BAR0 space
* @param[in] map_base virtual address return from mmap function
* @return read value
* @return 0 if OK
*/
uint32_t read_status_reg(void *map_base);

/**
* @brief Reads the value of Firmware Version register in BAR0 space
* @param[in] map_base virtual address return from mmap function
* @return error (-1) if FW not compatible with SW, FW Version otherwise,
*/
int  read_fw_version(void *map_base);

/**
* @brief Reads a 32 bit value from the Timestamp register in BAR0 space
*  and returns a formatted string
* @param[in] map_base virtual address return from mmap function
* @return read value
*/
char * read_fw_timestamp_str(void *map_base);

/**
* @brief Reads a 32 bit value from the Timestamp register in BAR0 space
* @param[in] map_base virtual address return from mmap function
* @return read value
*/
uint32_t  read_fw_timestamp(void *map_base);

/**
* @brief prints a progress indicator Bar
* @param[in] percentage (0.0 to 1.0)
* @return
*/
void printProgress(float percentage);

/**
* @brief extracts 64 sample counter value embedded in the DMA data packets.
*    (valid only in 32-bit DMA mode)
* @param[in] pointer to initial sample array
* @return counter value
*/
uint64_t get_counter_64bit(uint32_t *pSamp);

/**
* @brief extracts a 16 bit counter value with the number of 
* words ( 1 "word" = 16 Bytes) presently in the FPGA buffer.
*    (valid only in 32-bit DMA mode)
*    The number should always be < 32768, otherwise a buffer overflow 
*    occurs
* @param[in] pointer to initial sample array
* @return counter value
*/
uint16_t get_fifo_cnt(uint32_t *pSamp);

/**
* @param[in] map_base virtual address return from mmap function
* @brief extracts one float embedded in the DMA data packets.
*    (valid only in 32-bit DMA mode)
* @param[in] pointer to initial sample array
* @param[in] offset to float position in sample data
* @return float value
*/
float get_float_val(uint32_t *pSamp, int offset);

/**
* @param[in] map_base virtual address return from mmap function
* @brief extracts one float val in the DMA C2H_1 data packets.
* @param[in] pointer to initial sample array
* @param[in] offset to 32-word position in sample data
* @return float value
*/
float get_c2h1_float_val(uint32_t * pSamp, int offset);

/**
* @brief extracts one double val in the DMA C2H_1 data packets.
* @param[in] pointer to initial sample array
* @param[in] offset 32-bit word position in sample data
* @return float value
*/
double get_c2h1_double_val(uint32_t * pSamp, int offset);

/**
* @brief extracts two bits  for the fifo/dma status encoded in the DMA data packets.
*    (valid only in 32-bit DMA mode)
* @param[in] pointer to initial sample array
* @return uint8_t byte value
*/
uint8_t get_fifo_dma_status (uint32_t *pSamp); 

/**
* @brief extracts one bit for the chopper phase encoded in the DMA data packets.
*    (valid only in 32-bit DMA mode)
* @param[in] pointer to initial sample array
* @return uint8_t byte value
*/
uint8_t get_chopper_phase (uint32_t *pSamp);

/**
* @brief extracts a two float array with temperature in ºCelsius of the TMP101 sensors installed in FPGA board.
* @param[in] map_base virtual address return from mmap function
* @param[in] pointer to the float array (minimum size =2)
* @return 0 if OK
*/
int get_tmp101_vals (void *map_base, float *temps);

/**
* @brief extracts a two float array with temperature in ºCelsius of the MCP9808 sensors installed in FPGA board.
* @param[in] map_base virtual address return from mmap function
* @param[in] sensor pair number: 0: U19/U20, 1: U21/U22, 2: U23/U24, 3: U25/26
* @param[in] pointer to the float array (minimum size =2)
* @return 0 if OK
*/
int get_mca9808_vals(void *map_base, int pair, float *temps);

#endif /* _ATCA_V2_DAQ_LIB_H_ */
/*---------------------------------------------------------------------------*/

