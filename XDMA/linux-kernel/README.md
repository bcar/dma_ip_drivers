# Linux drivers, API lib and test apps for the ATCA-MIMO-ISOL V2 Board

### Project forked from the [XILINX PCIe XDMA IP](https://www.xilinx.com/support/documentation/ip_documentation/xdma/v4_1/pg195-pcie-dma.pdf) [Linux Reference driver](https://github.com/Xilinx/dma_ip_drivers) main project, and including API for this board.
* See how to fork this repository from Xilinx and [update](https://stackoverflow.com/questions/50973048/forking-git-repository-from-github-to-gitlab) it.

## This software applies to the ATCA-MIMO-V2 Board [FPGA Project](https://gitlab.mpcdf.mpg.de/bcar/atca-mimo-v2-adc)
1. Presently this firmware must be compiled with Xilinx Vivado 2022.1 Tools
2. To compile the Firmware, install [Vivado](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vivado-design-tools/archive.html) and follow the Steps described [here](https://gitlab.mpcdf.mpg.de/bcar/atca-mimo-v2-adc)

### Installation Steps

* Clone, change to driver folder and compile.  
Driver was tested in Fedora/Centos 3.10.x and
Debian 4.1.x, 5.10.x kernels, but should compile on newer versions.
```
git clone https://gitlab.mpcdf.mpg.de/bcar/dma_ip_drivers
cd dma_ip_drivers/XDMA/linux-kernel/xdma
make
```
* Install driver and give user permissions to device nodes:  
More information on [Xilinx Forums](https://github.com/RHSResearchLLC/XilinxAR65444/tree/master/Linux/Xilinx_Answer_65444_Linux_Files)
```
sudo make install
sudo cp 60-xdma.rules /etc/udev/rules.d/
sudo cp adc_xdma.conf /etc/modprobe.d/
```
After the last step in Debian you might need to update the system initram (not needed in Fedora):  
```
sudo update-initramfs -u -k all
```

You might need to adjust the read time `c2h_timeout` parameter on  adc_xdma.conf file, i.e.
the max time in seconds the board can be waiting for a hardware trigger.  
DMA poll mode option was not fully tested with this hardware. 

* On first use verify driver/hardware installation:  
```
sudo modprobe adc_xdma
ls -al /dev/adc_xdma0_*
```
Several nodes should appear with user read permissions. Troubleshooting can be done with `sudo dmesg`.

* Care should be taken on using the right SW/FW versions for compatibility.  
The API funtion *read_fw_version()* will return error (-1) in case SW/FW don't match. 
(See SHAPI doc for version numbering rules)
* To reload device driver when the FPGA is reprogrammed and the PC is not rebooted, 
do `sudo ./rescan_pcie.sh`. (Comment the last line if you don't need CPU IRQ isolation)

### User Applications
* Main C/Cpp software code is now maintained in `XDMA/linux-kernel/jni-lib/src/native/api` folder.
* A minimal application to get 16 bit data consists of these lines :
```
#include "atca-mimo32-v2-device.h"
namespace atca_test {
    int deviceNumber = 0;
	int nSamples = 32768;
    int dataMbSize = 128;
    unsigned int chopperPeriod = 2000;
    uint32_t channelMask = 0xFFFFFFFF;
    int timeoutMiliseconds = 5000;
    bool use32bits = false;
    uint32_t status;

	atca::AtcaMimo32V2 device(deviceNumber, dataMbSize, DMA_SIZE_DEFAULT);
	int rc = device.open(use32bits, chopperPeriod, channelMask);
    rc = device.readStatus(&status);
    printf("FPGA Status 0x%08X\n", status);
    rc = device.armAcquisition();
	rc = device.sendSoftwareTrigger();

     size_t buffSize = nSamples * ADC_CHANNELS * sizeof(uint16_t);
     uint8_t* buffer = (uint8_t*) malloc(buffSize);
	 rc = device.read(buffer, nSamples, timeoutMiliseconds);

     rc = device.stopAcquisition();
     rc = device.close();
	 free(buffer);
} // namespace atca_test
```
* A more general purpose application example is at `atca-mimo32-v2-test.cpp`.  
* Compile with `make acq_from_device`. Run `./acq_from_device -h` to see options:  
```
usage: ./acq_from_device [OPTIONS]

Read ATCA adc data, optionally save output to a file

  -d (--device) <n> 	 specifies device number, default is 0
  -s (--size) <n> 	 specifies size of a single transfer in bytes, default 2048 kB
  -g (--config) <file> 	 specifies config file name
  -c (--count) <n> 	 specifies number of DMA transfers, default is 1
  -f (--file) <file> 	 specifies file to write the acquired data. Default is no write
  -b (--data_mode) <m> 	 specifies data type, 0: 16-bit, 1:32-bit data. Default: 0
  -e (--eop_flush) 	 end dma when ST end-of-packet(eop) is rcved
  -p (--chopP) <n> 	 enables and specifies chopping period. Default: no chopping
  -m (--mask) <hex> 	 specifies channel mask. Default: 0xFFFF (all channels active)
  -h (--help) 	         print usage help and exit
  -v (--verbose) 	     verbose output

Example:
./acq_from_device -b 1 -m 0xFF -p 2000 -s 0x400000 -c 2 -f data/out_32.bin

Return code:
    0: all bytes were dma'ed successfully
       * with -e set, the bytes dma'ed could be smaller
  < 0: error

```

*  An example of a config file for the Interlock processing parameters is given bellow:
```
# (comment)
# W7-X ATCA V2  Diamagnetic Interlock System Acquisition and processing Parameters

# EO and WO compensation offsets
adc_modules = {
    # Eight integer numbers ( signed 18 bit). Values used in ATCA V1 Interlock board:
    eo_offsets= [-99, -108, -234, -133,           -26, 0, 0, 0];

    # Eight float numbers
    wo_offsets=[0.5452, 0.429, 0.0782, 0.5993,    0.4101, 0.0,  0.0, 0.0];
};

# Interlock Processing parameters
dsp_processing = {
    # Eight float numbers. Values used in ATCA V1 Interlock board:
    chann_coeff =[-8.2965e-05, -8.2820e-05,  -8.2790e-05, -8.2823e-05,     7.5544e-05, 0.0,  0.0, 0.0];

    # Two float numbers, first is 'F' threshold, second is 'dF' (derivative) threshold
    adder_coeff = [700, 1000.0];
  };

```

* A second test application and APIi can be compiled and tested with:
```
cd ../apps
make atcadaq
./atcadaq -524 -sw

Usage: atcadaq -524 |-1048 |-2096 -hw|-sw  -ch_on |-ch_off [dev_nr]

The full parameter list is explainded here:
-524 |-1048 |-2096 choses the data acquisition time in ms 
(depending on main memory of host pc)

-hw|-sw switches between hardware triggering with an extenal faling edge (LVTTL)
on Simple-RTM Input R4 or software triggering with    pressing simply the "return key"

-ch_on |-ch_off switches on/off the 2KHz chopper_phase signal on 
[dev_nr] optional device nr , currently only 0 or 1

```

### Read-Timeout setting

* Configurable read-timeout setting now via "sys" filesystem:
    - In the older driver there was an IOCTL code to set the "read timeout”, when the app is hanging in the first read  and waiting for the hardware trigger.  
    - Now it is a parameter of the kernel module which can be set/changed (as root) within the "/sys" filesystem in "/sys/module/adc_xdma/parameters/c2h_timeout"
    - To check the current actual value, use `cat /sys/module/adc_xdma/parameters/c2h_timeout`
    - To temporarily change the timeout (until the next reboot) you can set a new value e.g. 20 seconds 
in a sudo shell with `echo 20 > /sys/module/adc_xdma/parameters/c2h_timeout`
* For permanent changes we have to use the file `/etc/modprobe.d/adc_xdma.conf`,
```
# Check options with
# cat /sys/module/adc_xdma/parameters/c2h_timeout
# change file /etc/modprobe.d/adc_xdma.conf

options adc_xdma c2h_timeout=5
#options adc_xdma c2h_timeout=5 poll_mode=1
```

### Main Register Map
The FPGA includes an PCIe Endpoint (included in the Xilinx XDMA IP core),
which includes two BAR Memory Regions accessible through host-driven 
PIO memory read/write 32-bit operations (non-cacheable).  
1. BAR 0 region maps a series of internal registers that are used to govern the Board operation and are explained in next sections
of this page.  
The BAR 0 register map is shown on the  table below.  
2. BAR 1 region is reserved to Xilinx DMA IP core. It is used in the internals of Xilinx device driver and it 
should not be touched in normal ATCA board operations.
3. BAR 0 register map conforms to the Standard [SHAPI](https://www.picmg.org/wp-content/uploads/PICMG_MTCA_DG_1-Standard_Hardware_API_Design_Guide_RELEASED-2017-01-09-002.pdf)
It includes a Standard Device Identification register set and One SHAPI Module set for DAQ control and monotoring.
    * The first set (SHAPI Main Module) content is rigidly defined by SHAPI, 
and is fully described in Section 3.1.2 of the standard
    * The relevant registers for this firmware are:

#### SHAPI Main Module

| Name | Offset Address | Content | Description | Read/Write Access|
| --- | :-------:     | ------   |--------    | :----: |
|Magic Word/SHAPI Version| 0x00|0X53480100 |  Current SHAPI version | ro |
|First Module Address| 0x04| 0x40 | base address of the DAQ FW module| ro |
| Firmware Version | 0x10| 0x02020013 <br/> (Current version, but can change) | Major/Minor/Patch :no_entry_sign:| ro |
| Firmware Timestamp | 0x14|  &#45; | UNIX timestamp of compilation | ro |
| &#45; | &#45;| &#45; |&#45; | &#45;|
|SHAPI Device Capabilities  | 0x24| 0xC0000001 |Bit0: 1’, it’s possible  to  swap  between  endianness formats<br/>Bit1: 1’, is  able  to  detect  a  RTM  board connected to it<br/>‘1’, it’s possible  to  swap  between  endianness formats<br/>Bit30:  ‘1’, soft reset of Device is available <br/> Bit31:  ‘1’, full reset of Device is available | ro |
|SHAPI Device Status  | 0x28 | 0/1  | Bit0: 0-Little Endian, 1- Big Endian<br/> Bit30: 1-Device is running a soft reset| ro |
|SHAPI Device Control  | 0x2C | 0/1 | Bit0: 0-Little Endian, 1- Big Endian<br/> Bit30: Writing ‘1’ will begin a soft reset routine<br/> Bit31: Writing ‘1’ will begin a hard reset routine :warning:| rw |
| &#45; | &#45;| &#45; |&#45; | &#45;|
|Scratch Register  | 0x3C | Default: 0x000000BB  | For testing purposes | rw |

:warning: After a hard reset Device may loose PCIe connectivity, so a PCIe rescan should be done.  
:no_entry_sign: According to SHAPI document the Firmware Major/Minor version numbers **must** conform to 
respective API software version. At this date no backward compatibility can be guaranteed in either way. 
Minor Firmare upgrades should not require SW updates. User application should use API function *read_fw_version()* for checking.


#### DAQ Second FW Module

| Register Name | Offset Address | Default Content | Description | Read/Write Access|
| --- | :-------:     | ------   |--------    | :----: |
|Magic Word/SHAPI Version| 0x040|0X534D0100 |  Current SHAPI version| ro :information_source: |
| &#45; | &#45;| &#45; |&#45; | &#45;|
|Module Control  | 0x60 | 0 | Not used | ro |
| &#45; | &#45;| &#45; |&#45; | &#45;|
|DAQ Status  | 0x80 | 0x007F00CE | DAQ FW Status bits <br/> See Table Below  | ro |
|DAQ Control  | 0x84 | 0x00000000| DAQ Control Bits  <br/> See Table Below | rw |
|Chopp period/phase  | 0x88 | 0x07D003E8<br/> (1kHz chopping Freq., 50% Duty Cycle)  | 16 MSB: period in samples<br/>16 LSB:half period | rw |
|MAX DMA bytes | 0x88 | 0x00400000 <br/> (4MBytes) | Max bytes per *read()* call| ro |
|DMA TLP bytes | 0x8C | 0x0020 <br/> (32 Bytes) | Max bytes per PCIe TLP | ro |
|Channel Mask Reg| 0x94 |  0xFFFFFFFF | 32 bit Mask. See sect. below  <br/> Each bit: 0-Chan disabled (All zeros)<br/> 1: Channel Enabled | rw |
|IPMC :information_source: I2C Reg 0 | 0x98 | IPMC I2C 8 bit read only Regs 0-3 | Writen by IPMC with ATCA slot number  | ro |
|IPMC TMP101 | 0x9C | IPMC I2C TMP101 sensors 0/1 data | Writen by IPMC   | ro :bulb:|
|IPMC MCP9808_0_1 | 0xA0 | IPMC I2C MCP9808 sensors U19/U20 data | Writen by IPMC   | ro :bulb:|
|IPMC MCP9808_2_3 | 0xA4 | IPMC I2C MCP9808 sensors U21/U22 data | Writen by IPMC   | ro |
|IPMC MCP9808_4_5 | 0xA8 | IPMC I2C MCP9808 sensors U23/U24 data | Writen by IPMC   | ro |
|IPMC MCP9808_6_7 | 0xAC | IPMC I2C MCP9808 sensors U25/U26 data | Writen by IPMC   | ro |
|IPMC I2C Reg 1 | 0xB0 | 0x00005345 | Bits 0-7: Sends Master Address to IMPC <br/>  Bits 8-15: Magic number to IMPC <br/> Bit31: Requests an FPGA hard reset (TE0741 RESIN pin low)| rw |
| &#45; | &#45;| &#45; |&#45; | &#45;|
|EO Offset CH0  | 0x100 | 0 | E Offset Channel 0, integer.<br/> The 14 MSB are discarded. See equ. 1 | rw |
|EO Offset CH1  | 0x104 |  | E Offset Channel 1 | rw |
| &#45; | &#45;| &#45; |&#45; | &#45;|
|EO Offset CH31 | 0x17C |  | E Offset Channel 31 | rw |
| &#45; | &#45;| &#45; |&#45; | &#45;|
|WO Offset CH0  | 0x180 | 0.0 | W Offset Channel 0. Stored in Q15.16 format.<br/> See equ. 2.<br/> The SW API accepts float values and converts them to Q15.6.  | wo|
|WO Offset CH1  | 0x184 |  0.0  | W Offset Channel 0 | rw |
| &#45; | &#45;| &#45; |&#45; | &#45;|
|WO Offset CH31  | 0x1FC |  | WO Offset Channel 31 | rw |
| &#45; | &#45;| &#45; |&#45; | &#45;|
|Interlock 0 coef.  | 0x200 | 0.0 | "F" calculation $`Param_0`$ coeff. <br/> See equ. 3. Stored in 32 bit float| rw |
|Interlock 1 coef.  | 0x204 |  | "F" calculation $`Param_1`$ | rw |
| &#45; | &#45;| &#45; |&#45; | &#45;|
|Interlock 7 coef.  | 0x21C | 0.0 | "F" calculation $`Param_7`$ | rw |
| &#45; | &#45;| &#45; |&#45; | &#45;|
|Interlock "F" Threshold  | 0x220 |  0.0 |  "F" $`\; Threshold_{interlock}\;`$  value. See equ. 4. Stored in 32 bit float | rw |
|Interlock "der. F" Threshold  | 0x204 |  0.0 |  "derivative F"  $`\; Threshold_{quench} \;`$  value. <br/>
Stored in 32 bit float | rw |

:information_source: Registers marked 'ro' are **read only**, 'rw' are **read/write**, and 'wo' **Write only**  
:information_source: IPMC is  "Intelligent Platform Management Controller" located on the new MIMO-ISOL_V2 ATCA ADC Board, 
that controls infrastructure tasks like correct Power-Up/Down cycles, monitors several temperature sensors. The respective code can be seen 
[here](https://gitlab.mpcdf.mpg.de/mmz/ipmc-atca)
and voltages to guarantee proper function of the main ATCA board.
Registers marked 'ro' are **read only**, 'rw' are **read/write**, and 'wo' **Write only**  
:bulb: To get temperature values, use the API functions *get_tmp101_vals()* and *get_mca9808_vals()* respectively. 

#### DAQ **Status Register** Bits

| Field | Bits      | Description | Values|
| ---   | :-------:  |--------    | :----: |
| Reserved | 31-23 | &#45;      | &#45;  |
| Master | 22    | Board is CLOCK Master in ATCA crate <br/> **ONLY ONE** board can be Master | 0:Slave, 1:Master|
| MSI IRQ | 21 | MSI Interrupts are enabled by PCIe tree | Should always be '1'  |
| FPGA IDLAY Ready | 20 | FPGA data delay IP block ready  | Should always be '1'  |
| DAQ clock (50MHz) Locked | 19 | Main DAQ CLK is locked to one source  | Should always be '1'  |
| ATCA clock (10MHz)  Locked | 18 | ATCA CLK is locked in Board  | Should always be '1'  |
| TE0741 clock (100MHz) Locked | 17 | TRENZ CLK is locked in Board  | Should always be '1'  |
| RTM clock (10MHz)  Locked | 16 | RTM 10Mhz CLK is locked in Board  | Should be '1' when  TTE clock system is connected to ATCA RTM |
| Reserved | 15 | &#45;      | 0  |
| DMA FIFO Overflow Alert| 14 | '1' when Fifo word count > 32735  | At least one Alert occurred :star: |
| Double Float Overflow  | 13 | Accumulator Overflow in Sum Equ. 3 (Sum of channels 1,3,5,7) :bulb:  | At least one Overflow occurred :star:  |
| Double Float Underflow | 12 | Input Overflow in Sum Equ. 3 (Sum of odd number channels)  :bulb: | At least one Underflow occurred :star:  |
| Double Float Overflow  | 11 | Accumulator Overflow in Sum Equ. 3 (Sum of channels 0,2,4,6)   | At least one Overflow occurred :star: |
| Double Float Underflow | 10 | Input Overflow in Sum Equ. 3, (Sum of even number channels)   | At least one Underflow occurred :star:|
| DAQ is ON  | 9 | DAQ and DMA are enabled and triggered  |  1: DAQ in ON  |
| Reserved | 8 | &#45;      | &#45;  |
| ATCA Slot Address | 7-0 | &#45;      | HA address written by IPMC <br/> 0xCE for IPP-HGW Lab ATCA Crate|

* :bulb: See details on these bits in Xilinx
Floating Point IP core [documentation](https://www.xilinx.com/support/documentation/ip_documentation/floating_point/v7_1/pg060-floating-point.pdf).
* :star:  Note: These bits are of **Latch** type, meaning that if one or more events took place during an acquisition cycle the 
respective bit stays ON until acquisition is stopped.  
 
Applications should read the **Status Reg** after each/last *read()* and check for possible errors before changing **Control Reg**.

#### DAQ **Control Register** Bits

| Field | Bits | Description | Values|
| --- | :----: | ------      | :----: |
| Reserved | 31-25 | &#45;      | &#45;  |
| Soft Trigger | 24 | Start Acquisition & DMA      | 0 to 1 change: Start DAQ  |
| DAQ Enable | 23 | Enables/ Disables DAQ     | 0:Stop & Reset DAQ and Streaming.<br/> 1: Enable DAQ |
| Decimated DAQ Enable  | 20 | Enables Integral Streaming     | Not yet fully Tested |
| 32 bit Data Mode | 15 | Enables 32 data transfers      | 0: 16 bit<br/>  1:18bit left aligned to 32  |
| Interlock Quench Output | 14 | Enables output pin on RTM D6    | 0: Disabled, D6 pin is chopper phase<br/> 1: Enable  |
| Interlock Level Output | 13 | Enables output pin on RTM pin D7    | 0: Disabled <br/> 1: Enable  |
| Chop default state | 11 | Signal Phase when Chopp is disabled       | 0:Non-inverted<br/> 1:Inverted  |
| Chopping Mode | 10 | Chopping mode Enable       | 0:Non-chopped<br/> 1:Chopped  |
| Reserved | 9-0 | &#45;      | &#45;  |

### Channel Mask Register

* 32-bit register to enable or disable an ADC channel individually.

The main purpose of the Channel Mask Register is to force  ADC channels to generate data values with exactly zero, independent if an ADC module is assembled or not.  Channels without an assembled ADC module usally generate random data values. The default state of the Channel Register is 0xFFFFFFFF (all 32 channels enabled). With the API functions *read_channel_mask()* and *write_channel_mask()* the user can enable/disable each channel. 
```
0x   F    F    F    F    F    F    F    F       e.g. 0x00000011 -> channels 1 + 5 enabled, others disabled
   1111 1111 1111 1111 1111 1111 1111 1111           0xFFFF0000 -> channels 17-32 enabled, 1-16 disabled
   |                                     |
  msb                                   lsb
   |                                     |
 ch32 ..................................ch01
```

Note: The Channel Mask has no effect to the total number of tranferred channels/bytes in a DMA packet.  
A DMA packet transferred from the ATCA-FPGA to the Host PC via PCIe always contains 32 channels, 
 *disabled channels* all data values are zero, i.e. PCIe data rate is not reduced.

### Data Streaming to HOST PC

1. To stream the ADC data to the PC, the board implements two **Streaming** modes:
  * **16 bit** Mode. 32 channels are transmitted as 16 signed integers. Raw adc data is truncated to the 16 MSB bits.
    - Additionally in the **chopping mode** the *chop phase* is transmitted with the 
        1 LSB of the channel 31, sacrificing one aditional resolution bit.
    - DMA transmission bandwidth amounts to aprox. 128 MB/sec
  * **32-bit**  mode:  The 32 channels $`s_i[n]`$ are transmitted as 18 signed integers
        **left-justified**. 
    -  To recover the correct amplitude in LSB the software  must shift data by 14 bits right.
        (or divide by $`2^{14}`$ )
    - Example of C-code to extract the correct 18-bit ADC value (with sign-extension):

```cpp
static int32_t get_adc_val(uint32_t * pSamp, int offset)
{
 const int sign_bit = 0x00020000;
 uint32_t val = 0;
 val = ((pSamp[offset]) >> 14);
 if (val & sign_bit) {      // Check if sign-bit is set
   val = val | 0xFFFC0000;  // Sign-bit extension
 }
 return (int32_t) val;
}
```

#### Example of Python 3 to extract the correct 18-bit ADC value and Chopp phase

```python
import numpy as np
SCALE_32 = 2**14        # Data is shifted 14 bits

# Function to read binary data, in the same format as the 32-bit mode DMA packets
def get_data32_raw(filename):
    data = np.fromfile(filename, dtype='int32')
    data_mat = np.reshape(data, (32, -1), order='F')
    return data_mat

# Function to extract embedded data on 32-bit packets
def get_data32(data_mat):
    data_scaled = data_mat // SCALE_32
    chopp = data_mat[31] & 0x0001
    return data_scaled, chopp
```

- More Python examples [here](XDMA/linux-kernel/tests/atca_data_functions.py)
    - DMA transmission bandwidth amounts to aprox. 256 MB/sec.
    
#### In 32-bit mode the 12 LSB in each 32-word is used to transmit extra data, including:
1. A 64-bit Unsigned Integer ** sample Counter ** (Counter),
2. Float number (32 bit IEEE Standard 754) with the Interlock Function (equation 3.a) value (Float_3a next Table)
3. Float number with equation 3.b value (Float_3b)
4. Float number with equation 4.a value (Float_4a)
5. Float number with equation 4.b value (Float_4b)
6. A A 16-bit Unsigned Integer **DMA FIFO** word count** (FifoCount).
  * Indicates the number of words present into the FIFO waiting to be transfered to Host PC.
  * When this numbers is close to 32768, there is a danger of loosing samples.
7. Chopped phase signal and Interlock Output status bit.
8. The 32 Integrated Channels are decimated (1:32) and transmitted sequentially, i.e. first packet sends Int_Ch_0, 
second, Int_Ch_1 ... and so forth. Packet number 32 restarts and sends next sample of Int_Ch_0
9. To check correct indexing, in case of lost packets, this Index is also transmitted.

  :bulb: The API function calls  *get_counter_64bit()*, *get_float_val()*, and *get_fifo_cnt()* can be used to extract
   the values from the packet.  
  :warning: All applications reading streaming data must always read data with buffer size in multiples of 64 byte in 16-bit Mode,
  and 128 bytes in 32-bit Mode.  
  :warning: Note that lower *read()* sizes **increase** the Interrupt rate on the host PC. In 32-bit mode, a 128 buffer size will
  generate 2 million irqs/s, which is completly out of reach for a Linux PC to handle.  
  In the opposite way high read buffer sizes *increase* packet latency.
  In general read buffers should have $`256kB \le size \le 8MB`$

#### Streaming Data packet Format
  1. **32-bit transfer mode**   
  For each sample a new data packet with:warning: 128 bytes is assembled:

  | Byte Offset | Bits 31-14 | Bits 13-8 | Bits 7-0|
| :---: | :---- | ------      | :---- |
| 0 | Channel 0 Data| 0b000000    | Counter64[7:0]  |
| 4 | Channel 1 Data| 0b000000    | Counter64[15:8]  |
| 8 | Channel 2 Data| 0b000000    | Counter64[23:16]  |
| 12 | Channel 3 Data| 0b000000    | Counter64[31:24]  |
| 16 | Channel 4 Data| 0b000000    | Counter64[39:32]  |
| 20 | Channel 5 Data| 0b000000    | Counter64[47:40]  |
| 24 | Channel 6 Data| 0b000000    | Counter64[55:48]  |
| 28 | Channel 7 Data| 0b000000    | Counter64[63:56]  |
| 32 | Channel 8 Data| 0b000000    | Float_3a[7:0]  |
| 36 | Channel 9 Data| 0b000000    | Float_3a[15:8]  |
| 40 | Channel 10 Data| 0b000000    | Float_3a[23:16]  |
| 44 | Channel 11 Data| 0b000000    | Float_3a[31:24]  |
| 48 | Channel 12 Data| 0b000000    | Float_3b[7:0]  |
| 52 | Channel 13 Data| 0b000000    | Float_3b[15:8]  |
| 56 | Channel 14 Data| 0b000000    | Float_3b[23:16]  |
| 60 | Channel 15 Data| 0b000000    | Float_3b[31:24]  |
| 64 | Channel 16 Data| 0b000000    | Float_4a[7:0]  |
| 68 | Channel 17 Data| 0b000000    | Float_4a[15:8]  |
| 72 | Channel 18 Data| 0b000000    | Float_4a[23:16]  |
| 76 | Channel 19 Data| 0b000000    | Float_4a[31:24]  |
| 80 | Channel 20 Data| 0b000000    | Float_4b[7:0]  |
| 84 | Channel 21 Data| 0b000000    | Float_4b[15:8]  |
| 88 | Channel 22 Data| 0b000000    | Float_4b[23:16]  |
| 92 | Channel 23 Data| 0b000000    | Float_4b[31:24] :warning: |
| 96 | Channel 24 Data| 0b000000    | Integrated_Channel(idx)[7:0] |
| 100 | Channel 25 Data| 0b000000    | Integrated_Channel(idx)[15:8] |
| 104 | Channel 26 Data| 0b000000    | Integrated_Channel(idx)[23:16] |
| 108 | Channel 27 Data| 0b000000    | Integrated_Channel(idx)[31:24] |
| 112 | Channel 28 Data| 0b000000    | FifoCount[7:0]   |
| 116 | Channel 29 Data| 0b000000    | FifoCount[15:8]  |
| 120 | Channel 30 Data| 0b000000    | 0-4: Channel Index of the integrated Channel in this packet |
| 124 | Channel 31 Data| 0b000000    | 0: Chopp phase, 1: DMA FIFO Full<br/> 2-5: Accumulator Overflows <br/>6: Interlock "F" bit :bulb:<br/>7: Interlock "QF" bit :bulb:|

* :warning: The data format for this packets can be changed by user request. A recompilation of the firmware will be needed.
* :bulb: See Equ. (5) below

### Interlock FPGA Data Processing

1. The firmware first corrects raw data from the **eo** offset and recovers the **dechopped** signals, $`adc\_data_i[n]`$,
(in 18 bit, signed two's complement), using the expression: 
```math
\begin{split}
 \tag{1}  adc\_data\_eo_i[n] &= adc\_data_i[n] - eo_i \\
  s_i[n] &  = adc\_data\_eo_i[n] \cdot adc_{phase}[n]
  \end{split}
```
  * Here $`n`$ is the sample number,  $`eo_i`$ are the 32 Electronic Offsets **EO**, constants,
and $`adc_{phase}[n]`$ the phase signal of the analogue chopper, being either 1 or -1.
  *  $`i=0...31`$ is the channel number. 
  *  Result $`s_i[n]`$ is signed extended to 20 bit two's complement.  
    The above calculations are made in Integer format. (Full precision, No truncation errors)
  * The WO constants,  $`wo_i`$, are stored internally in  Q15.16 Format,
    but are signed extented to Q63.16.
    WO register resolution is equal to $`\pm 2^{-15} \approx 30\cdot 10^{-6}`$ :warning:
2. For **all** 32 channels the time Integral, corrected for the Wiring Offsets **WO**, is calculated by:
```math
 \tag{2}  \Phi_i[n] = \sum_{k=0}^n (s[k] - wo_i)
```
  *  This calculation is made in Q63.16 Fixed point format. The upper limit to
    the Integral is  $`\pm 2^{63}`$. 
  * A decimated (1:32, 62 kSPS) stream of this 32 Integral values are converted to **Single Float** (32 bit) and are encoded in the stream data packet.
 3. Interlock Calculation. For 8 **selected** channels two new functions are calculated inside the FPGA, for each new adc sample (2MSPS),
```math
 \tag{3} \begin{split}
         F[n] &= \sum_{j=0}^7 (\Phi_i[n] \cdot Param_i) \qquad (a)\\
          dF[n] &= F[n] - F[n-1] \qquad (b)
      \end{split}
```
  * $`j=0...7`$, and $`Param_i`$ is the multiplying parameter for each probe.
  *  Any set of 8 input channels can be chosen by user, but it will require a recompilation of the FPGA firmare.  
  See *INTEGER_CHANNEL_ARRAY* parameter in verilog [source](https://gitlab.mpcdf.mpg.de/bcar/atca-mimo-v2-adc/-/blob/master/src/hdl/adc_data_producer.sv) file. 
Where  
  *  These calculations are made in **Double Float**  arithmetic.:warning:
4. Calculated $F, dF $ values are converted back to **Single Float** (32 bit).
5. Finally the Algorithm applies a subtractive term:
```math
 \tag{4}  \begin{split}
          S_{interlock}[n] &= abs(F[n])-Threshold_{interlock} \qquad (a)\\
          S_{quench}[n]    &= abs(dF[n])-Threshold_{quench} \qquad (b)
          \end{split}    
```
* The Sign bit of these to functions (bit 31) are then forwarded to two physical pins on the ATCA RTM Board, serving as digital inputs to the W7-X Interlock protection System, respectivly *RTM_D7* and *RTM_D6*.  
The expressions used are:
```math
 \tag{5}  \begin{split}
          Output_{interlock}[n] &= (abs(F[n])  > Threshold_{interlock})?\; 1 : 0 \qquad  (a)\\
          Output_{quench}[n]    &= (abs(dF[n]) > Threshold_{quench})?\; 1 : 0  \qquad  (b)
          \end{split}    
```
* When Interlock Output *a* is disabled, *RTM_D6* will output the $`adc_{phase}`$ chopper signal.
6. The interlock algorithm is continually running inside the FPGA, whenever the aquisition is enabled and triggered,  
however the RTM Interlock output signals must be enabled with the respective bits on the Control Register.
7. During the FPGA reboot all **eo**, **wo** and **Param_i** parameters are reset to Zero.

### Further informations, FAQ, etc. 

* See also general [Xilinx notes](./readme.txt) in this folder.


