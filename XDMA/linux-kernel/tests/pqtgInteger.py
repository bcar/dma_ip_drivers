#!/usr/bin/env python3
# vim: sta:et:sw=4:ts=4:sts=4
"""
Use a
"""

import numpy as np

import pyqtgraph as pg
from pyqtgraph.Qt import QtGui
from pyqtgraph.dockarea import *
from data_bin_functs import *
from pdb import set_trace as bp

# FILENAME = 'data/out_16.bin'
#FILENAMECH1 = 'data/out_ilock_test_32_no_signal_connected.bin'
FILENAME = 'data/out_32_ilock.bin'
#FILENAMECH1 = 'data/out_fsin.bin'
NUMCHANNELS = 8
NUMINTCHANNELS = 8

DECIMATION = 200

pg.setConfigOption('background', 'w')  # before loading widget
pg.setConfigOption('leftButtonPan', False)

app = pg.mkQApp("DockArea Example")
win = QtGui.QMainWindow()
area = DockArea()
win.setCentralWidget(area)
win.resize(1000,500)
win.setWindowTitle('ATCA CH1 data + Ilock Float')

## Create docks, place them into the window one at a time.
## Note that size arguments are only a suggestion; docks will still have to
## fill the entire dock area and obey th>e limits of their internal widgets.
# d1 = Dock("Dock1", size=(1, 1))     ## give this dock the minimum possible size
d1 = Dock("Dock1", size=(500, 400) , closable=True)     ## give this dock the minimum possible size
d2 = Dock("Dock2 - Console", size=(500,400), closable=True)
d3 = Dock("Dock3", size=(500,400), closable=True)
d4 = Dock("Dock4 (tabbed) - Plot", size=(500,400), closable=True)
# d5 = Dock("Dock5 - Image", size=(500,200))
# d6 = Dock("Dock6 (tabbed) - Plot", size=(500,200))
area.addDock(d1, 'left')      ## place d1 at left edge of dock area (it will fill the whole space since there are no other docks yet)
area.addDock(d2, 'right')     ## place d2 at right edge of dock area
area.addDock(d3, 'bottom', d1)## place d3 at bottom edge of d1
area.addDock(d4, 'bottom', d2)     ## place d4 at right edge of dock area
# area.addDock(d5, 'left', d1)  ## place d5 at left edge of d1
# area.addDock(d6, 'top', d4)   ## place d5 at top edge of d4

## Test ability to move docks programatically after they have been placed
# area.moveDock(d4, 'top', d2)     ## move d4 to top edge of d2
# area.moveDock(d6, 'above', d4)   ## move d6 to stack on top of d4
# area.moveDock(d5, 'top', d2)     ## move d5 to top edge of d2

# def get_count(data_mat):
    # count = (data_mat[0] & 0xFF) | ((data_mat[1] & 0xFF) << 8)
    # count = count | ((data_mat[3] & 0xFF) << 24) | ((data_mat[2] & 0xFF) << 16)
    # count = count | ((data_mat[5] & 0xFF) << 40) | ((data_mat[4] & 0xFF) << 32)
    # count = count | ((data_mat[7] & 0xFF) << 56) | ((data_mat[6] & 0xFF) << 48)
    # return count

# def get_int32(data_mat, first):
    # valI  = ((data_mat[first+1] & 0xFF) << 8) | (data_mat[first] & 0xFF)
    # valI |= ((data_mat[first+3] & 0xFF) << 24) | ((data_mat[first+2] & 0xFF) << 16)
    # return valI

# def get_float(data_mat, first):
    # valFloat = ((data_mat[first+1] & 0xFF) << 8) | (data_mat[first] & 0xFF)
    # valFloat = valFloat | ((data_mat[first+3] & 0xFF) << 24) | ((data_mat[first+2] & 0xFF) << 16)
    # tobytes = valFloat.tobytes()
    # return np.frombuffer(tobytes,dtype='f')

def draw_float(pltWdgt, float_arr, color, nam):
    # pltWdgt.clear()
    # pltWdgt.setYRange(-32768*4, 32768*4, padding=0.01)
    pltWdgt.addLegend()
    # pltWdgt.setLabels(title='ch {}-{}'.format(first, first + 7), bottom='Sample',
        # left='LSB', right='Chopp')
    pltWdgt.plot(float_arr, pen=({'color': (color, NUMINTCHANNELS*1.3),
        'width': 1}), name=nam)
    return

def draw_plotI32(pltWdgt, data_mat):
    pltWdgt.clear()
    # pltWdgt.setYRange(-32768*4, 32768*4, padding=0.01)
    pltWdgt.addLegend()
    pltWdgt.setLabels(title='ch {}-{}'.format(0,7))
    # , bottom='Sample',        left='LSB', right='Chopp')
    x = np.arange(data_mat.shape[1]) * DECIMATION
    # for i in {0,2,6,4,16,17,18,19}:
    for i in {0,2,4,6}:
    # for i in range(NUMCHANNELS):
        pltWdgt.plot(x, data_mat[i], pen=({'color': (i, NUMINTCHANNELS*1.3),
            'width': 1}), name='ch{}'.format(i))
    return

def draw_plotF32(pltWdgt, data_mat):
    pltWdgt.clear()
    pltWdgt.addLegend()
    pltWdgt.setLabels(title='Floats')
    # , bottom='Sample',        left='LSB', right='Chopp')
    for i in range(4):
        pltWdgt.plot(data_mat[i+28], pen=({'color': (i, NUMINTCHANNELS*1.3),
            'width': 1}), name='Flt{}'.format(i))
    return

def draw_plotD64(pltWdgt, data_mat):
    pltWdgt.clear()
    # pltWdgt.setYRange(-32768*4, 32768*4, padding=0.01)
    pltWdgt.addLegend()
    pltWdgt.setLabels(title='Intgch {}--{}'.format(0,7))
    # , bottom='Sample',        left='LSB', right='Chopp')
    x = np.arange(data_mat.shape[1]) * DECIMATION
    samples = (data_mat.shape[1]-1) * DECIMATION
    stri = data_mat[6:14,-2] # last sample is zero?
    strwo = data_mat[6:14,-2] / samples # last sample is zero?
    print(f"#{samples} Samples, Last D64 mat. Last vals: {stri}, WO: {strwo}")
    # for i in range(10,14):
        # line=data_mat[i,:]
        # print(line[-2])
    # for i in range(8):
    for i in range(NUMINTCHANNELS):
        pltWdgt.plot(x, data_mat[i+6], pen=({'color': (i, NUMINTCHANNELS*1.3),
            'width': 1}), name=f'Ich--{i}')
    returno

# d1.hideTitleBar()
# w = create_plot32(data_mat, 0)
w1 = pg.PlotWidget(title="Dock 1 plot")
w1.setLabels(title='ch ', bottom='Sample',
    left='LSB', right='Count')
p1 = pg.ViewBox()
w1.scene().addItem(p1)
w1.getAxis('right').linkToView(p1)
p1.setXLink(w1)

#pltCount = pg.PlotCurveItem([0], [0], pen=pg.mkPen(color='k', width=2))
#p1.addItem(pltCount)
pltChopp = pg.PlotCurveItem([0], [0], pen=pg.mkPen(color='k', width=2))
p1.addItem(pltChopp)
p1.setYRange(-1, 8)
wlay = pg.LayoutWidget()
# w1 = pg.PlotWidget(title="Plot inside dock with no title bar")
refreshBtn = QtGui.QPushButton('Draw plot')
calcBtn = QtGui.QPushButton('Calc')
line_edit = QtGui.QLineEdit(FILENAME)
wlay.addWidget(w1, row=0, col=0, colspan=3)
wlay.addWidget(refreshBtn, row=1, col=0)
wlay.addWidget(line_edit, row=1, col=1)
wlay.addWidget(calcBtn, row=1, col=2)
d1.addWidget(wlay)

# w2 = create_plot32(data_mat, 8)
w2 = pg.PlotWidget(title="Dock 2 plot")
w2.setLabels(title='ch 8-15', bottom='Sample',
    left='LSB', right='"F"')
p2 = pg.ViewBox()
w2.scene().addItem(p2)
w2.getAxis('right').linkToView(p2)
p2.setXLink(w2)

# p2.addItem(pltCount)
pltFloat0 = pg.PlotCurveItem([0],[0], pen=pg.mkPen(color='k', width=2), name="dF")
pltFloat1 = pg.PlotCurveItem([0],[0], pen=pg.mkPen(color='b', width=2))
p2.addItem(pltFloat0)
p2.addItem(pltFloat1)
d2.addWidget(w2)

## Hide title bar on dock 3
# d3.hideTitleBar()
# w3 = create_plot32(data_mat, 16)
w3 = pg.PlotWidget(title="Dock 3 plot")
w3.addLegend()
w3.setLabels(bottom='Sample', left='dF', right='"F"')
p3 = pg.ViewBox()
w3.scene().addItem(p3)
w3.getAxis('right').linkToView(p3)
p3.setXLink(w3)

d3.addWidget(w3)

# d4.hideTitleBar()
w4 = pg.PlotWidget(title="Dock 4 plot")
w4.setLabels(title='ch 24-31', bottom='Sample',
        left='LSB', right='Chopp')
# w4 = create_plot32(data_mat, 24)

p4 = pg.ViewBox()
w4.scene().addItem(p4)
w4.getAxis('right').linkToView(p4)
p4.setXLink(w4)
#cross hair
vLine = pg.InfiniteLine(angle=90, movable=False)
# hLine = pg.InfiniteLine(angle=0, movable=False)
p4.addItem(vLine, ignoreBounds=True)
# p4.addItem(hLine, ignoreBounds=True)

# label = pg.LabelItem(text='XXX',justify='right')
label = pg.TextItem(text='XXX')
p4.addItem(label)


pltCh = pg.PlotCurveItem([0], pen=pg.mkPen(color='k', width=2))
# pltCh = pg.PlotCurveItem((data_mat[31] & 0x01), pen=pg.mkPen(color='r', width=3))
p4.setYRange(-1, 8)

p4.addItem(pltCh)

#vb = p4.vb

d4.addWidget(w4)

def updateViews():
    global p1, p2, p3, p4, w1, w2, w3, w4
    p1.setGeometry(w1.getViewBox().sceneBoundingRect())
    p1.linkedViewChanged(w1.getViewBox(), p1.XAxis)
    p2.setGeometry(w2.getViewBox().sceneBoundingRect())
    p2.linkedViewChanged(w2.getViewBox(), p2.XAxis)
    p3.setGeometry(w3.getViewBox().sceneBoundingRect())
    p3.linkedViewChanged(w3.getViewBox(), p3.XAxis)
    p4.setGeometry(w4.getViewBox().sceneBoundingRect())
    p4.linkedViewChanged(w4.getViewBox(), p4.XAxis)

def calc_offsets():
    global line_edit
    data_mat = get_data_I32(line_edit.text())
    meanEO=np.mean(data_mat, axis=1)
    # print(f'Mean= {meanEO}')
    meanI = (np.around(meanEO)).astype('int32')
    print('Mean is ' + repr(meanI))

def draw_all():
    global w1, w2, w3, w4, pltCh, pltCount, pltFloat0, pltFloat1, line_edit
    w1.clear()
    w3.clear()
    w4.clear()
    data_mat = get_data_I32(line_edit.text())
    draw_plotI32(w1, data_mat) # draq adc_dechop
    data_raw = get_data_raw(line_edit.text())
    chp =decode_chopp(data_raw)
    # count = data_mat[0]
    # count = decode_count64(data_raw)
    # print('count ' + repr(count[0:9]))
    # print('count end ' + repr(count[-10:-1]))
    # print('count ')
    # repr(count[0:9]))
    x = np.arange(data_mat.shape[1]) * DECIMATION
    pltChopp.setData(x,chp)
    # pltCount.setData(x,count)
    # :bp()
    # w2
    f0 = decode_float(data_raw, 8)
    f1 = decode_float(data_raw, 12)
    w2.addLegend()
    w2.plot(x, f0, pen=({'color': (0, 2*1.3),
            'width': 1}), name='f0')
    pltFloat1.setData(x,f1)
    # w2.plot(x, f1, pen=({'color': (1, 2*1.3),
            # 'width': 1}), name='f1')
    # data_mat = get_data_double(line_edit.text())
    # draw_plotD64(w2, data_mat)
    # w3
    # draw_plot32(w3, data_mat,16)
    # data_mat = get_data_float(line_edit.text())
    # draw_plotF32(w3, data_mat)
    # draw_plot32(w4, data_mat, 24)
    # count = get_count(data_mat)
    # # "Float 0- F"
    # float_mat = decode_flaot(data_mat, 8)
    # # print(float_mat[:10])
    # draw_float(w2, float_mat, 11, 'F')
    # # "Float 1 - dF "
    # float_mat = get_float(data_mat, 12)
    # # print(float_mat[:10])
    # # draw_float(w3, float_mat, 13, 'Float 1')
    # pltFloat0.setData(float_mat)
    # # pltFloat1.setData(float_mat)
    # # "Float 2  - abs(F) - p8"
    # float_mat = get_float(data_mat, 16)
    # # print(float_mat[:10])
    # draw_float(w2, float_mat, 15, 'abs(F)-par8')
    # # pltFloat0.setData(float_mat)
    # # "Float 3  - abs(dF) - p9"
    # float_mat = get_float(data_mat, 20)
    # # print(float_mat[:10])
    # #pltFloat1.setData(float_mat)  #  '>b'
    # # w3.plot(float_mat, pen=pg.mkPen(color='r', width=2),
            # # name="dF")
    # pltCh.setData((data_mat[31] & 0x0001))

updateViews()
w4.getViewBox().sigResized.connect(updateViews)
w3.getViewBox().sigResized.connect(updateViews)

refreshBtn.clicked.connect(draw_all)
calcBtn.clicked.connect(calc_offsets)

def mouseMoved(evt):
    pos = evt[0]  ## using signal proxy turns original arguments into a tuple
    mousePoint = p4.mapSceneToView(pos)
    index = int(mousePoint.x())
#        if index > 0 and index < len(data1):
#            label.setText("<span style='font-size: 12pt'>x=%0.1f,   <span style='color: red'>y1=%0.1f</span>,
#    <span style='color: green'>y2=%0.1f</span>" % (mousePoint.x(), data1[index], data2[index]))
#        if index > 0 and index < len(data1):
    xLine=mousePoint.x()
    label.setHtml("<span style='font-size: 12pt'> <span style='color: red'> x=%0.0f </span> </span>" % (xLine))
    # label.setText("<span style='font-size: 12pt'>x=%0.1f,   <span style='color: red'>y=%0.1f</span>" % (mousePoint.x(), mousePoint.y()))
    vLine.setPos(xLine)
    # hLine.setPos(mousePoint.y())

proxy = pg.SignalProxy(p4.scene().sigMouseMoved, rateLimit=60, slot=mouseMoved)
#p1.scene().sigMouseMoved.connect(mouseMoved)

draw_all()

## Add widgets into each dock

win.show()

if __name__ == '__main__':
    pg.exec()

