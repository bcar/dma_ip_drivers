#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 12 12:11:30 2020

@author: Bernardo Carvalho
"""

import sys
import numpy as np
import matplotlib.pyplot as plt

if len(sys.argv) > 1:
    filename = str(sys.argv[1])
else:
    filename = 'data/out_32.bin'

data = np.fromfile(filename, dtype='int32')
# ‘F’ means to readthe elements using Fortran-like index order,
# with the first index changing fastest,
data_mat = np.reshape(data, (2, -1), order='F')

fig, ax = plt.subplots()

ax.plot(data_mat[0], label='ch0')  # plot columm 0
ax.plot(data_mat[1]/16, label='ch1')  # plot columm 1
#ax.plot(data_mat[2]/2**12, label='ch2')  # plot columm 1
#ax.plot(data_mat[3]/2**12, label='ch3')  # plot columm 1
#ax.plot(data_mat[30] & 0xFFF, label='count')
#ax.plot(data_mat[30] & 0xFFF, label='count')
#ax.plot(data_mat[30], label='count0')
#ax.plot(data_mat[31], label='count')
ax.set_xlabel('Sample')
ax.set_ylabel('LSB')
ax.set_title("ATCA v2 32 counter bit data ~100MB/s")  # Add a title to the axes.
ax.legend()  # Add a legend.
color = 'tab:red'
ax2 = ax.twinx()
ax2.set_ylim(-1, 10)
ax2.plot(data_mat[1] & 1,  label='prog_full')  # plot columm 1
ax2.plot(data_mat[1] & 2,  label='fifo_ready')  # plot columm 1
ax2.plot(data_mat[1] & 4,  label='axis_tready')  # plot columm 1
ax2.plot(data_mat[1] & 8,  label='axis_tvalid')  # plot columm 1
ax2.legend()  # Add a legend.

plt.show()
