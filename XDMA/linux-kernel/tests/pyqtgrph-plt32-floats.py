#!/usr/bin/env python3
# vim: sta:et:sw=4:ts=4:sts=4
"""
Use a 
"""

import numpy as np

import pyqtgraph as pg
from pyqtgraph.Qt import QtGui
from pyqtgraph.dockarea import *
from atca_data_functions import get_data32_raw, get_data32, get_count64, get_float
from datetime import datetime

FILENAME32 = 'data/out_32.bin'
NUMCHANNELS = 8

SCALE_32 = 2**14  # Data is shifted 14 bits right
# DECIMATION = 20

pg.setConfigOption('background', 'w')  # before loading widget
pg.setConfigOption('leftButtonPan', False)

## Always start by initializing Qt (only once per application)
app = QtGui.QApplication([])

# app = pg.mkQApp("DockArea Example")
win = QtGui.QMainWindow()
area = DockArea()
win.setCentralWidget(area)
win.resize(1000,500)
dt = datetime.today().replace(microsecond=0)
title = 'ATCA v2 32b data + InterLck ' + str(dt)

win.setWindowTitle(title)

## Create docks, place them into the window one at a time.
## Note that size arguments are only a suggestion; docks will still have to
## fill the entire dock area and obey th>e limits of their internal widgets.
# d1 = Dock("Dock1", size=(1, 1))     ## give this dock the minimum possible size
d1 = Dock("Dock1", size=(500, 400) , closable=True)     ## give this dock the minimum possible size
d2 = Dock("Dock2 - Console", size=(500,400), closable=True)
d3 = Dock("Dock3", size=(500,400), closable=True)
d4 = Dock("Dock4 (tabbed) - Plot", size=(500,400), closable=True)
# d5 = Dock("Dock5 - Image", size=(500,200))
# d6 = Dock("Dock6 (tabbed) - Plot", size=(500,200))
area.addDock(d1, 'left')      ## place d1 at left edge of dock area (it will fill the whole space since there are no other docks yet)
area.addDock(d2, 'right')     ## place d2 at right edge of dock area
area.addDock(d3, 'bottom', d1)## place d3 at bottom edge of d1
area.addDock(d4, 'bottom', d2)     ## place d4 at right edge of dock area
# area.addDock(d5, 'left', d1)  ## place d5 at left edge of d1
# area.addDock(d6, 'top', d4)   ## place d5 at top edge of d4

## Test ability to move docks programatically after they have been placed
# area.moveDock(d4, 'top', d2)     ## move d4 to top edge of d2
# area.moveDock(d6, 'above', d4)   ## move d6 to stack on top of d4
# area.moveDock(d5, 'top', d2)     ## move d5 to top edge of d2

def draw_float(pltWdgt, float_arr, color, nam):
    # pltWdgt.clear()
    # pltWdgt.setYRange(-32768*4, 32768*4, padding=0.01)
    pltWdgt.addLegend()
    # pltWdgt.setLabels(title='ch {}-{}'.format(first, first + 7), bottom='Sample',
        # left='LSB', right='Chopp')
    pltWdgt.plot(float_arr, pen=({'color': (color, NUMCHANNELS*1.3),
        'width': 1}), name=nam)
    return

def draw_plot32(pltWdgt, data_mat, first):
    # w = pg.PlotWidget(title="Dock 2 plot")
    pltWdgt.clear()
    max_plot = 32768*4//1000
    pltWdgt.setYRange(-max_plot, max_plot, padding=0.01)
    pltWdgt.addLegend()
    pltWdgt.setLabels(title='ch {}-{}'.format(first, first + 7))
    # , bottom='Sample',        left='LSB', right='Chopp')
    # data_scaled = (data_mat / SCALE_32).astype('int32')
    for i in range(NUMCHANNELS):
        pltWdgt.plot(data_mat[i + first] // 1000 , pen=({'color': (i, NUMCHANNELS*1.3),
            'width': 1}), name="ch{}".format(i + first))
    return

# d1.hideTitleBar()
# w = create_plot32(data_mat, 0)
w1 = pg.PlotWidget(title="Dock 1 plot")
w1.setLabels(title='ch 0-7', bottom='Sample',
    left='LSB/1000', right='Count')
p1 = pg.ViewBox()
w1.scene().addItem(p1)
w1.getAxis('right').linkToView(p1)
p1.setXLink(w1)

pltCount = pg.PlotCurveItem([0], pen=pg.mkPen(color='k', width=2))
p1.addItem(pltCount)
# d2.addWidget(w2)
wlay = pg.LayoutWidget()
# w1 = pg.PlotWidget(title="Plot inside dock with no title bar")
refreshBtn = QtGui.QPushButton('Draw plot')
calcBtn = QtGui.QPushButton('Calc')
line_edit = QtGui.QLineEdit(FILENAME32)
wlay.addWidget(w1, row=0, col=0, colspan=3)
wlay.addWidget(refreshBtn, row=1, col=0)
wlay.addWidget(line_edit, row=1, col=1)
wlay.addWidget(calcBtn, row=1, col=2)
d1.addWidget(wlay)

w2 = pg.PlotWidget(title="Float Values")
w2.addLegend()
w2.setLabels(bottom='Sample', left='"F"', right='"dF"')
p2 = pg.ViewBox()
w2.scene().addItem(p2)
w2.getAxis('right').linkToView(p2)
p2.setXLink(w2)

pltFloatdF = pg.PlotCurveItem([0], pen=pg.mkPen(color='k', width=2), name="dF")
#pltFloat1 = pg.PlotCurveItem([0], pen=pg.mkPen(color='b', width=2))
p2.addItem(pltFloatdF)
# pltCount = pg.PlotCurveItem([0], pen=pg.mkPen(color='k', width=2))
# p2.addItem(pltCount)
d2.addWidget(w2)

## Hide title bar on dock 3
# d3.hideTitleBar()
# w3 = create_plot32(data_mat, 16)
w3 = pg.PlotWidget(title="Float Values")
w3.addLegend()
w3.setLabels(bottom='Sample', left='"abs(F) - p8"', right='"abs(dF) - p9"')
p3 = pg.ViewBox()
w3.scene().addItem(p3)
w3.getAxis('right').linkToView(p3)
p3.setXLink(w3)
pltFloatadF = pg.PlotCurveItem([0], pen=pg.mkPen(color='k', width=2), name="abs(dF) - p9")
p3.addItem(pltFloatadF)
d3.addWidget(w3)

# d4.hideTitleBar()
w4 = pg.PlotWidget(title="Dock 4 plot")
w4.setLabels(title='ch 24-31', bottom='Sample',
        left='LSB', right='Chopp')
# w4 = create_plot32(data_mat, 24)

p4 = pg.ViewBox()
w4.scene().addItem(p4)
w4.getAxis('right').linkToView(p4)
p4.setXLink(w4)
#cross hair
vLine = pg.InfiniteLine(angle=90, movable=False)
# hLine = pg.InfiniteLine(angle=0, movable=False)
p4.addItem(vLine, ignoreBounds=True)
# p4.addItem(hLine, ignoreBounds=True)

# label = pg.LabelItem(text='XXX',justify='right')
label = pg.TextItem(text='XXX')
p4.addItem(label)

pltCh = pg.PlotCurveItem([0], pen=pg.mkPen(color='k', width=2))
# pltCh = pg.PlotCurveItem((data_mat[31] & 0x01), pen=pg.mkPen(color='r', width=3))
p4.setYRange(-1, 8)

p4.addItem(pltCh)

#vb = p4.vb

d4.addWidget(w4)

def updateViews():
    global p1, p2, p3, p4, w1, w2, w3, w4
    p1.setGeometry(w1.getViewBox().sceneBoundingRect())
    p1.linkedViewChanged(w1.getViewBox(), p1.XAxis)
    p2.setGeometry(w2.getViewBox().sceneBoundingRect())
    p2.linkedViewChanged(w2.getViewBox(), p2.XAxis)
    p3.setGeometry(w3.getViewBox().sceneBoundingRect())
    p3.linkedViewChanged(w3.getViewBox(), p3.XAxis)
    p4.setGeometry(w4.getViewBox().sceneBoundingRect())
    p4.linkedViewChanged(w4.getViewBox(), p4.XAxis)

def calc_offsets():
    global line_edit
    # data_mat = get_data32(line_edit)
    # meanS=np.array2string(data_mat, precision=0)
    filename = line_edit.text()
    data_mat = get_data32_raw(filename)
    #/ SCALE_32).astype('int32')
    data32,c = get_data32(data_mat)
    meanEO=np.mean(data32, axis=1)
    # print(f'Mean= {meanEO}')
    meanI = (np.around(meanEO)).astype('int32')
    print('Mean is ' + repr(meanI))

def draw_all():
    global w1, w2, w3, w4, pltCh, pltCount, pltFloatdF, pltFloatadF, pltFloat1, line_edit
    w1.clear()
    w2.clear()
    w3.clear()
    w4.clear()
    filename = line_edit.text()
    data_mat = get_data32_raw(filename)
    data32, chopp = get_data32(data_mat)
    pltCh.setData(chopp)
    draw_plot32(w1, data32, 0)
#    draw_plot32(w2, data_mat, 8)
#    draw_plot32(w3, data_mat, 16)
    draw_plot32(w4, data32, 0)
    count = get_count64(data_mat)
    pltCount.setData(count)
    # "Float 0- F"
    float_mat = get_float(data_mat, 8)
    draw_float(w2, float_mat, 11, 'F')
    # print(float_mat[:10])
    # "Float 1 - dF "
    float_mat = get_float(data_mat, 12)
    pltFloatdF.setData(float_mat)
    # print(float_mat[:10])
    #draw_float(w4, float_mat, 11, 'dF')
    # draw_float(w3, float_mat, 13, 'Float 1')
    # pltFloat1.setData(float_mat)
    # "Float 2  - abs(F) - p8"
    float_mat = get_float(data_mat, 16)
    # print(float_mat[:10])
    draw_float(w3, float_mat, 15, 'abs(F)-par8')
    # "Float 3  - abs(dF) - p9"
    float_mat = get_float(data_mat, 20)
    pltFloatadF.setData(float_mat)
    # print(float_mat[:10])
    #pltFloat1.setData(float_mat)  #  '>b'
    # w3.plot(float_mat, pen=pg.mkPen(color='r', width=2),
            # name="dF")

updateViews()
w4.getViewBox().sigResized.connect(updateViews)
w3.getViewBox().sigResized.connect(updateViews)

refreshBtn.clicked.connect(draw_all)
calcBtn.clicked.connect(calc_offsets)

def mouseMoved(evt):
    pos = evt[0]  ## using signal proxy turns original arguments into a tuple
    mousePoint = p4.mapSceneToView(pos)
    index = int(mousePoint.x())
#        if index > 0 and index < len(data1):
#            label.setText("<span style='font-size: 12pt'>x=%0.1f,   <span style='color: red'>y1=%0.1f</span>,
#    <span style='color: green'>y2=%0.1f</span>" % (mousePoint.x(), data1[index], data2[index]))
#        if index > 0 and index < len(data1):
    xLine=mousePoint.x()
    label.setHtml("<span style='font-size: 12pt'> <span style='color: red'> x=%0.0f </span> </span>" % (xLine))
    # label.setText("<span style='font-size: 12pt'>x=%0.1f,   <span style='color: red'>y=%0.1f</span>" % (mousePoint.x(), mousePoint.y()))
    vLine.setPos(xLine)
    # hLine.setPos(mousePoint.y())

proxy = pg.SignalProxy(p4.scene().sigMouseMoved, rateLimit=60, slot=mouseMoved)
#p1.scene().sigMouseMoved.connect(mouseMoved)

draw_all()

## Add widgets into each dock

win.show()

if __name__ == '__main__':
       ## Start the Qt event loop
    app.exec_()
#    pg.exec()

