#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 12 12:11:30 2020

@author: Bernardo Carvalho
"""

import sys
import numpy as np
import matplotlib.pyplot as plt

if len(sys.argv) > 1:
    filename = str(sys.argv[1])
else:
    filename = 'data/out_32.bin'

NUM_CHANNELS = 32

data_i32 = np.fromfile(filename, dtype='int32')
# ‘F’ means to readthe elements using Fortran-like index order,
# with the first index changing fastest,
data_i32 = np.reshape(data_i32, (NUM_CHANNELS, -1), order='F')
#data = np.fromfile(filename, dtype='int32')
#dt = np.dtype('f8')   # 64-bit floating-point number

dt = np.dtype('f4')   # 32-bit floating-point number
data = np.fromfile(filename, dtype=dt)
# ‘F’ means to readthe elements using Fortran-like index order,
# with the first index changing fastest,
data_mat = np.reshape(data, (NUM_CHANNELS, -1), order='F')

fig, ax = plt.subplots()

ax.plot(data_i32[0], label='ch0')  # plot columm 0
#ax.plot(data_i32[1], label='ch1 d')  # plot columm 1
#ax.plot(data_mat[2], label='ch0 f')  # plot columm 0
#ax.plot(data_mat[3], label='ch1 f')  # plot columm 1
#ax.plot(data_mat[4], label='ch0 multf')  # plot columm 0
#ax.plot(data_mat[5], label='ch1 multf')  # plot columm 1
#ax.plot(data_mat[6], label='ch0 multf')  # plot columm 0
#ax.plot(data_mat[8], label='a8 multf')  # plot columm 1
#ax.plot(data_mat[2]/2**12, label='ch2')  # plot columm 1
#ax.plot(data_mat[3]/2**12, label='ch3')  # plot columm 1
#ax.plot(data_mat[14], label='a7e')  # plot columm 0
#ax.plot(data_mat[15], label='a7o')  # plot columm 1
#ax.plot(data_mat[16], label='a8e')
#ax.plot(data_mat[17], label='a8o')
#ax.plot(data_mat[18], label='a9e')
#ax.plot(data_mat[19], label='a9o')
ax.plot(data_mat[20], label='F a10e')
ax.plot(1.1*data_mat[21], label='a10o')
# ax.plot(1.2*data_mat[22], label='a11e')
#ax.plot(*data_mat[23], label='a11o dF')
#ax.plot(data_mat[20], label='a10e')
#ax.plot(data_mat[16], label='abs(accum)')
#ax.plot(data_mat[17], label='acumm')
ax.set_xlabel('Sample/200')
ax.set_ylabel('LSB')
ax.set_title("ATCA v2 32 bit Integrator/Diferentiator ")  # Add a title to the axes.
color = 'tab:red'
ax2 = ax.twinx()
linestyle = 'dashdot'
ax.plot(300*data_mat[23], label='dF', color='red', linestyle=linestyle, linewidth=1.5 )
ax.plot(300*data_mat[25], label='abs(dF)', linestyle=linestyle, linewidth=1.5)
#0x0D
ax.plot(300*data_mat[27], label='abs(dF) - 2e4', linestyle=linestyle, linewidth=1.5)

ax2.plot(data_i32[28] & 0x1, label='ch')  # plot columm 1
# ax2.plot(data_i32[28] & 0x2, label='ff')  # plot columm 1
ax2.plot((data_i32[28] & 0x4)//4, label='Flc')  # plot columm 1
ax2.plot((data_i32[28] & 0x8)//8, label='dFlck')  # plot columm 1
ax2.set_ylim(0,8)
# ax2.set_ylim(-2e5, 2e5)
#ax2.plot(data_mat[1] & 1,  label='prog_full')  # plot columm 1
#ax2.plot(data_mat[1] & 2,  label='fifo_ready')  # plot columm 1
#ax2.plot(data_mat[1] & 4,  label='axis_tready')  # plot columm 1
#ax2.plot(data_mat[1] & 8,  label='axis_tvalid')  # plot columm 1
ax.legend()  # Add a legend.
ax2.legend()  # Add a legend.

plt.show()
