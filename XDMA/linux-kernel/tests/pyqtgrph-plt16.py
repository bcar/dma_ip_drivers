#!/usr/bin/env python3
# vim: sta:et:sw=4:ts=4:sts=4
"""
Use a 
"""

import numpy as np

import pyqtgraph as pg
from pyqtgraph.Qt import QtGui
from pyqtgraph.dockarea import *
from atca_data_functions import get_data16

FILENAME = 'data/out_16.bin'
NUMCHANNELS = 8

pg.setConfigOption('background', 'w')  # before loading widget

## Always start by initializing Qt (only once per application)
app = QtGui.QApplication([])

#app = pg.mkQApp("DockArea Example")
win = QtGui.QMainWindow()
area = DockArea()
win.setCentralWidget(area)
win.resize(1000,500)
win.setWindowTitle('ATCA 16 data')

## Create docks, place them into the window one at a time.
## Note that size arguments are only a suggestion; docks will still have to
## fill the entire dock area and obey the limits of their internal widgets.
# d1 = Dock("Dock1", size=(1, 1))     ## give this dock the minimum possible size
d1 = Dock("Dock1", size=(500, 400) , closable=True)     ## give this dock the minimum possible size
d2 = Dock("Dock2", size=(500,400), closable=True)
d3 = Dock("Dock3", size=(500,400), closable=True)
# d4 = Dock("Dock4 (tabbed) - Plot", size=(500,400), closable=True)
d4 = Dock("Dock4 - Plot", size=(500,400), closable=True)
# d5 = Dock("Dock5 - Image", size=(500,200))
# d6 = Dock("Dock6 (tabbed) - Plot", size=(500,200))
area.addDock(d1, 'left')      ## place d1 at left edge of dock area (it will fill the whole space since there are no other docks yet)
area.addDock(d2, 'right')     ## place d2 at right edge of dock area
area.addDock(d3, 'bottom', d1)## place d3 at bottom edge of d1
area.addDock(d4, 'bottom', d2)     ## place d4 at right edge of dock area
# area.addDock(d5, 'left', d1)  ## place d5 at left edge of d1
# area.addDock(d6, 'top', d4)   ## place d5 at top edge of d4

## Test ability to move docks programatically after they have been placed
# area.moveDock(d4, 'top', d2)     ## move d4 to top edge of d2
# area.moveDock(d6, 'above', d4)   ## move d6 to stack on top of d4
# area.moveDock(d5, 'top', d2)     ## move d5 to top edge of d2

def draw_plot16(pltWdgt, data_mat, first):
    pltWdgt.clear()
    pltWdgt.setYRange(-32768, 32768, padding=0.01)
    pltWdgt.addLegend()
    pltWdgt.setLabels(title='ch {}-{}'.format(first, first + 7))
    # , bottom='Sample',        left='LSB', right='Chopp')
    for i in range(NUMCHANNELS):
        pltWdgt.plot(data_mat[i + first], pen=({'color': (i, NUMCHANNELS*1.3),
            'width': 1}), name="ch{}".format(i + first))
    return

# filename = self.line_edit.text()
# ‘F’ means to readthe elements using Fortran-like index order,
# with the first index changing fastest,
# filename = self.line_edit.text()
# ‘F’ means to readthe elements using Fortran-like index order,
# with the first index changing fastest,
#def get_data16(le):
#    filename = le.text()
#    data = np.fromfile(filename, dtype='int16')
#    data_mat = np.reshape(data, (32, -1), order='F')
#    return data_mat

d1.hideTitleBar()
w1 = pg.PlotWidget(title="Dock 1 plot")
w1.setLabels(left='LSB')
wlay = pg.LayoutWidget()
# w1 = pg.PlotWidget(title="Plot inside dock with no title bar")
refreshBtn = QtGui.QPushButton('Redraw plot')
line_edit = QtGui.QLineEdit(FILENAME)
wlay.addWidget(w1, row=0, col=0, colspan=2)
wlay.addWidget(refreshBtn, row=1, col=0)
wlay.addWidget(line_edit, row=1, col=1)
d1.addWidget(wlay)

w2 = pg.PlotWidget(title="Dock 2 plot")
w2.setLabels(bottom='Sample', left='LSB')
d2.addWidget(w2)

## Hide title bar on dock 3
# d3.hideTitleBar()/
w3 = pg.PlotWidget(title="Dock 3 plot")
# w3 = pg.PlotWidget(title="Plot inside dock with no title bar")
d3.addWidget(w3)

# d4.hideTitleBar()
w4 = pg.PlotWidget(title="Dock 4 plot")
w4.setLabels(left='LSB', right='Chopp')

p4 = pg.ViewBox()
w4.scene().addItem(p4)
w4.getAxis('right').linkToView(p4)
p4.setXLink(w4)
#cross hair
vLine = pg.InfiniteLine(angle=90, movable=False)
hLine = pg.InfiniteLine(angle=0, movable=False)
p4.addItem(vLine, ignoreBounds=True)
p4.addItem(hLine, ignoreBounds=True)

# label = pg.LabelItem(text='XXX',justify='right')
label = pg.TextItem(text='XXX')
p4.addItem(label)
pltCh = pg.PlotCurveItem([0], pen=pg.mkPen(color='k', width=2))
# pltCh = pg.PlotCurveItem((data_mat[31] & 0x01), pen=pg.mkPen(color='r', width=3))
p4.setYRange(-1, 8)

p4.addItem(pltCh)
d4.addWidget(w4)

def updateViews():
    global p4, w4
    p4.setGeometry(w4.getViewBox().sceneBoundingRect())
    p4.linkedViewChanged(w4.getViewBox(), p4.XAxis)

def draw_all():
    global w1, w2, w3, w4, pltCh, pltCount, line_edit
    w1.clear()
    w3.clear()
    w4.clear()
    filen = line_edit.text()
    data_mat = get_data16(filen)
    draw_plot16(w1, data_mat, 0)
    draw_plot16(w2, data_mat, 8)
    draw_plot16(w3, data_mat, 16)
    draw_plot16(w4, data_mat, 24)

updateViews()
w4.getViewBox().sigResized.connect(updateViews)
refreshBtn.clicked.connect(draw_all)

# vb = w4.vb

def mouseMoved(evt):
    pos = evt[0]  ## using signal proxy turns original arguments into a tuple
    mousePoint = p4.mapSceneToView(pos)
    index = int(mousePoint.x())
    label.setHtml("<span style='font-size: 12pt'> <span style='color: red'> x=%0.1f </span> <span style='color: green'>  y=%0.1f</span>" % (mousePoint.x(), mousePoint.y()))
    # label.setText("<span style='font-size: 12pt'>x=%0.1f,   <span style='color: red'>y=%0.1f</span>" % (mousePoint.x(), mousePoint.y()))
    vLine.setPos(mousePoint.x())
    hLine.setPos(mousePoint.y())

proxy = pg.SignalProxy(p4.scene().sigMouseMoved, rateLimit=60, slot=mouseMoved)
#p1.scene().sigMouseMoved.connect(mouseMoved)

## Add widgets into each dock
draw_all()

win.show()

if __name__ == '__main__':
    ## Start the Qt event loop
    app.exec_()
# pg.exec()

