#!/usr/bin/env python3
# vim: sta:et:sw=4:ts=4:sts=4
"""
Extract data from binary files and save in MATLAB format
"""

import sys
import numpy as np
from scipy.io import savemat
from atca_data_functions import get_data32_raw, get_data32, get_float

if len(sys.argv) > 1:
    filename = str(sys.argv[1])
else:
    filename = 'data/out_32.bin'

SCALE_32 = 2**14 # Raw Data is shifted 14 bits right
data_mat = get_data32_raw(filename)
data, chopp = get_data32(data_mat)
#data = data_raw // SCALE_32
F = get_float(data_mat, 8)
dF = get_float(data_mat, 12)
absF = get_float(data_mat, 16)
absdF = get_float(data_mat, 20)
mdic = {'data_32': data, 'chopp': chopp, 'F':F, 'dF': dF,
        'absF':absF, 'absdF': absdF}

file_out = filename[:-4] + '.mat'
savemat(file_out, mdic)
