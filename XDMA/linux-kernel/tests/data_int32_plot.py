#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 12 12:11:30 2020

@author: Bernardo Carvalho
"""

import sys
import numpy as np
import matplotlib.pyplot as plt

if len(sys.argv) > 1:
    filename = str(sys.argv[1])
else:
    filename = 'data/out_32.bin'

NUM_CHANNELS = 32

data_i32 = np.fromfile(filename, dtype='int32')
# ‘F’ means to readthe elements using Fortran-like index order,
# with the first index changing fastest,
data_i32 = np.reshape(data_i32, (NUM_CHANNELS, -1), order='F')
#data = np.fromfile(filename, dtype='int32')

dt = np.dtype('f4')   # 32-bit floating-point number
data = np.fromfile(filename, dtype=dt)
# ‘F’ means to readthe elements using Fortran-like index order,
# with the first index changing fastest,
data_float = np.reshape(data, (NUM_CHANNELS, -1), order='F')

fig, ax = plt.subplots()

# ax.plot(data_i32[0], label='ch0 d')  # plot columm 0
# ax.plot(data_i32[1], label='ch1 d')
# ax.plot(data_float[2], label='ch0 f')
# ax.plot(data_float[3], label='ch1 f')
# ax.plot(data_float[4], label='ch0 multf')
# ax.plot(data_float[5], label='ch1 multf')
# ax.plot(data_float[6], label='ch0 multf')
ax.plot(data_float[8], label='ch0 intF')
ax.plot(data_float[9], label='ch1 intF')
ax.plot(data_float[10], label='ch2 intF')
ax.plot(data_float[11], label='ch3 intF')
ax.plot(data_float[12], label='ch4 intF')
ax.plot(data_float[13], label='ch5 intF')
# ax.plot(data_float[14], label='ch6 intF')
# ax.plot(data_float[15], label='ch7 intF')
#ax.plot(data_float[3]/2**12, label='ch3')
#ax.plot(data_float[14], label='a7e')  # plot columm 0
#ax.plot(data_float[15], label='a7o')  # plot columm 1
#ax.plot(data_float[16], label='a8e')
#ax.plot(data_float[17], label='a8o')
#ax.plot(data_float[18], label='a9e')
#ax.plot(data_float[19], label='a9o')
#ax.plot(data_float[20], label='a10e')
# ax.plot(1.1*data_float[21], label='a10o')
#ax.plot(data_float[20], label='a10e')
#ax.plot(data_float[16], label='abs(accum)')
#ax.plot(data_float[17], label='acumm')
ax.set_xlabel('Sample/200')
ax.set_ylabel('LSB')
ax.set_title("ATCA v2 32 bit Integrated channels ")  # Add a title to the axes.
ax.legend()  # Add a legend.
color = 'tab:red'
# ax2 = ax.twinx()
linestyle = 'dashdot'
# ax2.plot(data_float[23], label='dF', color='red', linestyle=linestyle, linewidth=1.5 )
# ax2.plot(data_float[25], label='abs(dF)', linestyle=linestyle, linewidth=1.5)
#0x0D
# ax2.plot(data_float[27], label='abs(aBo) - 2e4', linestyle=linestyle, linewidth=1.5)
# ax2.set_ylim(-2e5, 2e5)
#ax2.plot(data_float[1] & 1,  label='prog_full')  # plot columm 1
#ax2.plot(data_float[1] & 2,  label='fifo_ready')  # plot columm 1
#ax2.plot(data_float[1] & 4,  label='axis_tready')  # plot columm 1
#ax2.plot(data_float[1] & 8,  label='axis_tvalid')  # plot columm 1
# ax2.legend()  # Add a legend.

plt.show()
