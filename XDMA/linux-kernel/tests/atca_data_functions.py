#!/usr/bin/env python3
# vim: sta:et:sw=4:ts=4:sts=4
"""
Extract embedded data from 32 bit packets
"""

import numpy as np

SCALE_32 = 2**14 # Data is shifted 14 bits

# ‘F’ means to readthe elements using Fortran-like index order,
# with the first index changing fastest,
# filename = self.line_edit.text()
# ‘F’ means to readthe elements using Fortran-like index order,
# with the first index changing fastest,
def get_data16(filename):
    data = np.fromfile(filename, dtype='int16')
    data_mat = np.reshape(data, (32, -1), order='F')
    return data_mat

def get_data32_raw(filename):
    data = np.fromfile(filename, dtype='int32')
    data_mat = np.reshape(data, (32, -1), order='F')
    return data_mat

# Functions to extract embedded data on 32-bit packets

def get_data32(data_mat):
    data_scaled = data_mat // SCALE_32
    chopp = data_mat[31] & 0x0001
    return data_scaled, chopp

def get_count64(data_mat):
    count = (data_mat[0] & 0xFF) | ((data_mat[1] & 0xFF) << 8)
    count = count | ((data_mat[3] & 0xFF) << 24) | ((data_mat[2] & 0xFF) << 16)
    count = count | ((data_mat[5] & 0xFF) << 40) | ((data_mat[4] & 0xFF) << 32)
    count = count | ((data_mat[7] & 0xFF) << 56) | ((data_mat[6] & 0xFF) << 48)
    return count

def get_int32(data_mat, first):
    valI  = ((data_mat[first+1] & 0xFF) << 8) | (data_mat[first] & 0xFF)
    valI |= ((data_mat[first+3] & 0xFF) << 24) | ((data_mat[first+2] & 0xFF) << 16)
    return valI

def get_float(data_mat, first):
    valFloat = ((data_mat[first+1] & 0xFF) << 8) | (data_mat[first] & 0xFF)
    valFloat = valFloat | ((data_mat[first+3] & 0xFF) << 24) | ((data_mat[first+2] & 0xFF) << 16)
    tobytes = valFloat.tobytes()
    return np.frombuffer(tobytes,dtype='f')
