#!/usr/bin/env python3
# vim: sta:et:sw=4:ts=4:sts=4
"""
Use a 
"""

import numpy as np

SCALE_32 = 2**14

# ‘F’ means to readthe elements using Fortran-like index order,
# with the first index changing fastest,
# filename =FILENAME32
# data = np.fromfile(filename, dtype='int32')
# data_mat = np.reshape(data, (32, -1), order='F')
def get_data_I32(filename):
    data = np.fromfile(filename, dtype='int32')
    data_raw = np.reshape(data, (32, -1), order='F')
    data_mat = (data_raw / SCALE_32).astype('int32')
    sh = data_mat.shape
    print(f'shape I32 is {sh}')
    print('I32 :11 ' + repr(data_mat[11,0:10]))
    return data_mat

def get_data_raw(filename):
    data = np.fromfile(filename, dtype='int32')
    data_raw = np.reshape(data, (32, -1), order='F')
    return data_raw

def get_data_float(filename):
    data = np.fromfile(filename, dtype='f4') # float
    data_mat = np.reshape(data, (32, -1), order='F')
    return data_mat

def get_data_I64(filename):
    data = np.fromfile(filename, dtype='int64')
    data_mat = np.reshape(data, (16, -1), order='F')
    # print('count mat ' + repr(data_mat[0,0:9]))
    # print('count end ' + repr(count[-10:-1]))
    return data_mat

def get_data_double(filename):
    # filename = le.text()
    data = np.fromfile(filename, dtype='f8') # double
    data_mat = np.reshape(data, (16, -1), order='F')
    return data_mat

def decode_chopp(data_raw):
    ch31 = data_raw[31];
    chopp = ch31 & 0x01
    return chopp

def decode_count64(data_raw):
#    count = (data_raw[0] & 0xFF).astype('uint64')
    data=data_raw.astype('uint64')
    count = (data[1] & 0xFF)  | ((data[1] & 0xFF) << 8)
    count = count | ((data[3] & 0xFF) << 24) | ((data[2] & 0xFF) << 16)
    count = count | ((data[5] & 0xFF) << 40) | ((data[4] & 0xFF) << 32)
    count = count | ((data[7] & 0xFF) << 56) | ((data[6] & 0xFF) << 48)
    return count

def decode_I32(data_raw, first):
    valI  = ((data_raw[first+1] & 0xFF) << 8) | (data_raw[first] & 0xFF)
    valI |= ((data_raw[first+3] & 0xFF) << 24) | ((data_raw[first+2] & 0xFF) << 16)
    return valI

def decode_float(data_raw, first):
    valFloat = ((data_raw[first+1] & 0xFF) << 8) | (data_raw[first] & 0xFF)
    valFloat = valFloat | ((data_raw[first+3] & 0xFF) << 24) | ((data_raw[first+2] & 0xFF) << 16)
    tobytes = valFloat.tobytes()
    return np.frombuffer(tobytes,dtype='f')


