#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 12 12:11:30 2020

@author: Bernardo Carvalho
"""

import sys
import numpy as np
import matplotlib.pyplot as plt

if len(sys.argv) > 1:
    FILENAME = str(sys.argv[1])
else:
    FILENAME = 'data/out_32.bin'

NUM_CHANNELS = 32

data = np.fromfile(FILENAME, dtype='int32')
# ‘F’ means to readthe elements using Fortran-like index order,
# with the first index changing fastest,
data_mat = np.reshape(data, (32, -1), order='F')
count = (data_mat[0] & 0xFF) | ((data_mat[1] & 0xFF) << 8)
count = count | ((data_mat[3] & 0xFF) << 24) | ((data_mat[2] & 0xFF) << 16)
count = count | ((data_mat[5] & 0xFF) << 40) | ((data_mat[4] & 0xFF) << 32)
count = count | ((data_mat[7] & 0xFF) << 56) | ((data_mat[6] & 0xFF) << 48)
dcount = np.diff(count)
print("Max cnt:{}".format(dcount[1:].max()))

func0 = ((data_mat[9] & 0xFF) << 8) | (data_mat[8] & 0xFF)
func0 = func0 | ((data_mat[11] & 0xFF) << 24) | ((data_mat[10] & 0xFF) << 16)
buffI = func0.tobytes()
Fdata = np.frombuffer(buffI, dtype='f')

func0 = ((data_mat[13] & 0xFF) << 8) | (data_mat[12] & 0xFF)
func0 = func0 | ((data_mat[15] & 0xFF) << 24) | ((data_mat[14] & 0xFF) << 16)
buffI = func0.tobytes()
Fdata1 = np.frombuffer(buffI, dtype='f')

data_mat = (np.reshape(data, (32, -1), order='F')/2**12).astype('int32')
data_u64 = np.fromfile(FILENAME, dtype='u8')
# ‘F’ means to readthe elements using Fortran-like index order,
# with the first index changing fastest,
data_u64 = np.reshape(data_u64, (NUM_CHANNELS//2, -1), order='F')

fig, ax = plt.subplots()

ax.plot(data_mat[24], label='ch24')
ax.plot(data_mat[25], label='ch25')
ax.plot(data_mat[26], label='ch26')
ax.plot(data_mat[27], label='ch27')
ax.plot(data_mat[28], label='ch28')
ax.plot(data_mat[29], label='ch29')
ax.plot(data_mat[30], label='ch30')
ax.plot(data_mat[31], label='ch31')
# ax.plot(data_mat[12]/2**12, label='ch12')
# ax.plot(data_mat[13]/2**12, label='ch13')
# ax.plot(data_mat[14]/2**12, label='ch14')
# ax.plot(data_mat[15]/2**12, label='ch15')
# ax.plot(data_mat[30] & 0xFFF, label='count')
# ax.plot(data_mat[30] & 0xFFF, label='count')
# ax.plot(data_mat[30], label='count0')
ax.set_xlabel('Sample')
ax.set_ylabel('LSB')
ax.set_title("ATCA v2 32 bit data")  # Add a title to the axes.
ax.legend(loc='upper left')  # Add a legend.
COLOR = 'tab:red'
ax2 = ax.twinx()
# ax2.plot(count, label='count rec')
# ax2.plot(Fdata, color='r', label='Fdata')
# ax2.plot(Fdata1[1:], color='r', label='Fdata1')
# ax2.set_ylim(-1, 10)
# ax2.plot(data_mat[31] & 1, color=color, label='ch0')  # plot columm 1
# ax2.plot(data_mat[0] & 1,  label='prog_full')  # plot columm 1
# ax2.plot(data_mat[30],  label='count32')
# ax2.plot(data_u64[15] ,  label='count64')  # plot columm 1
# ax2.plot(data_mat[29], label='wr_count')  # plot columm 1
# ax2.plot(data_mat[31] & 1,  label='chop')  # plot columm 1
# ax2.plot((data_mat[31] & 2) - 0.1,  label='m_tlast')  # plot columm 1
# ax2.plot((data_mat[0] & 4)-0.5,  label='axis_tready')  # plot columm 1
# ax2.plot((data_mat[0] & 8)-0.5,  label='axis_tvalid')  # plot columm 1
# ax2.plot((data_mat[1] & 1)-0.5,  label='axis_tready')  # plot columm 1
# ax2.plot((data_mat[1] & 2)-0.5,  label='axis_tvalid')  # plot columm 1
ax2.legend(loc='lower right')  # Add a legend.

plt.show()
