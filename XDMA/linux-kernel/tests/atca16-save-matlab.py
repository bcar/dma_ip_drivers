#!/usr/bin/env python3
# vim: sta:et:sw=4:ts=4:sts=4
"""
Extract data from binary files and save in MATLAB format
"""

import sys
import numpy as np
import scipy.io
from atca_data_functions import get_data16

if len(sys.argv) > 1:
    filename = str(sys.argv[1])
else:
    filename = 'data/out_16.bin'

data = get_data16(filename)
file_out = filename[:-4] + '.mat'

scipy.io.savemat(file_out, {'data_16_raw': data})
