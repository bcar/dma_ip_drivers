#!/usr/bin/env python3
# vim: set ts=8 sw=4 tw=0 et :
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 23 12:11:30 2021

@author: Bernardo Carvalho
"""

import sys
import numpy as np
import matplotlib.pyplot as plt
N_BINS = 200

if len(sys.argv) > 1:
    FILENAME = str(sys.argv[1])
else:
    FILENAME = 'time_data.csv'

data = np.genfromtxt(FILENAME, delimiter=",", usemask=True)
# data = np.fromfile(filename, dtype='int16')
# # ‘F’ means to readthe elements using Fortran-like index order,
# # with the first index changing fastest,
# data_mat = np.reshape(data, (32, -1), order='F')

# data_u64 = np.fromfile(filename, dtype='u8')
# data_u64 = np.reshape(data_u64, (8, -1), order='F')


fig, (ax0, ax1, ax2, ax3) = plt.subplots(4, tight_layout=True)

# lineObjects = ax.plot(data/1.0e6)  # plot 3 curves
# plt.legend(iter(lineObjects), ('foo', 'bar', 'baz'))

ax0.plot(data[:, 0]/1e6, label='read()')  # plot columm 0
ax0.plot(data[:, 1]/1e6, label='memcpy()')  # plot columm 0
ax0.plot(data[:, 2]/1e6, label='getdata()')  # plot columm 0
# ax.plot(data_mat[0], label='ch0')  # plot columm 0
# ax.plot(data_mat[11] , lab el='ch11')


ax0.set_xlabel('read() count #')
ax0.set_ylabel('time/us')
ax0.set_title("ATCA v2 32 data, streaming cycle times")
# Add a title to the axes.
ax0.legend(loc='lower right')  # Add a legend.
# ax2 = ax.twinx()
# ax2.set_ylim(-1, 5)
# ax2.plot(data_mat[31] & 1, color='red', label='chop')  # plot columm 1
# # ax2.plot(data_mat[0] & 1,  label='prog_full')  # plot columm 1
# #ax2.plot(data_u64[7] ,  label='count64')  # plot columm 1
# #ax2.plot(data_mat[31] & 1,  label='chop')  # plot columm 1
# ax0.hist(data[:, 0]/1e6, label='read()')  # plot columm 0
ax1.hist(data[:, 0]/1e6, range=(7, 8), bins=N_BINS)  # plot columm 0
ax2.hist(data[:, 1]/1e6, range=(0.25, 1.25), bins=N_BINS, color='#ff7f0e')
ax3.hist(data[:, 2]/1e6, range=(0, 0.2), bins=N_BINS, color='#2ca02c')
ax3.set_xlabel('time/us')
ax3.set_ylabel('counts')
# ax1.hist(data, bins=n_bins)
# ax2.hist(dist2, bins=n_bins)

# rng = np.hstack((rng.normal(size=1000),

# rng.normal(loc=5, scale=2, size=1000)))

# _ = plt.hist(a, bins='auto')  # arguments are passed to np.histogram

# plt.title("Histogram with 'auto' bins")
plt.show()
