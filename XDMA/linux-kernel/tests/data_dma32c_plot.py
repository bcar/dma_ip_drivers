#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 12 12:11:30 2020

@author: Bernardo Carvalho
"""

import sys
import numpy as np
import matplotlib.pyplot as plt

if len(sys.argv) > 1:
    filename = str(sys.argv[1])
else:
    filename = 'data/out_32.bin'

NUM_CHANNELS = 32

data = np.fromfile(filename, dtype='int32')
# ‘F’ means to readthe elements using Fortran-like index order,
# with the first index changing fastest,
data_mat = np.reshape(data, (32, -1), order='F')

data_u64 = np.fromfile(filename, dtype='u8')
# ‘F’ means to readthe elements using Fortran-like index order,
# with the first index changing fastest,
data_u64 = np.reshape(data_u64, (NUM_CHANNELS//2, -1), order='F')

fig, ax = plt.subplots()

ax.plot(data_mat[16]/2**12, label='ch16')
ax.plot(data_mat[17]/2**12, label='ch17')
ax.plot(data_mat[18]/2**12, label='ch18')
ax.plot(data_mat[19]/2**12, label='ch19')
ax.plot(data_mat[20]/2**12, label='ch20')
ax.plot(data_mat[21]/2**12, label='ch21')
ax.plot(data_mat[22]/2**12, label='ch22')
ax.plot(data_mat[23]/2**12, label='ch23')
# ax.plot(data_mat[12]/2**12, label='ch12')
# ax.plot(data_mat[13]/2**12, label='ch13')
# ax.plot(data_mat[14]/2**12, label='ch14')
# ax.plot(data_mat[15]/2**12, label='ch15')
# ax.plot(data_mat[30] & 0xFFF, label='count')
# ax.plot(data_mat[30] & 0xFFF, label='count')
#ax.plot(data_mat[30], label='count0')
ax.set_xlabel('Sample')
ax.set_ylabel('LSB')
ax.set_title("ATCA v2 32 bit data")  # Add a title to the axes.
ax.legend(loc='upper left')  # Add a legend.
COLOR = 'tab:red'
# ax2 = ax.twinx()
# ax2.set_ylim(-1, 10)
# ax2.plot(data_mat[31] & 1, color=color, label='ch0')  # plot columm 1
# ax2.plot(data_mat[0] & 1,  label='prog_full')  # plot columm 1
# ax2.plot(data_u64[15] ,  label='count64')  # plot columm 1
# ax2.plot(data_mat[29], label='wr_count')  # plot columm 1
# ax2.plot(data_mat[31] & 1,  label='chop')  # plot columm 1
# ax2.plot((data_mat[31] & 2) - 0.1,  label='m_tlast')  # plot columm 1
# ax2.plot((data_mat[0] & 4)-0.5,  label='axis_tready')  # plot columm 1
# ax2.plot((data_mat[0] & 8)-0.5,  label='axis_tvalid')  # plot columm 1
# ax2.plot((data_mat[1] & 1)-0.5,  label='axis_tready')  # plot columm 1
# ax2.plot((data_mat[1] & 2)-0.5,  label='axis_tvalid')  # plot columm 1
# ax2.legend(loc='lower right')  # Add a legend.

plt.show()
