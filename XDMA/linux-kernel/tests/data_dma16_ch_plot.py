#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 12 12:11:30 2020

@author: Bernardo Carvalho
"""

import sys
import numpy as np
import matplotlib.pyplot as plt

if len(sys.argv) > 1:
    filename = str(sys.argv[1])
else:
    filename = 'data/out_32.bin'
#    'dataDMA16.bin'

data = np.fromfile(filename, dtype='int16')
# ‘F’ means to readthe elements using Fortran-like index order,
# with the first index changing fastest,
data_mat = np.reshape(data, (32, -1), order='F')

data_u64 = np.fromfile(filename, dtype='u8')
data_u64 = np.reshape(data_u64, (8, -1), order='F')


fig, ax = plt.subplots()

ax.plot(data_mat[0], label='ch0')  # plot columm 0
# ax.plot(data_mat[1], label='ch1')  # plot columm 1
# ax.plot(data_mat[2], label='ch2')
# ax.plot(data_mat[3], label='ch3')
# ax.plot(data_mat[4], label='ch4')
# ax.plot(data_mat[5], label='ch5')
# ax.plot(data_mat[6], label='ch6')
# ax.plot(data_mat[7], label='ch7')
# ax.plot(data_mat[8], label='ch8')
# ax.plot(data_mat[9], label='ch9')
# ax.plot(data_mat[10], label='ch10')
# ax.plot(data_mat[11], label='ch11')
ax.set_xlabel('Sample')
ax.set_ylabel('LSB')
ax.set_title("ATCA v2 16 bit data")  # Add a title to the axes.
ax.legend()  # Add a legend.
ax2 = ax.twinx()
ax2.set_ylim(-1, 5)
ax2.plot(data_mat[31] & 1, color='red', label='chop')  # plot columm 1
# ax2.plot(data_mat[0] & 1,  label='prog_full')  # plot columm 1
# ax2.plot(data_u64[7] ,  label='count64')  # plot columm 1
# ax2.plot(data_mat[31] & 1,  label='chop')  # plot columm 1

plt.show()
