#!/bin/bash

line="$(cat /proc/interrupts | grep xdma)"
irq=${line:1:2}
start=1
cpumask=f0

#cpumask=$(bc <<< "2^$cpu")
#cpumask=$(bc <<< "obase=16;$cpumask")
	
echo "Setting CPU irq $irq, mask = $cpumask"

echo $cpumask > /proc/irq/$irq/smp_affinity

cpu_affinity=`cat /proc/irq/$irq/smp_affinity`

echo "Value of /proc/irq/$irq/smp_affinity = $cpu_affinity"

