/*
 * vim: set ts=8 sw=4 tw=0 et :
 * This file is part of the Xilinx DMA IP Core driver tool for Linux
 *
 * Copyright (c) 2016-present,  Xilinx, Inc.
 * All rights reserved.
 *
 * This source code is licensed under BSD-style license (found in the
 * LICENSE file in the root directory of this source tree)
 */
#define _POSIX_C_SOURCE 200809L
#define _DEFAULT_SOURCE
#define _XOPEN_SOURCE 500
#include <fcntl.h>
#include <getopt.h>
//#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <pthread.h>

#include "dma_utils.c"
#include "atca-v2-daq-lib.h"

#define COUNT_DEFAULT 10
#define TIME_COLS  5
#define PRINT_CYCLE 10

#define FATAL do { fprintf(stderr, "Error at line %d, file %s (%d) [%s]\n", __LINE__, __FILE__, errno, strerror(errno)); goto out; } while(0)

#define MAX_EQUAL 10  // max allowable consective zeros sampple in channels
#define MAX_JUMP 500 // max data difference between consective samples

#define N_CHECKS 8 // max data difference between consective samples

//short lastData[NUM_CHAN];
//short countEqual[NUM_CHAN];;

static uint32_t chan_mask = 0xFFFFFFFF;

typedef struct {
    uint32_t size, channel;
    uint64_t fixed;
    short last_data, repeat, njump, pjump, cnt_jump;
    short * data;
} Check_str;

void * th_check_errors(void * ptr) {
    Check_str * ch_s = (Check_str * ) ptr;
    short * data_ptr = ch_s->data;
    unsigned max_eq = ch_s->size/100;  // Check if 1%  of packet is fixed
    unsigned ch = ch_s->channel;
    short data16, s_diff;

    if ( (1 << ch) & chan_mask) {  // Ig channel is masked, skip test
        for(int j = 0; j < ch_s->size / ADC_CHANNELS / sizeof(short); j++) {
            data16 = data_ptr[ADC_CHANNELS * j + ch]; // get sample data for this channel
            s_diff = data16 - ch_s->last_data;
            if (s_diff == 0) { // Is adc value repeated?
                ch_s->repeat++;
                if(ch_s->repeat > max_eq){
                    printf("MAX Equal s:%d,c:%d\t", j, ch);
                    ch_s->fixed++;
                }
            }
            else {  //No, reset counter and store last sample
                ch_s->repeat = 0;
                ch_s->last_data = data16;
            }
            if (s_diff > ch_s->pjump) { // Is + jump too big?
                ch_s->pjump = s_diff;
            }
            if ( -s_diff > ch_s->njump) { // Is - jump too big?
                ch_s->njump = -s_diff;
            }
            if ((s_diff > MAX_JUMP) || ( -s_diff > MAX_JUMP )) { // Is jump too big??
                ch_s->cnt_jump++;
            }
        }

    }
}

static volatile int keep_running = 1;
void sig_handler(int dummy) {
    keep_running = 0;
}

static int eop_flush = 0;
static int d_mode = 0;
static int chopp_en = 0;

int main(int argc, char *argv[]){
    int cmd_opt;
    ssize_t rc = 0;
    /*char *device = DEVICE_NAME_DEFAULT;*/
    uint64_t aperture = 0;
    uint64_t dma_size = SIZE_DEFAULT;
    /*uint64_t offset = 0;*/
    uint64_t count = COUNT_DEFAULT;
    uint64_t nsamples;
    unsigned int nsamp_pkt;
    unsigned int chopper_period = 0;
    size_t bytes_done = 0;
    char *ofname = "data/out_16.bin";
    FILE * out_fd;
    int fd_bar_0, fd_bar1_c2h_0;
    int fd_bar1_c2h_1 = 0;
    struct timespec ts_start, ts_end;
    long total_time = 0; //, t0;
    int  device = 0;
    void *map_base;//, *virt_addr;
    char *allocated = NULL;
    uint32_t shapi_ver, fw_version;
    shapi_device_info * pDevice;
    shapi_module_info * pModule;
    STATUS_FLDS * pStatusBits;
    //uint64_t  total_fix;

    void *pAdcData=NULL;
    void *pAdcDataWr = NULL;
    short * pData;
    //    struct atca_eo_config eo_config;
    //    struct atca_wo_config wo_config;
    //long (*time_array)[TIME_COLS];   // mind the parenthesis!
    short max_pjump = 0;
    short max_njump = 0;
    uint64_t cnt_jump, total_fix;
    Check_str check_strc[N_CHECKS];
    pthread_t thrd_id[N_CHECKS];
    int t_ret[N_CHECKS];

    signal(SIGINT, sig_handler);
    //while ((cmd_opt = getopt(argc, argv, "vheb:p:c:f:d:m:s:o:")) != -1) {
    while ((cmd_opt = getopt(argc, argv, "c:d:m:s:")) != -1) {
	switch (cmd_opt) {
	    case 0:
		/* long option */
		break;
	    case 'c':
		count = getopt_integer(optarg);
		break;
	    case 'd':
		device = getopt_integer(optarg);
		break;
	    case 'm':
		chan_mask = getopt_integer(optarg);
		break;
	    case 's':
		/* DMA size in bytes */
		dma_size = getopt_integer(optarg);
		break;
	}
    }
    // optind is for the extra arguments
    // which are not parsed
    for(; optind < argc; optind++){     
	printf("extra arguments: %s\n", argv[optind]);
    }
    nsamp_pkt = dma_size / PCKT_SIZE_16BIT;// sizeof(short);
    nsamples = nsamp_pkt * count;

    posix_memalign((void **) &allocated, 4096 /*alignment */ , dma_size + 4096);
    if (!allocated) {
	fprintf(stderr, "OOM %lu.\n", dma_size + 4096);
	rc = -ENOMEM;
	fflush(stdout);
	goto out;
    }
    pData = (short *) allocated;

    map_base = atca_init_device(device, chopper_period, &fd_bar_0, &fd_bar1_c2h_0);
    if (map_base == (void *)-1){
	printf("ATCA Device %d not opened\n", device);
	FATAL;
    }

    pDevice = (shapi_device_info *) map_base;
    shapi_ver =  pDevice->SHAPI_VERSION;
    if (shapi_ver == -1)
	FATAL;
    printf("SHAPI VER: 0x%X\t", shapi_ver);
    printf("%s\n", read_fw_timestamp_str(map_base));
    pModule = (shapi_module_info *) (map_base +
	    pDevice->SHAPI_FIRST_MODULE_ADDRESS);
    pStatusBits = (STATUS_FLDS *) & pModule->atca_status;
    printf("SHAPI MOD VER: 0x%X\n", pModule->SHAPI_VERSION);
    fw_version = read_fw_version(map_base);
    printf("FW Version: 0x%.8X, MAJOR: %d, MINOR: %d, PATCH:%d\n", fw_version,
            (fw_version & 0xFF000000)>>24, (fw_version & 0x00FF0000)>>16,
            (fw_version & 0xFFFF) );

//    printf("Control reg: 0x%08X\n", read_control_reg(map_base));

    //memset(lastData, 0, sizeof(lastData));
    //memset(countEqual, 0, sizeof(countEqual));
    //printf("Channel Mask: 0x%08X\n", read_channel_mask(map_base));
    atca_soft_reset(map_base);
    write_channel_mask(map_base, chan_mask);
    printf("Channel Mask: 0x%08X\n", read_channel_mask(map_base));

    atca_arm_acq(map_base);
    /*printf("Control reg: 0x%08X\n", read_control_reg(map_base));*/
    memset(check_strc, 0, N_CHECKS * sizeof(Check_str));
    for (int c = 0; c < N_CHECKS; c++){
        check_strc[c].data = (short *) allocated;
        check_strc[c].size = dma_size;
        check_strc[c].channel = c;
    }
    atca_soft_trigger(map_base);
    rc = read(fd_bar1_c2h_0, allocated, dma_size);
    if (rc < 0) {
	printf("Read error. Out rc = %d\n", rc);
	FATAL;
    }
    for (int c = 0; c < N_CHECKS; c++){
	check_strc[c].last_data = pData[((nsamp_pkt-1) * ADC_CHANNELS) + c]; // store last sample in first packet
        //lastData[c] = pData[((nsamp_pkt-1) * NUM_CHAN) + c]; // store last sample in first packet
    }
    rc = clock_gettime(CLOCK_MONOTONIC, &ts_start);
    for (uint64_t i = 0; i < count; i++) {
	if(!keep_running){
	    printf("\n OUCH!, you hit Ctrl-C!\n");
	    break;
	}
	rc = read(fd_bar1_c2h_0, allocated, dma_size);
	if (rc < 0) {
	    printf("Read error. Out rc = %d\n", rc);
	    break;
	}
	bytes_done += rc;
        for (int c = 0; c < N_CHECKS; c++)
            t_ret[c] = pthread_create( &thrd_id[c], NULL, th_check_errors, (void*) &check_strc[c]);

        total_fix = 0;
        cnt_jump = 0;
        for (int c = 0; c < N_CHECKS; c++){
            pthread_join(thrd_id[c], NULL);
            total_fix += check_strc[c].fixed;
            cnt_jump += check_strc[c].cnt_jump;
            if (check_strc[c].pjump > max_pjump)
                max_pjump = check_strc[c].pjump;
            if (check_strc[c].njump > max_njump)
                max_njump = check_strc[c].njump;
        }
        //total_fix += check_errors(dma_size, (short *) allocated);
	if (((i%PRINT_CYCLE)==0))
	    printf("Pkt:%10lu, Total Fixed:%15lu, max_pjump:%7d, max_njump:%7d, cnt_jump:%15lu\r",
		    i, total_fix, max_pjump, max_njump, cnt_jump);

    }

    clock_gettime(CLOCK_MONOTONIC, &ts_end);
    printf("\nEND Status reg: 0x%08X\n", read_status_reg(map_base));
    write_control_reg(map_base, 0);

    /* subtract the start time from the end time */
    timespec_sub(&ts_end, &ts_start);
    total_time= 1e9*ts_end.tv_sec + ts_end.tv_nsec;
    //printf("CLOCK_MONOTONIC %ld.%ld sec., total %ld MB, packet size: %ld kB\n",
    printf("Total Time %ld.%ld s, total %ld MB, packet size: %ld kB\n",
	    ts_end.tv_sec, ts_end.tv_nsec, bytes_done/1024/1024, dma_size/1024);
    printf("Average BW = %.2f MB/s\n", bytes_done*1000.0/((float)(total_time)));
    //    printf("Count misses: %d, First M Pckt: %d, Last Pckt: %d last_cnt %d, 0x%08X, Fixed:%d\n", cnt_misses, first_m_pckt, nrpk, last_cnt, last_cnt, cnt_fixed);

    printf("Total Fixed:%lu, max_pjump:%d, max_njump:%d, cnt_jump:%lu\n",
	    total_fix, max_pjump, max_njump, cnt_jump);
    out_fd = fopen(ofname, "wb");
    if (out_fd == NULL) {
	fprintf(stderr, "unable to open output file %s, %d.\n",
		ofname, out_fd);
	perror("open output file");
	rc = -EINVAL;
	FATAL;
    }
    else{
	fwrite(allocated, 1, dma_size, out_fd);
	fclose(out_fd);
    }
out:
    close(fd_bar_0);
    close(fd_bar1_c2h_0);
    if(fd_bar1_c2h_1)
	close(fd_bar1_c2h_1);
    if (allocated)
	free(allocated);
    if (pAdcData)
	free(pAdcData );

//    if (time_array)
//	free(time_array);
    return rc;
}
