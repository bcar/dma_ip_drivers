/*
 * This file is part of the Xilinx DMA IP Core driver tool for Linux
 *
 * Copyright (c) 2016-present,  Xilinx, Inc.
 * All rights reserved.
 *
 * This source code is licensed under BSD-style license (found in the
 * LICENSE file in the root directory of this source tree)
 */
#define _POSIX_C_SOURCE 200809L
#define _DEFAULT_SOURCE
// #define _BSD_SOURCE  deprecated
#define _XOPEN_SOURCE 500
//#include <assert.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
//#include <string.h>
//#include <unistd.h>
#include <time.h>
#include <signal.h>
#include <libconfig.h>

//#include <sys/mman.h>
//#include <sys/stat.h>
#include <sys/time.h>
/*#include <sys/types.h>*/

#include "dma_utils.c"
#include "atca-v2-daq-lib.h"

#define DEVICE_NUM_DEFAULT 0

#define SIZE_DEFAULT (32)
#define COUNT_DEFAULT (1)
#define TIME_COLS  5

//#define FATAL do { fprintf(stderr, "Error at line %d, file %s (%d) [%s]\n", __LINE__, __FILE__, errno, strerror(errno)); exit(1); } while(0)
#define FATAL do { fprintf(stderr, "Error at line %d, file %s (%d) [%s]\n", __LINE__, __FILE__, errno, strerror(errno)); goto out; } while(0)

#define MAP_SIZE (32*1024UL)

#define MAP_MASK (MAP_SIZE - 1)

static volatile int keep_running = 1;
void sig_handler(int dummy) {
    keep_running = 0;
}


static int eop_flush = 0;
static int d_32bit = 0;
static int chopp_en = 0;
static unsigned int chopper_period =0;

int main(int argc, char *argv[])
{
    int cmd_opt;
    /*char *device = DEVICE_NAME_DEFAULT;*/
    uint64_t aperture = 0;
    uint64_t size = SIZE_DEFAULT;
    uint32_t chan_mask = 0xFFFFFFFF;
    /*uint64_t offset = 0;*/
    uint64_t count = COUNT_DEFAULT;
    char *ofname = NULL;
    ssize_t rc = 0;
    size_t bytes_done = 0;
    uint64_t i;
    //    uint64_t apt_loop = aperture ? (size + aperture - 1) / aperture : 0;
    /*char *buffer = NULL;*/
    struct timespec ts_start, ts_end;
    struct timespec ts_rd_start, ts_rd_end;
    int fd_bar_0, fd_bar1_c2h_0;
    int  device = 0;
    long total_time = 0;
    float result;
    void *map_base;//, *virt_addr;
    uint32_t read_result, control_reg, fw_version, val;
    uint32_t shapi_ver;
    uint32_t i2c_reg;
    shapi_device_info * pDevice;
    shapi_module_info * pModule;
    STATUS_FLDS * pStatusBits;
    map_base = atca_init_device(device, chopper_period, &fd_bar_0, &fd_bar1_c2h_0);
    if (map_base == (void *)-1){
        printf("ATCA Device %d not opened\n", device);
        FATAL;
    }
    fflush(stdout);

    pDevice = (shapi_device_info *) map_base;
    shapi_ver =  pDevice->SHAPI_VERSION;
    if (shapi_ver == -1){
        FATAL;
    }
    printf("SHAPI VER: 0x%X\t", shapi_ver);
    pModule = (shapi_module_info *) (map_base +
            pDevice->SHAPI_FIRST_MODULE_ADDRESS);
    pStatusBits = (STATUS_FLDS *) & pModule->atca_status;
    printf("SHAPI MOD VER: 0x%X\n", pModule->SHAPI_VERSION);
    printf("i2c_reg0: 0x%08X, i2c_reg1: 0x%08X\n", pModule->i2c_reg0, pModule->i2c_reg1);
    write_control_reg(map_base, 0);
    usleep(10);
    /*control_reg = (1<<ACQ_EN_BIT);//0x00800000; // acq enab + dma16bit*/
    if(d_32bit == 1)
        control_reg |= (1<<DMA_DATA32_BIT);
    /* swap 32-bit endianess if host is not little-endian no need in x86 hw*/
    //    control_reg = htoll(control_reg);
    //    *((uint32_t *) virt_addr) = control_reg;
    if(chopper_period > 0) {
        control_reg |= (1<<CHOP_EN_BIT);
        write_chopp_period(map_base, chopper_period);
    }
    write_control_reg(map_base, control_reg);

    //    read_result = *((uint32_t *) virt_addr);
    /*printf("FW_TS: %ld\n", read_fw_timestamp(map_base));*/
    printf("%s\n", read_fw_timestamp_str(map_base));
    fw_version = read_fw_version(map_base);
    printf("FW Version: 0x%.8X, MAJOR: %d, MINOR: %d, PATCH:%d\n", fw_version,
            (fw_version & 0xFF000000)>>24, (fw_version & 0x00FF0000)>>16,
            (fw_version & 0xFFFF) );
    //     printf("FW Timestamp: %u\n", read_fpga_reg(map_base, TIME_STAMP_REG_OFF));
    printf("Status reg: 0x%08X, (expected 0x007E0045, for IPFN ATCA crate, top slot )\n", read_status_reg(map_base));
    /*printf("Status Bits, SLOT: %d TE741_LCK: %d \n", pStatusBits->SLOT_ID, pStatusBits->TE741_LCK);*/
    printf("Status reg: 0x%08X\n", read_status_reg(map_base));
    printf("Control reg: 0x%08X\n", read_control_reg(map_base));
    /*printf("STA: 0x%X\n", pModule->atca_status);*/
    /*printf("CTR: 0x%X\n", pModule->atca_control);*/
    /*printf("CHO: 0x%.8X\n", pModule->atca_chop_period);*/
    printf("MAX : 0x%X\n", pModule->atca_max_dma_size);
    /*printf("TLP : 0x%X\n", pModule->atca_max_tlp_size);*/
    /*printf("EO 1: %d\n", pModule->atca_eo_offsets[1]);*/

    printf("I2C_REG_0: 0x%08X\n", read_fpga_reg(map_base, I2C_REG_0_OFF));
    printf("TMP101: 0x%08X\n", read_fpga_reg(map_base, TMP101_0_1_OFF));
    printf("MCP9808: 0x%08X\n", read_fpga_reg(map_base, MCP9808_0_1_OFF));
    i2c_reg = read_fpga_reg(map_base, I2C_REG_1_OFF);
    printf("I2C_REG_1: 0x%08X\n", i2c_reg);
    i2c_reg |= 0x80000000;  // Ask IPMC to RESET fpga
    write_fpga_reg(map_base, 0x2C, 0x80000000);
    printf("I2C_REG_1: 0x%08X\n", read_fpga_reg(map_base, I2C_REG_1_OFF));
    /*write_channel_mask(map_base, 0xFFFFFFFF);*/
    write_channel_mask(map_base, chan_mask);
    printf("Channel Mask: 0x%08X\n", read_channel_mask(map_base));

    //control_reg |= (1<<STRG_BIT);
    //write_control_reg(map_base, control_reg);
    rc = clock_gettime(CLOCK_MONOTONIC, &ts_start);
    int stat_error = 0;
    write_control_reg(map_base, 0);
    /* subtract the start time from the end time */
    timespec_sub(&ts_end, &ts_start);
    total_time= 1e9*ts_end.tv_sec + ts_end.tv_nsec;
    //total_time += ts_end.tv_nsec;
    printf("** Average BW = %.2f MB/s\n", bytes_done*1000.0/((float)(total_time)));

out:
    close(fd_bar_0);
    close(fd_bar1_c2h_0);
    /*close(fd_bar_1);*/

    return rc;
}
