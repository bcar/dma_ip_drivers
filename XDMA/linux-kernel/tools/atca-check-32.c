/*
 * vim: set ts=8 sw=4 tw=0 et :
 * This file is part of the Xilinx DMA IP Core driver tool for Linux
 *
 * Copyright (c) 2016-present,  Xilinx, Inc.
 * All rights reserved.
 *
 * This source code is licensed under BSD-style license (found in the
 * LICENSE file in the root directory of this source tree)
 */
#define _POSIX_C_SOURCE 200809L
#define _DEFAULT_SOURCE
#define _XOPEN_SOURCE 500
//#include <fcntl.h>
#include <getopt.h>
//#include <stdint.h>
#include <stdlib.h>
//#include <time.h>
#include <signal.h>
#include <pthread.h>

#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>       /* for AF_INET */
#include <arpa/inet.h>


#include "dma_utils.c"
#include "atca-v2-daq-lib.h"

#define NUM_BUFFS 2
#define COUNT_DEFAULT 10
//#define TIME_COLS  5
//#define PRINT_CYCLE 111

#define MAX_UDP_SIZE (0xFFFF -28) // 65635 -28
#define UDP_PORT 3333

#define PBWIDTH 50

#define FATAL do { fprintf(stderr, "Error at line %d, file %s (%d) [%s]\n", __LINE__, __FILE__, errno, strerror(errno)); goto out; } while(0)

//#define MAX_EQUAL 10  // max allowable consective zeros sampple in channels
#define MAX_JUMP (0x800 << 14) // max data difference between consective samples. (Data is 14 left shifted)

#define N_CHECKS 8 // max

float fval;

typedef struct {
    uint32_t size, channel, chan_mask;
    uint64_t fixed;
    int last_data, repeat, njump, pjump, cnt_jump;
    int * data;
} Check_str;

// ssize_t sendto(int sockfd, const void *buf, size_t len, int flags,
//                      const struct sockaddr *dest_addr, socklen_t addrlen);
typedef struct {
    int sockfd;
    void *buf;
    size_t len;
    int flags;
    struct sockaddr *dest_addr;
    socklen_t addrlen;

    ssize_t to_send, sent;
} udp_strct;

int ret1,ret2;

void * th_udp_send (void * ptr) {
    udp_strct * udp_s = (udp_strct * ) ptr;
    size_t len, sent, to_send = udp_s->to_send;
    void * buf_ptr = udp_s->buf;
    while (to_send > 0) {
        if((len = to_send) > udp_s->len)
            len = udp_s->len;
        if((sent = sendto(udp_s->sockfd, buf_ptr, len, 0,
                        udp_s->dest_addr, udp_s->addrlen)) == -1){
            //printf("Send error\n");
            perror("Send error");
            ret1 = -1;
            pthread_exit(&ret1);
            //exit(1);
        }
        udp_s->sent += sent;
        buf_ptr += sent;
        to_send -=sent;
    }
    ret1 = 0;
    pthread_exit(&ret1);
    //return __VOID(0); /* Not needed, but this makes the compiler smile */

}

void * th_check_errors(void * ptr) {
    Check_str * ch_s = (Check_str * ) ptr;
    int * data_ptr = ch_s->data;
    unsigned max_eq = ch_s->size/100;  // Check if 1%  of packet is fixed
    unsigned ch = ch_s->channel;
    uint32_t chan_mask = ch_s->chan_mask;
    int * pData32;
    int s_diff;

    if ( (1 << ch) & chan_mask) {  // If channel is masked, skip test
        for(int j = 0; j < ch_s->size / ADC_CHANNELS / sizeof(int); j++) {
            pData32 = data_ptr + ADC_CHANNELS * j + ch; // get sample data for this channel
            //for(int k = 0; k < 4; k++)
            //  fval += get_float_val(pData32, 8 + 4 * k); //  pSamp, int offset){
            s_diff = *pData32 - ch_s->last_data;
            if (s_diff == 0) { // Is adc value repeated?
                ch_s->repeat++;
                if(ch_s->repeat > max_eq){
                    printf("MAX Equal s:%d,c:%d\t", j, ch);
                    ch_s->fixed++;
                }
            }
            else {  //No, reset counter and store last sample
                ch_s->repeat = 0;
                ch_s->last_data = *pData32;
            }
            if (s_diff > ch_s->pjump) { // Is + jump too big?
                ch_s->pjump = s_diff;
            }
            if ( -s_diff > ch_s->njump) { // Is - jump too big?
                ch_s->njump = -s_diff;
            }
            if ((s_diff > MAX_JUMP) || ( -s_diff > MAX_JUMP )) { // Is jump too big??
                ch_s->cnt_jump++;
            }
        }

    }
}

static volatile int keep_running = 1;
void sig_handler(int dummy) {
    keep_running = 0;
}

static int eop_flush = 0;
static int d_mode = 0;
static int chopp_en = 0;
static int udp_en = 0;

static struct option const long_opts[] = {
    {"device", required_argument, NULL, 'd'},
    {"mask", required_argument, NULL, 'm'},
    {"size", required_argument, NULL, 's'},
    {"count", required_argument, NULL, 'c'},
    {"udp", required_argument, NULL, 'u'},
    {"help", no_argument, NULL, 'h'},
    {0, 0, 0, 0}
};

static void usage(const char *name)
{
    int i = 0;
    fprintf(stdout, "%s\n\n", name);
    fprintf(stdout, "usage: %s [OPTIONS]\n\n", name);
    //fprintf(stdout, "Read via SGDMA, optionally save output to a file\n\n");

    fprintf(stdout, "  -%c (--%s) device. Defaults to %d\n",
            long_opts[i].val, long_opts[i].name, DEVICE_NUM_DEFAULT);
    i++;
    fprintf(stdout, "  -%c (--%s) channel mask. Default 0x%08X (all channels acquired)\n",
            long_opts[i].val, long_opts[i].name, CHANNEL_MASK_DEFAULT);
    i++;
    fprintf(stdout,
            "  -%c (--%s) size of a single transfer in bytes, default %d kB.\n",
            long_opts[i].val, long_opts[i].name, SIZE_DEFAULT/1024);
    i++;
    fprintf(stdout, "  -%c (--%s) number of transfers, default is %d.\n",
            long_opts[i].val, long_opts[i].name, COUNT_DEFAULT);
    i++;
    fprintf(stdout, "  -%c (--%s) Enable UDP send packets, default is No.\n",
            long_opts[i].val, long_opts[i].name);
    i++;
    fprintf(stdout, "  -%c (--%s) print usage help and exit\n",
            long_opts[i].val, long_opts[i].name);
    i++;
    fprintf(stdout, "\nExample:\n");
    fprintf(stdout, "../tools/atca-check-32 -c 1000 -s 0x400000 -m 0xFF\n");
    fprintf(stdout, "\nReturn code:\n");
    fprintf(stdout, "  0: all bytes were dma'ed successfully\n");
    fprintf(stdout, "  < 0: error\n\n");
}


int connect_sock(int port, struct sockaddr_in * remote_addr){
    /* Hostname to ip Starts here */
    char ip[100];

    struct hostent *he;
    struct in_addr **addr_list;
    const char * hname = "localhost";
    int sockfd, sent_bytes;

    if ( (he = gethostbyname(hname) ) == NULL){
        herror("gethostbyname ");
        return -1;
    }
    printf("hname %s\n", hname);
    addr_list = (struct in_addr **) he->h_addr_list;

    for(int i = 0; addr_list[i] != NULL; i++){
        strcpy(ip , inet_ntoa(*addr_list[i]) );
    }
    struct sockaddr_in local_addr;

    if((sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1){
        perror("Socket ");
        return sockfd;
        //exit(1);
    }
    else 
        printf("Socket %d to %s\n", sockfd, ip);

    remote_addr->sin_family = AF_INET;
    remote_addr->sin_port = htons(UDP_PORT); //htons(atoi(argv[2]));
    remote_addr->sin_addr.s_addr = inet_addr(ip);
    memset(remote_addr->sin_zero, '\0', sizeof(remote_addr->sin_zero));

    int remote_connect = connect(sockfd, (struct sockaddr *)remote_addr, sizeof(struct sockaddr));
    if(remote_connect == -1){
        perror("Connect ");
        // exit(1);
    }
    else
        printf("Connected %d to %s\n", remote_connect, ip);

    return sockfd;
}

int main(int argc, char *argv[]){
    int cmd_opt;
    ssize_t rc = 0;
    uint64_t aperture = 0;
    uint64_t print_cycle, dma_size = SIZE_DEFAULT;
    uint64_t count = COUNT_DEFAULT;
    uint64_t nsamples;
    unsigned int nsamp_pkt;
    unsigned int chopper_period = 2000;
    size_t bytes_done = 0;
    char * ofname = "data/out_32.bin";
    FILE * out_fd;
    int fd_bar_0, fd_bar1_c2h_0;
    int fd_bar1_c2h_1 = 0;
    struct timespec ts_start, ts_end;
    long total_time = 0; //, t0;
    int  device = 0;
    int  buff_num = 0;
    void *map_base;
    char * allocated[NUM_BUFFS] = {NULL, NULL};
    char *allocated1 = NULL;
    uint32_t shapi_ver, fw_version;
    shapi_device_info * pDevice;
    shapi_module_info * pModule;
    STATUS_FLDS * pStatusBits;
    uint64_t sent_bytes;
    struct sockaddr_in remote_addr;
    udp_strct udp_pckt_str;

    udp_pckt_str.sockfd = connect_sock(UDP_PORT, &remote_addr);
    udp_pckt_str.flags = 0;
    //int slen = sizeof(struct sockaddr_in);
    udp_pckt_str.addrlen = sizeof(struct sockaddr_in);

    /* Ends Here */

    struct atca_eo_config eo_config;
    struct atca_wo_config wo_config;
    struct  atca_ilck_params ilck_params;
    //long (*time_array)[TIME_COLS];   // mind the parenthesis!
    int max_pjump = 0;
    int max_njump = 0;
    uint64_t cnt_jump=0, total_fix=0;
    uint64_t c2hcounter, next_count, miss_count = 0;
    Check_str check_strc[N_CHECKS];
    pthread_t thrd_id[N_CHECKS];
    int t_ret[N_CHECKS];
    pthread_t thrd_udp;
    uint32_t chan_mask = CHANNEL_MASK_DEFAULT;

    signal(SIGINT, sig_handler);
    while ((cmd_opt = getopt(argc, argv, "huc:d:m:s:")) != -1) {
        switch (cmd_opt) {
            case 0:
                /* long option */
                break;
            case 'd':
                device = getopt_integer(optarg);
                break;
            case 'c':
                count = getopt_integer(optarg);
                break;
            case 'm':
                chan_mask = getopt_integer(optarg);
                break;
            case 's':
                /* DMA size in bytes */
                dma_size = getopt_integer(optarg);
                break;
            case 'u':
                udp_en = 1;
                break;
            case 'h':
                /* print usage help and exit */
            default:
                usage(argv[0]);
                exit(0);
                break;
        }
    }
    // optind is for the extra arguments
    // which are not parsed
    for(; optind < argc; optind++){
        printf("extra arguments: %s\n", argv[optind]);
    }
    print_cycle = SIZE_DEFAULT * 128 / dma_size;
    nsamp_pkt = dma_size / PCKT_SIZE_32BIT;
    nsamples = nsamp_pkt * count;

    for (int i = 0; i < NUM_BUFFS; i++){
        posix_memalign((void **) &allocated[i], 4096 /*alignment */ , dma_size + 4096);
        if (!allocated[i]) {
            fprintf(stderr, "OOM %lu.\n", dma_size + 4096);
            rc = -ENOMEM;
            fflush(stdout);
            goto out;
        }
    }

    map_base = atca_init_device(device, chopper_period, &fd_bar_0, &fd_bar1_c2h_0);
    if (map_base == (void *)-1){
        printf("ATCA Device %d not opened\n", device);
        FATAL;
    }

    pDevice = (shapi_device_info *) map_base;
    shapi_ver =  pDevice->SHAPI_VERSION;
    if (shapi_ver == -1)
        FATAL;
    printf("SHAPI VER: 0x%X\t", shapi_ver);
    printf("%s\n", read_fw_timestamp_str(map_base));
    pModule = (shapi_module_info *) (map_base +
            pDevice->SHAPI_FIRST_MODULE_ADDRESS);
    pStatusBits = (STATUS_FLDS *) & pModule->atca_status;
    printf("SHAPI MOD VER: 0x%X\n", pModule->SHAPI_VERSION);
    fw_version = read_fw_version(map_base);
    printf("FW Version: 0x%.8X, MAJOR: %d, MINOR: %d, PATCH:%d\n", fw_version,
            (fw_version & 0xFF000000)>>24, (fw_version & 0x00FF0000)>>16,
            (fw_version & 0xFFFF) );

    memset(&eo_config, 0,   sizeof(struct atca_eo_config));
    memset(&wo_config, 0,   sizeof(struct atca_wo_config));
    memset(&ilck_params, 0, sizeof(struct  atca_ilck_params));
    //    printf("Control reg: 0x%08X\n", read_control_reg(map_base));

    atca_soft_reset(map_base);
    write_channel_mask(map_base, chan_mask);
    printf("Channel Mask: 0x%08X\n", read_channel_mask(map_base));
    ilck_params.fval[0] = 1.0;
    write_ilck_params(map_base, &ilck_params);
    atca_use_data32(map_base, 1);
    atca_arm_acq(map_base);
    memset(check_strc, 0, N_CHECKS * sizeof(Check_str));
    for (int c = 0; c < N_CHECKS; c++){
        check_strc[c].data = (int *) allocated[buff_num];
        check_strc[c].size = dma_size;
        check_strc[c].channel = c;
        check_strc[c].chan_mask = chan_mask;
    }
    atca_soft_trigger(map_base);
    rc = read(fd_bar1_c2h_0, allocated[buff_num], dma_size);
    if (rc < 0) {
        printf("Read error. Out rc = %d\n", rc);
        FATAL;
    }
    c2hcounter = get_counter_64bit( (uint32_t * ) (allocated[buff_num] + dma_size - PCKT_SIZE_32BIT));
    next_count = c2hcounter + nsamp_pkt; //dma_size / PCKT_SIZE_32BIT;

    int * pData = (int *) allocated[buff_num];
    for (int c = 0; c < N_CHECKS; c++){
        check_strc[c].last_data = pData[c]; // store first sample of packet
        t_ret[c] = pthread_create( &thrd_id[c], NULL, th_check_errors, (void*) &check_strc[c]);
    }
    udp_pckt_str.buf = allocated[buff_num];
    udp_pckt_str.len = MAX_UDP_SIZE;
    udp_pckt_str.to_send = dma_size; // 
    udp_pckt_str.dest_addr = (struct sockaddr*) &remote_addr;

    if(udp_en)
        pthread_create( &thrd_udp, NULL, th_udp_send, (void*) &udp_pckt_str);

    rc = clock_gettime(CLOCK_MONOTONIC, &ts_start);
    for (uint64_t i = 0; i < count; i++) {
        if(!keep_running){
            printf("\n OUCH!, you hit Ctrl-C!\n");
            break;
        }
        buff_num = (buff_num + 1) % NUM_BUFFS;
        rc = read(fd_bar1_c2h_0, allocated[buff_num], dma_size);
        if (rc < 0) {
            printf("Read error. Out rc = %d\n", rc);
            break;
        }
        c2hcounter = get_counter_64bit( (uint32_t * ) (allocated[buff_num] + dma_size - PCKT_SIZE_32BIT));

        if(c2hcounter != next_count)
            miss_count += c2hcounter - next_count;

        //        printf("Missed samples at pck: %ld c2hcounter: 0x%08X, %d\n", i, c2hcounter, c2hcounter);
        next_count = c2hcounter + dma_size / PCKT_SIZE_32BIT;

        bytes_done += rc;
        if ((i % print_cycle) == 0){
            //printf("%*c\r", 100, ' '); // print N spaces

// Pkt:       999, TotFix:         0, mpjump:     85, mnjump:     83, cnt_jump:       0, m:       0 0
            printf("\rPkt:%8lu, TotFix:%8lu, mpjump:%7d, mnjump:%7d, cnt_jump:%8u, m:%8u        ",
                    i, total_fix, max_pjump>>14, max_njump>>14, cnt_jump, miss_count);
            fflush(stdout);
        }
        fval = 0.0;
        total_fix = 0; // just add accumulators
        cnt_jump = 0;
        for (int c = 0; c < N_CHECKS; c++){
            pthread_join(thrd_id[c], NULL);
            total_fix += check_strc[c].fixed;
            cnt_jump += check_strc[c].cnt_jump;
            if (check_strc[c].pjump > max_pjump)
                max_pjump = check_strc[c].pjump;
            if (check_strc[c].njump > max_njump)
                max_njump = check_strc[c].njump;
        }
        if(udp_en)
            pthread_join(thrd_udp, NULL);

        for (int c = 0; c < N_CHECKS; c++){
            check_strc[c].data = (int *) allocated[buff_num];
            t_ret[c] = pthread_create( &thrd_id[c], NULL, th_check_errors, (void*) &check_strc[c]);
        }
        udp_pckt_str.buf = allocated[buff_num];
        if(udp_en)
            pthread_create( &thrd_udp, NULL, th_udp_send, (void*) &udp_pckt_str);

    }

    clock_gettime(CLOCK_MONOTONIC, &ts_end);
    //printf("\n\t\t\t\t\t\tE\r");
    printf("\nEND Status reg: 0x%08X\n", read_status_reg(map_base));
    write_control_reg(map_base, 0);

    for (int c = 0; c < N_CHECKS; c++)
        pthread_join(thrd_id[c], NULL);

    if(udp_en){
        pthread_join(thrd_udp, NULL);
        printf("UDP Sent %lu MB\n", udp_pckt_str.sent >> 20);
    }
    //th_udp_send ( &udp_pckt_str);
    /*sent_bytes = udp_pckt_str.sent;
    if(sent_bytes == -1){
        perror("Send error");
    }
    else
    */
    /* subtract the start time from the end time */
    timespec_sub(&ts_end, &ts_start);
    total_time= 1e9*ts_end.tv_sec + ts_end.tv_nsec;
    //printf("CLOCK_MONOTONIC %ld.%ld sec., total %ld MB, packet size: %ld kB\n",
    printf("Total Time %ld.%ld s, total %ld MB, packet size: %ld kB\n",
            ts_end.tv_sec, ts_end.tv_nsec, bytes_done/1024/1024, dma_size/1024);
    printf("Average BW = %.2f MB/s\n", bytes_done*1000.0/((float)(total_time)));
    //    printf("Count misses: %d, First M Pckt: %d, Last Pckt: %d last_cnt %d, 0x%08X, Fixed:%d\n", cnt_misses, first_m_pckt, nrpk, last_cnt, last_cnt, cnt_fixed);

    printf("Total Fixed:%lu, max_pjump:%d, max_njump:%d, cnt_jump:%lu, miss_count:%lu\n",
            total_fix, max_pjump >> 14, max_njump >> 14 , cnt_jump, miss_count);
    out_fd = fopen(ofname, "wb");
    if (out_fd == NULL) {
        fprintf(stderr, "unable to open output file %s, %d.\n",
                ofname, out_fd);
        perror("open output file");
        rc = -EINVAL;
        FATAL;
    }
    else{
        fwrite(allocated[buff_num], 1, dma_size, out_fd);
        fclose(out_fd);
    }
out:
    close(fd_bar_0);
    close(fd_bar1_c2h_0);
    if(fd_bar1_c2h_1)
        close(fd_bar1_c2h_1);
    for (int i = 0; i < NUM_BUFFS; i++)
        if (allocated[i])
            free(allocated[i]);
    
    //    if (time_array)
    //	free(time_array);
    return rc;
}
