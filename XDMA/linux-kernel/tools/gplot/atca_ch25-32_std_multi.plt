# Plot the first 8 channels of ATCA-MIMO-ISOL
#set terminal "jpeg"
#set output "/afs/ipp/u/mmz/ATCA-MIMO-ISOL-plot_01.png"
#set output "/home/mmz/ATCA-MIMO-ISOL-plot_01.jpeg"
#set term png enhanced font '/usr/share/fonts/truetype/liberation/LiberationSans-Regular.ttf' 12
#set output 'kian/atca_01-08-chop.png'
#set multiplot layout 1,1 columnsfirst
set multiplot 
#set key outside
set xrange [0:100000]                
#set xrange [0:1199]                
#set xrange [0:50]                
#set xrange [0:199]                
#set x2range [0:599]                
#set x2range [0:99999]                
#set x2range [0:99]                
#set x2range [0:25]                
#set xtics  0,200,1199
#set yrange [-1500:1500] 
set yrange [-32768:32767] 
#set yrange [-40000:40000] 
set yrange [-15000:15000] 
#set yrange [-24000:24000] 
#set yrange [-6000:6000] 
#set yrange [-10000:10000] 
#set yrange [-9000:9000] 
#set yrange [-200:200] 
#set yrange [-10:10]                    
set xlabel 'Number of Samples (2MSPS = 500 ns/sample)'
#set xlabel 'Time (microseconds)'
set ylabel 'ADC steps'
#set ylabel 'Volts'
set xtics axis
#set xtics format ""
#set x2tics axis
set ytics axis
# define line styles using explicit rgbcolor names
#
set style line 1 lt 2 lc rgb "red" lw 1
set style line 2 lt 2 lc rgb "orange" lw 1
set style line 3 lt 2 lc rgb "yellow" lw 1
set style line 4 lt 2 lc rgb "green" lw 1
set style line 5 lt 2 lc rgb "blue" lw 1
set style line 6 lt 2 lc rgb "black" lw 1
set style line 7 lt 2 lc rgb "violet" lw 1
set style line 8 lt 2 lc rgb "brown" lw 1
#
set style line 32 lt 2 lc rgb "black" lw 1

#

#set title "ATCA-MIMO-ISOL Board - 100 Hz-1.0Vpp (integrators) Channel 25-32, 27.09.2021 MMZ IPP/GAR"
set title "ATCA-MIMO-ISOL_V2 Board - 100 Hz-5.0Vpp Std ADCs  Channel 25-32, 04.10.2021 MMZ IPP/GAR"

set key spacing 5*0
dataFile='../pdata/ch25.txt'
# rot
yoff = 0
plot dataFile using ($0):($1 * 1 + yoff) title 'channel 25' with lines lt 1

set key spacing 5*1
dataFile='../pdata/ch26.txt'
# orange
yoff = 250*2
plot dataFile using ($0):($1 * 1 + yoff) title 'channel 26' with lines lt 2 

set key spacing 5*2
dataFile='../pdata/ch27.txt'
# yellow
yoff = 250*4
plot dataFile using ($0):($1 * 1 + yoff) title 'channel 27' with lines lt 3

set key spacing 5*3
dataFile='../pdata/ch28.txt'
# green
yoff = 250*6
plot dataFile using ($0):($1 * 1 + yoff) title 'channel 28' with lines lt 4

set key spacing 5*4
dataFile='../pdata/ch29.txt'
# blue
yoff = 250*8
plot dataFile using ($0):($1 * 1 + yoff) title 'channel 29' with lines lt 5

set key spacing 5*5
dataFile='../pdata/ch30.txt'
# black
yoff = 250*10
plot dataFile using ($0):($1 * 1 + yoff) title 'channel 30' with lines lt 6

set key spacing 5*6
dataFile='../pdata/ch31.txt'
# violet
yoff = 250*12
plot dataFile using ($0):($1 * 1 + yoff) title 'channel 31' with lines lt 7

set key spacing 5*7
dataFile='../pdata/ch32.txt'
# brown
yoff = 250*14
plot dataFile using ($0):($1 * 1 + yoff) title 'channel 32' with lines lt 8

unset multiplot
#set term x11
#set term wxt
#replot
#pause -1 "Hit return to continue" 
