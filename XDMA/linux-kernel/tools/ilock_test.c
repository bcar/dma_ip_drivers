/*
 * vim: set ts=8 sw=4 tw=0 et :
 * This file is part of the Xilinx DMA IP Core driver tool for Linux
 *
 * Copyright (c) 2016-present,  Xilinx, Inc.
 * All rights reserved.
 *
 * This source code is licensed under BSD-style license (found in the
 * LICENSE file in the root directory of this source tree)
 */
 
/* 
  Test program to demonstrate streaming in interlock mode 
  ADC data and Interlock data are extracted both and saved in a common file
*/

#define _POSIX_C_SOURCE 200809L
#define _DEFAULT_SOURCE
// #define _BSD_SOURCE  deprecated
#define _XOPEN_SOURCE 500
//#include <assert.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
//#include <string.h>
//#include <unistd.h>
#include <time.h>
#include <signal.h>
#include <libconfig.h>

//#include <sys/mman.h>
//#include <sys/stat.h>
#include <sys/time.h>
/*#include <sys/types.h>*/

#include "dma_utils.c"
#include "atca-v2-daq-lib.h"

/*#define DEVICE_NAME_DEFAULT "/dev/adc_xdma0_c2h_0"*/
#define DEVICE_NUM_DEFAULT 0
#define DEVICE_USER_NAME_DEFAULT "/dev/adc_xdma0_user"

#define SIZE_DEFAULT 32
#define PCKT_SIZE_32BIT 128
#define COUNT_DEFAULT 1
#define TIME_COLS  5
#define C2H1_DECIMATION 200

#define TST_SAMPLES_MAX 16384
#define TST_ARRAY_SIZE (PCKT_SIZE_32BIT*TST_SAMPLES_MAX)

//#define FATAL do { fprintf(stderr, "Error at line %d, file %s (%d) [%s]\n", __LINE__, __FILE__, errno, strerror(errno)); exit(1); } while(0)
#define FATAL do { fprintf(stderr, "Error at line %d, file %s (%d) [%s]\n", __LINE__, __FILE__, errno, strerror(errno)); goto out; } while(0)

#define MAP_SIZE (32*1024UL)

#define MAP_MASK (MAP_SIZE - 1)

static volatile int keep_running = 1;
void sig_handler(int dummy) {
    keep_running = 0;
}
void getkey ()
{
    printf("Press Return to continue ...\n");
    getchar();
}

static struct option const long_opts[] = {
    {"device", required_argument, NULL, 'd'},
    {"mask", required_argument, NULL, 'm'},
    //	{"address", required_argument, NULL, 'a'},
    //    {"aperture", required_argument, NULL, 'k'},
    {"size", required_argument, NULL, 's'},
    {"offset", required_argument, NULL, 'o'},
    {"count", required_argument, NULL, 'c'},
    {"file", required_argument, NULL, 'f'},
    {"data_mode", no_argument, NULL, 'b'},
    {"eop_flush", no_argument, NULL, 'e'},
    /*{"decimaTe", no_argument, NULL, 't'},*/
    {"chopP", required_argument, NULL, 'p'},
    {"help", no_argument, NULL, 'h'},
    {"verbose", no_argument, NULL, 'v'},
    {0, 0, 0, 0}
};

static int eop_flush = 0;
static int d_mode = 0;
static int chopp_en = 0;
static unsigned int chopper_period =0;

// Functions for interlock test
void save_to_disk_ilock(int nrOfReads, int32_t *acqData, uint64_t *counter, float *ilockData);
static int get_ilock_data (uint32_t *pSample); // get the interlock data out of raw data
static int32_t get_adc_val(uint32_t * pSamp, int offset); // get the 18-bit ADC values
static uint8_t get_ilock_F_bit (uint32_t *pSamp);
static uint8_t get_ilock_QF_bit (uint32_t *pSamp);

// Variables for interlock test
int32_t adc_data[TST_SAMPLES_MAX*32];  	   // allocate array with 18-bit adc data for 16384 samples * 32 channels
float ilock_data[TST_SAMPLES_MAX*4];   	   // allocate array with 32-bit float interlock data for 16384 samples (each sample has 4 floats as result) 
uint64_t  tst_counter[TST_SAMPLES_MAX];    // allocate array with 64-bit tst_counter values for 16384 samples (uint64_t unsigned 64-bit value) 
uint8_t chopper_phase[TST_SAMPLES_MAX];    // allocate array with 1-bit chopper phase signal bit (stored in uint8_t bytes values) 
uint8_t ilock_F_bit[TST_SAMPLES_MAX];      // allocate array with 1-bit ilock_F-bit signal (stored in uint8_t bytes values) 
uint8_t ilock_QF_bit[TST_SAMPLES_MAX];     // allocate array with 1-bit ilock_QF-bit signal (stored in uint8_t bytes values) 

static void usage(const char *name)
{
    int i = 0;
    fprintf(stdout, "%s\n\n", name);
    fprintf(stdout, "usage: %s [OPTIONS]\n\n", name);
    fprintf(stdout, "Read via SGDMA, optionally save output to a file\n\n");

    fprintf(stdout, "  -%c (--%s) device (defaults to %d)\n",
            long_opts[i].val, long_opts[i].name, DEVICE_NUM_DEFAULT);
    i++;
    /*fprintf(stdout, "  -%c (--%s) memory address aperture\n",*/
    /*long_opts[i].val, long_opts[i].name);*/
    /*i++;*/
    fprintf(stdout,
            "  -%c (--%s) size of a single transfer in bytes, default %d.\n",
            long_opts[i].val, long_opts[i].name, SIZE_DEFAULT);
    i++;
    /*fprintf(stdout, "  -%c (--%s) page offset of transfer\n",*/
    /*long_opts[i].val, long_opts[i].name);*/
    /*i++;*/
    fprintf(stdout, "  -%c (--%s) number of transfers, default is %d.\n",
            long_opts[i].val, long_opts[i].name, COUNT_DEFAULT);
    i++;
    fprintf(stdout,
            "  -%c (--%s) file to write the data of the transfers\n",
            long_opts[i].val, long_opts[i].name);
    i++;
    fprintf(stdout,
            "  -%c (--%s) end dma when ST end-of-packet(eop) is rcved\n",
            long_opts[i].val, long_opts[i].name);
    fprintf(stdout,
            "\t\t* streaming only, ignored for memory-mapped channels\n");
    fprintf(stdout,
            "\t\t* acutal # of bytes dma'ed could be smaller than specified\n");
    i++;
    fprintf(stdout, "  -%c (--%s) get 32 bit data\n",
            long_opts[i].val, long_opts[i].name);
    i++;
    fprintf(stdout, "  -%c (--%s) chopping period\n",
            long_opts[i].val, long_opts[i].name);
    i++;
    fprintf(stdout, "  -%c (--%s) print usage help and exit\n",
            long_opts[i].val, long_opts[i].name);
    i++;
    fprintf(stdout, "  -%c (--%s) verbose output\n",
            long_opts[i].val, long_opts[i].name);
    i++;

    fprintf(stdout, "\nReturn code:\n");
    fprintf(stdout, "  0: all bytes were dma'ed successfully\n");
    fprintf(stdout, "     * with -e set, the bytes dma'ed could be smaller\n");
    fprintf(stdout, "  < 0: error\n\n");
}

int main(int argc, char *argv[])
{
    int cmd_opt;
    /*char *device = DEVICE_NAME_DEFAULT;*/
    uint64_t aperture = 0;
    uint64_t size = SIZE_DEFAULT;
    //uint32_t chan_mask = 0xFFFFFFFF;
    uint32_t chan_mask = 0x00FF0055;
    /*uint64_t offset = 0;*/
    uint64_t count = COUNT_DEFAULT;
    uint64_t nsamples;
    char *ofname = NULL;
    ssize_t rc = 0;
    size_t out_offset = 0;
    size_t bytes_done = 0;
    uint64_t i, j, k; // loop variables
    char *allocated = NULL;
    struct timespec ts_start, ts_end;
    struct timespec ts_rd_start, ts_rd_end;
    FILE * out_fd;
    int fd_bar_0, fd_bar1_c2h_0;
    int fd_bar1_c2h_1 = 0;
    int  device = 0;
    long total_time = 0, t0;
    float temper[2];
    float result;
    float avg_time = 0;
    double dval=0.0;
    int underflow = 0;
    char *device_u = DEVICE_USER_NAME_DEFAULT;
    void *map_base;  // *virt_addr;
    off_t target;
    uint32_t read_result, control_reg, fw_version, val;
    uint64_t addr=0;
    void *pAdcData=NULL;
    void *pAdcDataWr = NULL;
    int samples_read;

    float float0, float1, float2, float3; // Interlock data values

    struct sample32i_s {
            volatile uint32_t channel[ADC_CHANNELS];   // 32
    } * pSample32i;

    struct sample32_s {
        volatile uint64_t channel[ADC_CHANNELS/2];  // 32/2
    } * pSample32;

    volatile void *data32Ptr;
    int32_t data0, data1, data2, data3;

    /* Configuration */ 
    struct atca_eo_config eo_config;
    struct atca_wo_config wo_config;
    struct  atca_ilck_params ilck_params;

    uint32_t shapi_ver;
    shapi_device_info * pDevice;
    shapi_module_info * pModule;
    STATUS_FLDS * pStatusBits;
    uint64_t c2hcounter, next_count;
    uint8_t chopper_bit, F_bit, QF_bit;

    /* Configuration */
    config_t cfg;
    char *cfgname = NULL;
    config_setting_t *setting;
    int   dataCfg;

    /*int   dataDecim = 0;*/
    long (*time_array)[TIME_COLS];   // mind the parenthesis!

    signal(SIGINT, sig_handler);

    while ((cmd_opt = getopt_long(argc, argv, "vheb:p:c:f:d:m:s:o:", long_opts, NULL)) != -1) {
        switch (cmd_opt) {
            case 0:
                /* long option */
                break;
            case 'd':
                device = getopt_integer(optarg);
                /* device node name */
                /*device = strdup(optarg);*/
                break;
            case 's':
                /* RAM size in bytes */
                size = getopt_integer(optarg);
                break;
            case 'o':
                cfgname = strdup(optarg);
                break;
            case 'c':
                count = getopt_integer(optarg);
                break;
                /* count */
            case 'f':
                ofname = strdup(optarg);
                break;
                /* print usage help and exit */
            case 'm':
                chan_mask = getopt_integer(optarg);
                break;
            case 'v':
                verbose = 1;
                break;
            case 'e':
                eop_flush = 1;
                break;
            case 'b':
                d_mode = getopt_integer(optarg);
                break;
            /*case 't':*/
                /*dataDecim = 1;*/
                /*break;*/
            case 'p':
                chopper_period = getopt_integer(optarg);
                // chopp_en = 1;

                break;
            case 'h':
fault:
                usage(argv[0]);
                exit(0);
                break;
        }
    }
    if (verbose)
        fprintf(stdout, "dev %s, size 0x%lx,  " "count %lu\n", device, size, count);

    time_array = malloc(sizeof(long[count][TIME_COLS])); // explicit 2D array notation

   /* Configuration */
    memset(&eo_config, 0,   sizeof(struct atca_eo_config));
    memset(&wo_config, 0,   sizeof(struct atca_wo_config));
    memset(&ilck_params, 0, sizeof(struct  atca_ilck_params));

    /* Check if config file exists */
    if (cfgname) {
        config_init(&cfg);
        if (!config_read_file(&cfg, cfgname)) {
            fprintf(stderr, "%s:%d - %s\n",  cfgname,
                    config_error_line(&cfg),
                    config_error_text(&cfg));
            config_destroy(&cfg);
            return(EXIT_FAILURE);
        }

        /*adc_eo_offset= (int *) calloc (adc_n_chans, sizeof(int));*/
        setting = config_lookup(&cfg, "adc_modules.eo_offsets");
        /*dsp_processing = {*/
        dataCfg = config_setting_length(setting);
        //  printf("data %d \n", data);

        if (dataCfg != ADC_CHANNELS) {
            printf("adc_n_chans is not the same as number of eo offset params\n");
            return(EXIT_FAILURE);
        }
        printf("*** Reading parameters from 'atca.conf' file:\n");
        printf("*** eo_offsets:\n");
        for (i = 0; i < ADC_CHANNELS; i++) {
            eo_config.offset[i] = config_setting_get_int_elem(setting, i);
            printf("\t#%d. %d\n",i, eo_config.offset[i]);
        }
        setting = config_lookup(&cfg, "adc_modules.wo_offsets");
        dataCfg = config_setting_length(setting);

        if (dataCfg != INT_CHANNELS) {
            printf("int_chans is not the same as number of wo offset params\n");
            return(EXIT_FAILURE);
        }
        printf("*** wo_offsets:\n");
        for (i = 0; i < INT_CHANNELS; i++) {
            wo_config.offset[i] = config_setting_get_float_elem(setting, i);
            printf("\t#%d. %f\n",i, wo_config.offset[i]);
        }
        setting = config_lookup(&cfg, "dsp_processing.chann_coeff");
        dataCfg = config_setting_length(setting);

        if (dataCfg != INT_CHANNELS) {
            printf("int_chans is not the same as number of dsp_processing.dsp_coeff params\n");
            return(EXIT_FAILURE);
        }
        printf("*** ilck_params:\n");
        for (i = 0; i < INT_CHANNELS; i++) {
            ilck_params.param[i] = config_setting_get_float_elem(setting, i);
            printf("\t#%d. %f\n",i, ilck_params.param[i]);
        }

        setting = config_lookup(&cfg, "dsp_processing.adder_coeff");
        dataCfg = config_setting_length(setting);
        printf("dataCfg %d \n", dataCfg);
        if (dataCfg != 2) {
            printf("2 is not the same as number of dsp_processing.adder_coeffs params\n");
            return(EXIT_FAILURE);
        }
        for (i = 0; i < 2; i++) {
            ilck_params.param[INT_CHANNELS + i] =
                config_setting_get_float_elem(setting, i);
            /*printf("\t#%d. %f\n",i, ilck_params.param[i]);*/
        }
    }

   /* allocate memory for storing data */
    if (ofname) {
        pAdcData =  malloc( count * size); /* cmd line parameter c (number of packets) and s (packet size) */
        pAdcDataWr  = pAdcData;
        printf("##count= %d ##size = %d\n", count, size);
    }

    posix_memalign((void **) &allocated, 4096 /*alignment */ , size + 4096);
    if (!allocated) {
        fprintf(stderr, "OOM %lu.\n", size + 4096);
        rc = -ENOMEM;
        fflush(stdout);
        goto out;
    }
    map_base = atca_init_device(device, chopper_period, &fd_bar_0, &fd_bar1_c2h_0);
    if (map_base == (void *)-1){
        printf("ATCA Device %d not opened\n", device);
        FATAL;
    }
    if(d_mode == 2){
        /*if (dataDecim){*/
        if((fd_bar1_c2h_1 = atca_open_c2h_1(device)) < 0){
            printf("ATCA Device %d C2H_1 not opened\n", device);
            FATAL;
        }
    }
    printf("Memory mapped at address %p.\n", map_base);
    fflush(stdout);
    printf ("*** TST_ARRAY_SIZE = %d\n", TST_ARRAY_SIZE);

    pDevice = (shapi_device_info *) map_base;
    shapi_ver =  pDevice->SHAPI_VERSION;
    if (shapi_ver == -1) {
        FATAL;
    }
    printf("SHAPI VER: 0x%X\t", shapi_ver);
    pModule = (shapi_module_info *) (map_base + pDevice->SHAPI_FIRST_MODULE_ADDRESS);
    pStatusBits = (STATUS_FLDS *) & pModule->atca_status;
    printf("SHAPI MOD VER: 0x%X\n", pModule->SHAPI_VERSION);
    printf("i2c_reg0: 0x%08X, i2c_reg1: 0x%08X\n", pModule->i2c_reg0, pModule->i2c_reg1);
    printf("STA: 0x%X\n", pModule->atca_status);
    printf("CTR: 0x%X\n", pModule->atca_control);
    printf("CHO: 0x%.8X\n", pModule->atca_chop_period);
    write_control_reg(map_base, 0);
    usleep(10);
    /*control_reg = (1<<ACQ_EN_BIT);//0x00800000; // acq enab + dma16bit*/

   /* 32-bit mode */
    if (d_mode == 1) {
        atca_use_data32(map_base, 1);
        /*printf("CTR Use: 0x%08X\n", pModule->atca_control);*/
        printf("32-bit mode = %d\n", d_mode);
        fflush(stdout);
    }
    else
        atca_use_data32(map_base, 0);  /* 16-bit mode now */

    /*control_reg |= (1<<DMA_DATA32_BIT);*/
    if(chopper_period > 0) {
        enable_chopping(map_base);
        /*control_reg |= (1<<CHOP_EN_BIT);*/
        write_chopp_period(map_base, chopper_period);
    }
    /*control_reg |= (1<<DMA_DATA32_BIT);*/
    /*write_control_reg(map_base, control_reg);*/
    printf("Control reg: 0x%08X\n", read_control_reg(map_base));

    //#define WO_SAMPL 256000
    /*wo_config.offset[0]=  (-3.10e6f/ WO_SAMPL );*/


    /*ilck_params.param[4]= 7.5544e-05; //1.0;// -1.1f;*/
    /*ilck_params.param[0]= 1.0f;*/
    /*
    */
    /*ilck_params.param[9]= 0.0;//10000.0f; // "dF" subtract term*/

    write_eo_offsets(map_base, &eo_config);
    write_wo_offsets(map_base, &wo_config);
    write_ilck_params(map_base, &ilck_params);
    printf("EO 0: %d\n", pModule->atca_eo_offsets[0]);
    printf("EO 24: %d\n", pModule->atca_eo_offsets[24]);
    /*printf("FW_TS: %ld\n", read_fw_timestamp(map_base));*/
    printf("%s\n", read_fw_timestamp_str(map_base));
    fw_version = read_fw_version(map_base);
    printf("FW Version: 0x%.8X, MAJOR: %d, MINOR: %d, PATCH:%d\n", fw_version,
            (fw_version & 0xFF000000)>>24, (fw_version & 0x00FF0000)>>16,
            (fw_version & 0xFFFF) );
    //     printf("FW Timestamp: %u\n", read_fpga_reg(map_base, TIME_STAMP_REG_OFF));
    printf("Status reg: 0x%08X, (expected 0x007E0045, for IPFN ATCA crate, top slot )\n", read_status_reg(map_base));
    printf("StatBits SLOT:%d TE741_LCK:%d Master:%d\n", pStatusBits->SLOT_ID, pStatusBits->TE741_LCK, pStatusBits->MASTER);
    usleep(10);
    printf("MAX : 0x%X\n", pModule->atca_max_dma_size);
    /*printf("TLP : 0x%X\n", pModule->atca_max_tlp_size);*/
    /*printf("EO 1: %d\n", pModule->atca_eo_offsets[1]);*/

    printf("I2C_REG_0: 0x%08X\n", read_fpga_reg(map_base, I2C_REG_0_OFF));
    rc = read_fpga_reg(map_base, TMP101_0_1_OFF);
    printf("TMP101: 0x%08X\t", rc);
    get_tmp101_vals (map_base, temper);
    printf("0: %5.2f, 1: %5.2f\n", temper[0], temper[1]);
    printf("MCP9808: 0x%08X\t", rc);
    rc =get_mca9808_vals(map_base, 0, temper);
    printf("0: %5.2f, 1: %5.2f, ", temper[0], temper[1]);
    rc =get_mca9808_vals(map_base, 1, temper);
    printf("2: %5.2f, 3: %5.2f, ", temper[0], temper[1]);
    rc =get_mca9808_vals(map_base, 2, temper);
    printf("4: %5.2f, 5: %5.2f, ", temper[0], temper[1]);
    rc =get_mca9808_vals(map_base, 3, temper);
    printf("6: %5.2f, 7: %5.2f\n", temper[0], temper[1]);
    printf("I2C_REG_1: 0x%08X\n", read_fpga_reg(map_base, I2C_REG_1_OFF));

    write_channel_mask(map_base, chan_mask);
    //atca_soft_reset(map_base);
    printf("Channel Mask: 0x%08X\n", read_channel_mask(map_base));

    /*buffer = allocated */
    if (verbose)
        fprintf(stdout, "host buffer size 0x%lx, @%p.\n", size + 4096, allocated);

    /* Arm ATCA ADC board */
    atca_arm_acq(map_base);
    printf("ADCs armed - Control reg: 0x%08X\n", read_control_reg(map_base));
    printf("software trigger mode active\n");
    getkey(); // Press any key to start the acquisition
    atca_soft_trigger(map_base);
    rc = clock_gettime(CLOCK_MONOTONIC, &ts_start);
    int stat_error = 0;

    pSample32i = (struct sample32i_s *) allocated;

    /* Loop over number of packets */
    for (i = 0; i < count; i++) {
        if(!keep_running){
            printf("\n OUCH!, you hit Ctrl-C!\n");
            break;
       	} 
	clock_gettime(CLOCK_MONOTONIC, &ts_rd_start);

        /* read data into memory buffer */
        if(d_mode == 2)
            rc = read(fd_bar1_c2h_1, allocated, size); /* Read decimaeted ADC data from ADC board */
        else
            rc = read(fd_bar1_c2h_0, allocated, size); /* Read ADC data from ATCA board */
            {
	      printf("rc = %d - sizeof(struct sample32_s) = %d\n\n", rc, sizeof( struct sample32_s));

	      /* Extract 18-bit ADC data,  4 Interlock floats + status and timing information */
              for (j = 0; j < rc/sizeof(struct sample32i_s); j++)  // Scan samples in packet
	      {

		        //asm volatile("" ::: "memory"); // prevent reordering of stores
                	// wr_cnt= get_fifo_cnt((uint32_t *) &(pSample32i[j].channel[0]));
                	// wr_data_count_max_tmp = (wr_cnt > wr_data_count_max_tmp)? wr_cnt :wr_data_count_max_tmp;
                	// c2hcounter = get_counter_64bit((uint32_t * ) &(pSample32[j].channel[0]));
			//printf("float3a: %.3f float3b: %.3f float4a: %.3f float4b: %.3f\n", float0, float1, float2, float3);
                	//get_ilock_data ((uint32_t * ) data32Ptr); //get_ilock_data ((uint32_t * ) &(pSample32i[j].channel[0]));
		        /*
			data0 = get_adc_val((int32_t*)data32Ptr,0);
			data1 = get_adc_val((int32_t*)data32Ptr,1);
			data2 = get_adc_val((int32_t*)data32Ptr,2);
			data3 = get_adc_val((int32_t*)data32Ptr,3);
			printf("data0: %-06d data1: %-06d data2: %-06d data3: %-06d\n", data0, data1, data2, data3);
			printf("data0: %-08x data1: %-08x data2: %-08x data3: %-08x\n", data0, data1, data2, data3);
			*/

                	data32Ptr = &(pSample32i[j].channel[0]); // Update pointer to current sample
			
                	// Extract 64-bit counter
                	c2hcounter = get_counter_64bit((uint32_t * ) data32Ptr); // Extract 64-bit counter
			// Store counter value in allocated array
			tst_counter [j] = c2hcounter; // Store counter value in allocated array
			
                	// Extract the 4 Interlock floats (3a, 3b, 4a, 4b) 
                	float0 = get_float_val((uint32_t * ) data32Ptr, 8);
                	float1 = get_float_val((uint32_t * ) data32Ptr, 12);
                	float2 = get_float_val((uint32_t * ) data32Ptr, 16);
                	float3 = get_float_val((uint32_t * ) data32Ptr, 20);
			// Store Interlock floats in allocated array
			ilock_data [j*4 + 0] = float0;
			ilock_data [j*4 + 1] = float1;
			ilock_data [j*4 + 2] = float2;
			ilock_data [j*4 + 3] = float3;
			if (j > 16380) {
				printf("pAddr1 = %p sample# %-06d c2hcounter = %-08d\n",data32Ptr, j, c2hcounter);
				printf("float3a: %.3f float3b: %.3f float4a: %.3f float4b: %.3f\n", float0, float1, float2, float3);
			}	
                	// Extract the  the ADC values for 32 channels and store in allocated array adc_data[]
              		for (k = 0; k < 32; k++)  // Scan adc samples in packet
			{
				adc_data[j*32 + k] = get_adc_val((int32_t*)data32Ptr,k);
			}
			/*
                	float0 = get_float_val((uint32_t * ) &(pSample32i[j].channel[0]), 8);
                	float1 = get_float_val((uint32_t * ) &(pSample32i[j].channel[0]), 12);
                	float2 = get_float_val((uint32_t * ) &(pSample32i[j].channel[0]), 16);
                	float3 = get_float_val((uint32_t * ) &(pSample32i[j].channel[0]), 20);
			printf("sample# %-06d pAddr = %p\n",j, &(pSample32i[j].channel[0]));
			printf("float3a: %.3f, float3b: %.3f float4a: %.3f, float4b: %.3f\n", float0, float1, float2, float3);
			*/
			//
			// printf("sample# %-06d floats %g,  %g, %g, %g\n", float0, float1, float2, float3);
			//
                	// Extract Chopper phase signal
                	chopper_bit = get_chopper_phase((uint32_t*) data32Ptr); // Extract 64-bit counter
			// Store chooper phase signal bit value in allocated array
			chopper_phase [j] = chopper_bit; 
                	// Extract Interlock_F-bit 
                	F_bit = get_ilock_F_bit ((uint32_t*) data32Ptr); 
			// Store interlock F-bit value in allocated array
			ilock_F_bit [j] = F_bit; 
                	// Extract Interlock_QF-bit 
                	QF_bit = get_ilock_QF_bit ((uint32_t*) data32Ptr); 
			// Store interlock F-bit value in allocated array
			ilock_QF_bit [j] = QF_bit; 

			/*	
                	if(c2hcounter != next_count ) {
                    	    if(i > 0) {
                       	 	printf("\ncount: %d, rc: %d, samp: %d, counter64 %u, counter64b c2h:%u, fifo_cnt:%u\n", i, rc, j,
                         	c2hcounter, next_count, wr_cnt);

                        	if(!first_miss)
                            		first_miss = i;
                        	missed_bursts++;
                        	missed_count = (c2hcounter - next_count);
                        	missed_samples += missed_count;
                        	if(missed_count > max_burst)
                            	max_burst = missed_count;
                        	if(missed_count > rc/sizeof(struct sample32_s))
                            	missed_packets++;
                    	   }
                           next_count = c2hcounter + 1;
                	}
                	else
                    	   next_count++;
		 	*/
               }
	       samples_read = (int) j;
	       printf ("*** loop var j = %ld\n", j);
            }


        if (rc < 0) {
            printf("Read error. Out rc = %d\n", rc);
            keep_running = 0;
            /*goto out;*/
        }
// after read
        clock_gettime(CLOCK_MONOTONIC, &ts_rd_end);
        if (rc < size)
            underflow++;
        timespec_sub(&ts_rd_end, &ts_rd_start);
        time_array[i][0] = 1e9*ts_rd_end.tv_sec + ts_rd_end.tv_nsec;
        if (ofname) {
            memcpy(pAdcDataWr, allocated, rc);
            pAdcDataWr += rc;
        }
// after memcpy
        clock_gettime(CLOCK_MONOTONIC, &ts_rd_end);
        timespec_sub(&ts_rd_end, &ts_rd_start);
        time_array[i][1] = 1e9*ts_rd_end.tv_sec + ts_rd_end.tv_nsec;

        if(d_mode == 1) /* We are in 32-bit mode for Interlock */
        {
            c2hcounter = get_counter_64bit( (uint32_t * ) (allocated + size - 16 * 8 ));
            if( (i > 0) && (c2hcounter != next_count))
                printf("Missed samples at pck: %ld c2hcounter: 0x%08X, %d\n", i, c2hcounter, c2hcounter);
            next_count = c2hcounter + size /  ADC_CHANNELS / sizeof(int);
            result += get_float_val((uint32_t *)(allocated + size - 2*PCKT_SIZE_32BIT), 12 );
        }

        bytes_done += rc;
        clock_gettime(CLOCK_MONOTONIC, &ts_rd_end);
        timespec_sub(&ts_rd_end, &ts_rd_start);
        rc = read_status_reg(map_base);
        time_array[i][2] = 1e9*ts_rd_end.tv_sec + ts_rd_end.tv_nsec;
        if((rc & OVERFLOW_MASK ) && (!stat_error) && (d_mode != 2 )) {   // Check  bits 10-15 for errors
            printf("Status Error at pckt %d\n", i);
            stat_error = -1;
        }
        //        if (!underflow && bytes_done < size)
        //            underflow = 1;
        //
        /* a bit less accurate but side-effects are accounted for */
        if (verbose){
            fprintf(stdout,
                    "#%lu: CLOCK_MONOTONIC %ld.%09ld sec. read %ld/%ld bytes\n",
                    i, ts_end.tv_sec, ts_end.tv_nsec, bytes_done, size);
        }
    } /* end for loop over packets */

    clock_gettime(CLOCK_MONOTONIC, &ts_end);
    printf("\nEND Status reg: 0x%08X\n", read_status_reg(map_base));
    write_control_reg(map_base, 0);
    nsamples = (size * count * C2H1_DECIMATION) / PCKT_SIZE_32BIT;
    if(d_mode == 2){
        /*if (dataDecim) {*/
        printf("Nsamples: %ld ", nsamples);
        printf("LDvals: ");
        for(i = 0; i < INT_CHANNELS; i++) {
            dval = get_c2h1_double_val((uint32_t *)(allocated + size - 2*PCKT_SIZE_32BIT), 12 + i*2);
            printf("%lf ", dval);
        }
        printf("\n");
        for(i = 0; i < INT_CHANNELS; i++){
            printf("%lg, ", get_c2h1_double_val((uint32_t *)(allocated + size - 2*PCKT_SIZE_32BIT),
                        12 + i*2) / nsamples);
        }
        printf("\n");
    }
    /* subtract the start time from the end time */
    timespec_sub(&ts_end, &ts_start);
    total_time= 1e9*ts_end.tv_sec + ts_end.tv_nsec;
    //total_time += ts_end.tv_nsec;
    out_fd = fopen("time_data.csv", "w");
    for (i = 0; i < count; i++) {
        t0 = time_array[i][0];
        fprintf(out_fd, "%d, %d, ", t0, time_array[i][1]-t0);
        t0 = time_array[i][1];
        fprintf(out_fd, " %d\n", time_array[i][2]-t0);
    }
    fclose(out_fd);
    //    map_basmap_baseeprintf("rw read 0x%lx\n", read_result);
    if (ofname) {
        /* create file to write data to */
        out_fd = fopen(ofname, "wb");
        if (out_fd == NULL) {
            fprintf(stderr, "unable to open output file %s, %d.\n",
                    ofname, out_fd);
            perror("open output file");
            rc = -EINVAL;
            goto out;
        }
        pAdcDataWr = pAdcData;
        for (i = 0; i < count; i++) {
            fwrite(pAdcDataWr, 1, size, out_fd);
            pAdcDataWr += size;
        }
        fclose(out_fd);
    }

    if (!underflow) {
        avg_time = (float)total_time/(float)count;
        result = ((float)size)*1000/avg_time;
        if (verbose)
            printf("** Avg time  total time %ld nsec, avg_time = %f, size = %lu, BW = %f \n",
                    total_time, avg_time, size, result);
        printf(" ** Average BW = %lu, %f MB/s\n", size, result);
        rc = 0;
    } else if (eop_flush) {
        /* allow underflow with -e option */
        rc = 0;
        //   printf("Underflow %u\n", underflow);
    } else {
        rc = -EIO;
        printf("Underflow %u,  bytes_done %d\n", underflow, bytes_done);
    }
    printf("CLOCK_MONOTONIC %ld.%ld sec., total %ld MB, packet size: %ld kB\n",
            ts_end.tv_sec, ts_end.tv_nsec, bytes_done/1024/1024, size/1024);
    printf("** Average BW = %.2f MB/s\n", bytes_done*1000.0/((float)(total_time)));

    // Save interlock data as .txt files
    save_to_disk_ilock(samples_read, &adc_data[0], &tst_counter[0], &ilock_data[0]);
	
out:
    close(fd_bar_0);
    close(fd_bar1_c2h_0);
    if(fd_bar1_c2h_1)
        close(fd_bar1_c2h_1);
    /*close(fd_bar_1);*/
    if (allocated)
        free(allocated);
    if (pAdcData)
        free(pAdcData );

    if (time_array)
        free(time_array);
    return rc;
    }

static int32_t get_adc_val(uint32_t * pSamp, int offset){
	const int sign_bit = 0x00020000;
        uint32_t val = 0;
	val = ((pSamp[offset]) >> 12);
        if (val & sign_bit) {   // Check if sign-bit is set
		//printf("Sign-bit set in val2\n");
		val = val | 0xFFFC0000;  // Sign-bit extension
	}
	return (int32_t) val;
}

static int get_ilock_data (uint32_t *pSample32)
{
        //uint64_t counter64;
        float float3a;
        float float3b;
        float float4a;
        float float4b;
        //uint16_t fifoCntr;
        //uint32_t dma_fifo_status;

        //counter64 = get_counter_64bit(pSample);
        float3a = get_float_val (pSample32, 8);
        float3b = get_float_val (pSample32, 12);
        float4a = get_float_val (pSample32, 16);
        float4b = get_float_val (pSample32, 20);

        //fifoCntr = get_fifo_cnt(pSample);
        //printf("counter64 = %lld\n", counter64);
        printf("pAddr2 = %p\n", pSample32);
        printf("float3a: %.3f float3b: %.3f float4a: %.3f float4b: %.3f\n\n", float3a, float3b, float4a, float4b);

}

uint8_t get_ilock_F_bit (uint32_t *pSamp) {
	uint8_t val = 0;
	val = (pSamp[31]) & 0x40; // bit 6 (dez 64)
	if( val > 0 ) val = 1; //convert to 0 | 1 
	  else val = 0; 
	return val;
}

uint8_t get_ilock_QF_bit (uint32_t *pSamp) {
	uint8_t val = 0;
	val = (pSamp[31]) & 0x80; // bit 7 (dez. 128)
	if( val > 0 ) val = 1; //convert to 0 | 1 
	  else val = 0; 
	return val;
}


/* Save all acquired data in interlock mode in separate .txt files */

void save_to_disk_ilock(int nrOfReads, int32_t *acqData, uint64_t *counter, float *ilockData){
    FILE *stream_out;
    char file_name[40];
    char dir_name[40];
    char path_name[80];

    int numchan;
    int nrpk;
    time_t now;
    struct tm *t;

    // Build directory name based on current date and time
    now = time(NULL);
    t = localtime(&now);
    strftime(dir_name, sizeof(dir_name)-1, "ilock/%Y%m%d%H%M%S", t);

    printf("Saving data to disk ...\n");
    printf("\ndir_name: %s\n", dir_name);

    mkdir(dir_name, 0777); // Create directory
    printf("*** Step 1 Saving ADC data\n");
    // Save data for all 32 channels each in a separate file
    for (numchan = 0; numchan < NUM_CHAN; numchan++) {
	// Create complete pathname for data file
        sprintf(file_name,"/ch%2.2d.txt", numchan+1);
        strcpy(path_name, dir_name);
        strcat(path_name, file_name);
        //printf("path_name: %s\n", path_name);
        stream_out = fopen(path_name,"wt");
        // Write data of each channel  into a separate file (max 32)
	for (nrpk=0; nrpk < nrOfReads ; nrpk++) {
		//fprintf(stream_out, "%.4hd ",acqData[(nrpk*NUM_CHAN)+numchan]);
		fprintf(stream_out, "%d ",acqData[(nrpk*NUM_CHAN)+numchan]);
	        fprintf(stream_out, "\n");
	}
	//printf("%d 20-bit data values for channel %d saved\n", nrOfReads, numchan+1);
	
    }
    printf("%d 20-bit data values for each of the 32 channels saved\n", nrOfReads);
    
    // Save 64-bit counter value for all samples in separate file
    // Create complete pathname for data file
    printf("*** Step 2 Saving 64-bit Counter\n");
    sprintf(file_name,"/64-bitCounter.txt");
    strcpy(path_name, dir_name);
    strcat(path_name, file_name);
    //printf("path_name: %s\n", path_name);
    stream_out = fopen(path_name,"wt");
    // Write counter data into file 
    for (nrpk=0; nrpk < nrOfReads ; nrpk++) {
	fprintf(stream_out, "%.4hd ",tst_counter[nrpk]);
        fprintf(stream_out, "\n");
    }
    printf("%d 64-bit counter values saved\n",nrOfReads);

    printf("*** Step 3 Saving Interlock real-time data \n");
    // Save data for all 4 Interlock values each in a separate file 
    for (numchan = 0; numchan < 4; numchan++) {
	// Create complete pathname for data file
        sprintf(file_name,"/floatvar%2.2d.txt", numchan+1);
        strcpy(path_name, dir_name);
        strcat(path_name, file_name);
        //printf("path_name: %s\n", path_name);
        stream_out = fopen(path_name,"wt");
        // Write data of each channel  into a separate file (max 32)
	for (nrpk=0; nrpk < nrOfReads ; nrpk++) {
		fprintf(stream_out, "%.4hf ",ilockData[(nrpk*4)+numchan]);
	        fprintf(stream_out, "\n");
	}
	printf("%d Interlock data for float%d saved\n", nrOfReads, numchan+1);
	
    }
    // Save 1-bit "Interlock F & QF" values for all samples in separate file
    // Create complete pathname for data file
    printf("*** Step 4 saving 1-bit Interlock-F & QF outputs (RTM signals from output 6 & 7)\n");
    sprintf(file_name,"/ilock-F.txt");
    strcpy(path_name, dir_name);
    strcat(path_name, file_name);
    //printf("path_name: %s\n", path_name);
    stream_out = fopen(path_name,"wt");
    // Write F-bit data into file 
    for (nrpk=0; nrpk < nrOfReads ; nrpk++) {
	fprintf(stream_out, "%u ",ilock_F_bit[nrpk]);
        fprintf(stream_out, "\n");
    }
    printf("%d Interlock_F-bit values saved\n",nrOfReads);

    sprintf(file_name,"/ilock-QF.txt");
    strcpy(path_name, dir_name);
    strcat(path_name, file_name);
    //printf("path_name: %s\n", path_name);
    stream_out = fopen(path_name,"wt");
    // Write F-bit data into file 
    for (nrpk=0; nrpk < nrOfReads ; nrpk++) {
	fprintf(stream_out, "%u ",ilock_QF_bit[nrpk]);
        fprintf(stream_out, "\n");
    }
    printf("%d Interlock_QF-bit values saved\n",nrOfReads);
    //
    // Save 1-bit "Chopper phase" signal bit values for all samples in separate file
    // Create complete pathname for data file
    printf("*** Step 5 saving 1-bit chopper phase signal (RTM signals from output 6)\n");
    sprintf(file_name,"/chopper_phase.txt");
    strcpy(path_name, dir_name);
    strcat(path_name, file_name);
    //printf("path_name: %s\n", path_name);
    stream_out = fopen(path_name,"wt");
    // Write F-bit data into file 
    for (nrpk=0; nrpk < nrOfReads ; nrpk++) {
	fprintf(stream_out, "%u ",chopper_phase[nrpk]);
        fprintf(stream_out, "\n");
    }

   fflush(stream_out);
   fclose(stream_out);
   printf("Saving data to disk finished\n");

}
