/*
 * @file test_dma_c2h.c
 * @brief  Test application to check for lost packets
 * @author Bernardo Carvalho / modified (cleaned) by M. Zilker
 * @date 01/06/2021
 *
 * @copyright Copyright 2016 - 2021 IPFN-Instituto Superior Tecnico, Portugal
 * Licensed under the EUPL, Version 1.2 only (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence, available in 23 official languages of
 * the European Union, at:
 * https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12
 *
 * @warning Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 *
 * @details
 * 
 * Definitions for the Linux Device Driver
 */

#define _POSIX_C_SOURCE 200809L
#define _DEFAULT_SOURCE
#define _XOPEN_SOURCE 500

//#include <assert.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <signal.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <limits.h>
#include "dma_utils.c"
#include "atca-v2-daq.h"
#include "atca-v2-daq-lib.h"

#define DEVICE_NAME_DEFAULT "/dev/adc_xdma0_c2h_0"
#define DEVICE_USER_NAME_DEFAULT "/dev/adc_xdma0_user"

#define SIZE_DEFAULT (32)
#define COUNT_DEFAULT (1)

#define FATAL do { fprintf(stderr, "Error at line %d, file %s (%d) [%s]\n", __LINE__, __FILE__, errno, strerror(errno)); exit(1); } while(0)

#define MAP_SIZE (32*1024UL)

#define MAP_MASK (MAP_SIZE - 1)

static struct option const long_opts[] = {
    {"device", required_argument, NULL, 'd'},
    {"size", required_argument, NULL, 's'},
    {"count", required_argument, NULL, 'c'},
    {"file", required_argument, NULL, 'f'},
    {"eop_flush", no_argument, NULL, 'e'},
    {"data_32b", no_argument, NULL, 'b'},
    {"help", no_argument, NULL, 'h'},
    {"verbose", no_argument, NULL, 'v'},
    {0, 0, 0, 0}
};

static int test_dma(int device, uint64_t aperture, uint64_t size, uint64_t offset, uint64_t count, char *ofname);

static int get_ilock_data (uint32_t *pSample); // get the interlock data out of raw data


static int eop_flush = 0;
static int d_32bit = 0;  // We are in 16-bit mode as default
static volatile int keepRunning = 1;

uint32_t chan_mask;

void intHandler(int dummy) { keepRunning = 0; }

static void usage(const char *name)
{
    int i = 0;
    fprintf(stdout, "%s\n\n", name);
    fprintf(stdout, "usage: %s [OPTIONS]\n\n", name);
    fprintf(stdout, "Read via SGDMA, optionally save output to a file\n\n");

    fprintf(stdout, "  -%c (--%s) device (defaults to %s)\n", long_opts[i].val, long_opts[i].name, DEVICE_NAME_DEFAULT);
    i++;
    /*fprintf(stdout, "  -%c (--%s) memory address aperture\n",*/
            /*long_opts[i].val, long_opts[i].name);*/
    /*i++;*/
    fprintf(stdout, "  -%c (--%s) size of a single transfer in bytes, default %d.\n", long_opts[i].val, long_opts[i].name, SIZE_DEFAULT);
    i++;
    /*fprintf(stdout, "  -%c (--%s) page offset of transfer\n",*/
            /*long_opts[i].val, long_opts[i].name);*/
    /*i++;*/
    fprintf(stdout, "  -%c (--%s) number of transfers, default is %d.\n", long_opts[i].val, long_opts[i].name, COUNT_DEFAULT);
    i++;
    fprintf(stdout, "  -%c (--%s) file to write the data of the transfers\n", long_opts[i].val, long_opts[i].name);
    i++;
    fprintf(stdout, "  -%c (--%s) end dma when ST end-of-packet(eop) is rcved\n", long_opts[i].val, long_opts[i].name);
    fprintf(stdout, "\t\t* streaming only, ignored for memory-mapped channels\n");
    fprintf(stdout, "\t\t* acutal # of bytes dma'ed could be smaller than specified\n");
    i++;
    fprintf(stdout, "  -%c (--%s) get 32 bit data\n", long_opts[i].val, long_opts[i].name);
    i++;
    fprintf(stdout, "  -%c (--%s) print usage help and exit\n", long_opts[i].val, long_opts[i].name);
    i++;
    fprintf(stdout, "  -%c (--%s) verbose output\n", long_opts[i].val, long_opts[i].name);
    i++;

    fprintf(stdout, "\nReturn code:\n");
    fprintf(stdout, "  0: all bytes were dma'ed successfully\n");
    fprintf(stdout, "     * with -e set, the bytes dma'ed could be smaller\n");
    fprintf(stdout, "  < 0: error\n\n");
}

int main(int argc, char *argv[])
{
    int cmd_opt;
    /*char *device = DEVICE_NAME_DEFAULT;*/
    uint64_t aperture = 0;
    uint64_t size = SIZE_DEFAULT;  // (32) 
    uint64_t offset = 0;
    uint64_t count = COUNT_DEFAULT; // (1)
    char *ofname = NULL;
    int  device = 0;

    while ((cmd_opt = getopt_long(argc, argv, "vhebc:f:d:s:o:", long_opts, NULL)) != -1) {

        switch (cmd_opt) {
            case 0:
                /* long option */
                break;
            case 'd':
                device = getopt_integer(optarg);
                /* device node name */
                /*device = strdup(optarg);*/
                break;
            /*case 'k':*/
                /*[> memory aperture windows size <]*/
                /*aperture = getopt_integer(optarg);*/
                /*break;*/

            case 's':
                /* RAM size in bytes */
                size = getopt_integer(optarg);
                break;
            case 'o':
                offset = getopt_integer(optarg) & 4095;
                break;
                /* count */
            case 'c':
                count = getopt_integer(optarg);
                break;
                /* count */
            case 'f':
                ofname = strdup(optarg);
                break;
                /* print usage help and exit */
            case 'v':
                verbose = 1;
                break;
            case 'e':
                eop_flush = 1;
                break;
            case 'b':
                d_32bit = 1;
                break;
            case 'h':
            default:
                usage(argv[0]);
                exit(0);
                break;
        }
    }
    if (verbose)
        fprintf(stdout, "dev = %s, RAMsize = 0x%lx, offset = 0x%lx, count = %lu\n", device, size, offset, count);

    return test_dma(device, 0, size, offset, count, ofname);
}

static int test_dma (int device,  uint64_t aperture, uint64_t size, uint64_t offset, uint64_t count, char *ofname)
{
    ssize_t rc = 0;
    size_t out_offset = 0;
    size_t bytes_done = 0;
    uint64_t i, j;

    //    uint64_t apt_loop = aperture ? (size + aperture - 1) / aperture : 0;

    char *buffer = NULL;
    char *allocated = NULL;
    struct timespec ts_start, ts_end;
    FILE * out_fd;
    int fpga_fd, fpga_u_fd;  // FPGA file descriptors
    long total_bytes_est, total_time = 0;
    float result, time_est;
    float avg_time = 0;
    float float0, float1, float2, float3; // Interlock data values
    int underflow = 0;
    char *device_u = DEVICE_USER_NAME_DEFAULT; // "/dev/adc_xdma0_user"
    void *map_base;  // virtual address of Fpga base register;
    off_t target;
    uint32_t read_result, control_reg;
    uint64_t addr=0;
    //    int32_t *pAdcData=NULL;
    //    int32_t *pAdcDataWr = NULL;
    struct atca_eo_config eo_config[ADC_CHANNELS];
    struct atca_wo_config wo_config[INT_CHANNELS];

    struct atca_ilck_params ilck_params[N_ILOCK_PARAMS];

    int nerr = 0;

    struct sample32_s {
        volatile uint64_t channel[ADC_CHANNELS/2];  // 32/2
    } * pSample32;

    struct sample16_s {
         volatile uint64_t channel[ADC_CHANNELS/4]; // 32/4
    } * pSample16;

    struct sample32i_s {
        volatile uint32_t channel[ADC_CHANNELS];   // 32
    } * pSample32i;

    struct sample16s_s {
         volatile uint16_t channel[ADC_CHANNELS];  // 32
    } * pSample16s;

    unsigned long missed_bursts =0;
    unsigned long max_burst =0;
    unsigned long missed_samples=0;
    unsigned long missed_packets=0;
    unsigned long missed_count;
    unsigned long first_miss=0;
    unsigned long min_rc= UINT_MAX;
    uint64_t next_count=0;
    uint64_t c2hcounter;
    uint16_t  wr_cnt, wr_data_count_max=0, wr_data_count_max_tmp=0;
    int last_sample, sec, lastsec;

    signal(SIGINT, intHandler);

  
    /* use O_TRUNC to indicate to the driver to flush the data up based on  EOP (end-of-packet), streaming mode only 
    if (eop_flush)
        fpga_fd = open(devname, O_RDWR | O_TRUNC);   //BAR 0 (DMA) space
    else
        fpga_fd = open(devname, O_RDWR);

    if (fpga_fd < 0) {
        fprintf(stderr, "unable to open device %s, %d.\n", devname, fpga_fd);
        perror("open device");
        return -EINVAL;
    }

    printf("Device %s opened.\n", devname );

    if ((fpga_u_fd = open(device_u, O_RDWR | O_SYNC)) == -1) FATAL;

    printf("Device_u %s opened.\n", device_u );

    fflush(stdout);
*/
    map_base = atca_init_device(device, 2000, &fpga_u_fd, &fpga_fd);
    /* allocate page aligned user memory for acquired data */
    posix_memalign((void **)&allocated, 4096 /*alignment */ , size + 4096);
    if (!allocated) {
        fprintf(stderr, "OOM %lu.\n", size + 4096);
        rc = -ENOMEM;
        goto out;
    }
    total_bytes_est= size*count;

    printf("d_32bit = %d\n", d_32bit);

    if(d_32bit)
        time_est=total_bytes_est/256.0/1024/1024;
    else
        time_est=total_bytes_est/128.0/1024/1024;

    printf("Buff: %ukB, Total est.Bytes: %uMB, time_est: %0.3f s\n", size/1024,  total_bytes_est/1024/1024, time_est);

    /* map one page i on BAR1 (FPGA registers )*/
    //map_base = mmap(0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fpga_u_fd, 0);
    //if (map_base == (void *)-1) FATAL;

    printf("Memory mapped at address %p.\n", map_base);
    fflush(stdout);

    chan_mask = 0xFFFFFFFF; // bit=1 set for channel: ADC data value is used; bit=0 set for channel: all data values for channel are 0 
    //chan_mask = 0x000000FF;  // only data of the first 8 channels is used - all other channels are set to zero
    write_channel_mask(map_base, chan_mask); /* Channel_mask = 0xFFFFFFFF enables all channels */
    printf("Channel Mask: 0x%08X\n", read_channel_mask(map_base));

    write_control_reg(map_base, 0);
    usleep(100);

    //control_reg = (1<<ACQ_EN_BIT);//0x00800000; // acq enab + dma16bit
    atca_arm_acq(map_base);
    enable_chopping(map_base);
//    control_reg |= (1<<CHOP_EN_BIT);

    atca_use_data32 (map_base, d_32bit); // API function to enable 32-bit mode

    /* swap 32-bit endianess if host is not little-endian no need in x86 hw*/
    control_reg = htoll(control_reg);
    //write_control_reg(map_base, control_reg);

    //    *((uint32_t *) virt_addr) = control_reg;

    printf("FW Timestamp: %u\n", read_fpga_reg(map_base, TIME_STAMP_REG_OFF));
    printf("Status reg: 0x%08X\n", read_status_reg(map_base));
    usleep(10);
    // atca_arm_acq(map_base);
    //control_reg |= (1<<STRG_BIT);// 0x01004000; // acq enab + sft_trg cjop en:
    //write_control_reg(map_base, control_reg);
    atca_soft_trigger(map_base);

    printf("Status reg: 0x%08X\n", read_status_reg(map_base));

    buffer = allocated + offset;

    pSample32 = (struct sample32_s *) buffer;
    pSample16 = (struct sample16_s *) buffer;

    pSample32i = (struct sample32i_s *) buffer;
    pSample16s = (struct sample16s_s *) buffer;

    if (verbose)
        fprintf(stdout, "host buffer 0x%lx, %p.\n", size + 4096, buffer);

    rc = clock_gettime(CLOCK_MONOTONIC, &ts_start);

    /*Flush Fifo */
    rc = read(fpga_fd, buffer, size);


    if(d_32bit){
      last_sample= rc/sizeof(struct sample32_s) -1;
      //printf("pS :%u\n", pSample32[last_sample].channel[15] + 1);//]pSample32[j].channel[15] + 1;
      next_count =pSample32[last_sample].channel[15] + 1;
    }
    else {
      last_sample= rc/sizeof(struct sample16_s) -1;
      next_count =pSample16[last_sample].channel[7] + 1;
    }

    fprintf(stderr, "Time (s)  Packet    Misses  FF_Word_cnt  Max_FIFO_WC\n");

    fflush(stdout);
    fflush(stderr);

    if(d_32bit) /* 32-bit Interlock mode */
        printf("\n*** We are in Interlock Mode now ...\n");

    for (i = 0; i < count; i++) { // Loop over all samples (128 times count)
        if(!keepRunning){
            printf("\nOUCH, you hit Ctrl-C!\n");
            break;
        }
        wr_data_count_max_tmp = 0;
        nerr=0;

        /* read data from ADC board into memory buffer */
        rc = read(fpga_fd, buffer, size);
        if (rc < 0) {
            printf("Read error. Out rc = %d\n", rc);
            continue;
            //goto out;
        }

        if (rc < size) /* too less data available */
            underflow++;

        bytes_done += rc;

    /*
	 *printf("bytes_done: %d\n", bytes_done);
     *    printf("sizeof(struct sample32_s) = %d\n",sizeof(struct sample32_s));
     */

        if(d_32bit){ /* 32-bit Interlock mode */


            for (j = 0; j < rc/sizeof(struct sample32_s); j++) { // Scan samples in packet

                //asm volatile("" ::: "memory"); // prevent reordering of stores

                wr_cnt= get_fifo_cnt((uint32_t *) &(pSample32i[j].channel[0]));
                wr_data_count_max_tmp = (wr_cnt > wr_data_count_max_tmp)? wr_cnt :wr_data_count_max_tmp;
                c2hcounter = get_counter_64bit((uint32_t * ) &(pSample32[j].channel[0]));
		/*get_ilock_data ((uint32_t * ) &(pSample32[j].channel[0]));*/
                float0 = get_float_val((uint32_t * ) &(pSample32[j].channel[0]), 8);
                float1 = get_float_val((uint32_t * ) &(pSample32[j].channel[0]), 12);
                float2 = get_float_val((uint32_t * ) &(pSample32[j].channel[0]), 16);
                float3 = get_float_val((uint32_t * ) &(pSample32[j].channel[0]), 20);
                if(c2hcounter != next_count ) {
                    if(i > 0){
                        printf("\ncount: %d, rc: %d, samp: %d, counter64 %u, counter64b c2h:%u, fifo_cnt:%u\n", i, rc, j,
		      	 c2hcounter, next_count, wr_cnt);

                        if(!first_miss)
                            first_miss = i;
                        missed_bursts++;
                        missed_count = (c2hcounter - next_count);
                        missed_samples += missed_count;
                        if(missed_count > max_burst)
                            max_burst = missed_count;
                        if(missed_count > rc/sizeof(struct sample32_s))
                            missed_packets++;
                    }
                    next_count = c2hcounter + 1;
                }
                else
                    next_count++;
            }
        }
        else { /* 16-bit streaming mode */
            for (j = 0; j < rc/sizeof(struct sample16_s); j++) {
                wr_cnt = pSample16s[j].channel[27];
                wr_data_count_max_tmp = (wr_cnt > wr_data_count_max_tmp)? wr_cnt :wr_data_count_max_tmp;
                // wr_data_count_max = (wr_cnt > wr_data_count_max)? wr_cnt :wr_data_count_max ;

                c2hcounter=pSample16[j].channel[7] ;
                if(c2hcounter != next_count) {
                    /*
                    if(nerr++ < 5){ does not work
                        printf("count: %d, rc: %d, samp: %d, counter64 c2h:%u, i:%u\n", i, rc, j,
                                c2hcounter , counter);
                    }
                    */
                    next_count =c2hcounter + 1;//]pSample32[j].channel[15] + 1;
                    if(!first_miss)
                            first_miss = i;
                    missed_bursts++;
                    //i=count+1; //break outer for loop
                    break;
                   }
                else
                    next_count++;
            }
            rc = read_status_reg(map_base);
            if((rc & 0x0000FC00 )) // Check  bits 10-15 for errors
                printf("Status Error at pckt %d\n", i);
        } // end 16-bit streaming mode

        wr_data_count_max = (wr_data_count_max_tmp > wr_data_count_max)? wr_data_count_max_tmp :wr_data_count_max ;

        clock_gettime(CLOCK_MONOTONIC, &ts_end);
        /* subtract the start time from the end time */
        timespec_sub(&ts_end, &ts_start);
        sec=ts_end.tv_sec;
        /*if(i % 201==0){ // update line print each n counts*/

        /*if(1){ // update line print each sec*/

        if(sec != lastsec){ // update line print each sec
            fprintf(stdout, "\r%08u  %-10u  %-10u %-10u %-10u", ts_end.tv_sec, i, missed_bursts, wr_data_count_max_tmp, wr_data_count_max);
            //printProgress(wr_data_count_max_tmp/32768.0);
            fflush(stdout);
            lastsec=sec;
        }
        /* a bit less accurate but side-effects are accounted for */
        if (verbose)
           fprintf(stdout, "\n#%lu: CLOCK_MONOTONIC %ld.%09ld sec. read %ld/%ld bytes\n", i, ts_end.tv_sec, ts_end.tv_nsec, bytes_done, size);

    }
    printf("\nEND Status reg: 0x%08X\n", read_status_reg(map_base));

    write_control_reg(map_base, 0);
    if(first_miss){
        printf(" first miss pck: %u, Counter:%u ", first_miss, next_count);
    }
    printf("\n");
    clock_gettime(CLOCK_MONOTONIC, &ts_end);
    /* subtract the start time from the end time */
    timespec_sub(&ts_end, &ts_start);
    total_time= 1e9*ts_end.tv_sec + ts_end.tv_nsec;

    if (ofname) {
        out_fd = fopen(ofname, "wb");
        if (out_fd == NULL) {
            fprintf(stderr, "unable to open output file %s, %d.\n",
                    ofname, out_fd);
            perror("open output file");
            rc = -EINVAL;
            goto out;
        }
        //Write last buffer
        //for (i = 0; i < count; i++) {
        fwrite(buffer, 1, size, out_fd);
        //  pAdcDataWr += size / sizeof(int32_t);
        //}
        fclose(out_fd);
    }

    if (!underflow) {
        avg_time = (float)total_time/(float)count;
        result = ((float)size)*1000/avg_time;
        if (verbose)
            printf("** Avg time total time %ld nsec, avg_time = %f, size = %lu, BW = %f \n",
                    total_time, avg_time, size, result);
        rc = 0;
    } else if (eop_flush) {
        /* allow underflow with -e option */
        rc = 0;
        //   printf("Underflow %u\n", underflow);
    } else {
        rc = -EIO;
        printf("Underflow %u,  bytes_done %d\n", underflow, bytes_done);
    }
    printf("CLOCK_MONOTONIC %ld.%ld sec., total %ld MB, packet size: %ld kB\n",
            ts_end.tv_sec, ts_end.tv_nsec, bytes_done/1024/1024, size/1024);
    printf("** Average BW = %.3f MB/s\n", bytes_done*1000.0/((float)(total_time)));
    printf("Miss packets: %d, Seq. Fails: %d, Max Sample Jump: %d, Max FF WC: %d Miss Samp Total: %d\n", 
            missed_packets, missed_bursts, max_burst, wr_data_count_max, missed_samples);
    printf("floats %g,  %g, %g, %g\n", float0, float1, float2, float3);
out:
    write_control_reg(map_base, 0);
    close(fpga_fd);
    close(fpga_u_fd);
    //if (out_fd >= 0)
    //	close(out_fd);
    free(allocated);
//    if (pAdcData)
//        free(pAdcData );

    return rc;
}

static int get_ilock_data (uint32_t *pSample)
{
	uint64_t counter64;
	float float3a;
	float float3b;
	float float4a;
	float float4b;
	uint16_t fifoCntr;
	uint32_t dma_fifo_status;
	
	counter64 = get_counter_64bit(pSample);
	float3a = get_float_val (pSample, 8);
	float3b = get_float_val (pSample, 12);
	float3a = get_float_val (pSample, 16);
	float3a = get_float_val (pSample, 20);

	fifoCntr = get_fifo_cnt(pSample);
	printf("counter64 = %lld\n", counter64);
	printf("float3a = %e\n", float3a);
	printf("float3b = %g\n", float3b);
	printf("float4a = %g\n", float4a);
	printf("float4b = %g\n", float4b);

}
