/*
 * vim: set ts=8 sw=4 tw=0 et :
 * This file is part of the Xilinx DMA IP Core driver tool for Linux
 *
 * Copyright (c) 2016-present,  IPFN-IST
 * All rights reserved.
 *
 * This source code is licensed under BSD-style license (found in the
 * LICENSE file in the root directory of this source tree)
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>

struct configuration {
  int myport;
  int backlog;
};

#define MAX_SIZE 0x10000 // 65636
int main(int argc, char **argv){
  unsigned int port =  3333;
  if(argc > 1)
	  port = atoi(argv[1]);
  /*
  if(argc != 2)
    printf("Usage : %s port\n", argv[0]);
//    puts("Usage : ./atca-upd-server port dma-size");
    exit(1);
  }
  */
  int sockfd, received_bytes; //, sended_bytes;
  char *buffer;
//  unsigned int dma_size = atoi(argv[2]);
 // , buffer_to_send[1024];
  printf("Buff size: %u\n", MAX_SIZE);
  //struct configuration conf={port, 20};
  //struct configuration *confptr;
  //confptr = &conf;

  struct sockaddr_in local_addr;
  struct sockaddr_in remote_addr;

  sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

  if(sockfd == -1){
    perror("Socket error ");
    return 1;
  }
  buffer =  malloc(MAX_SIZE);
  char tr = '1';

  if (setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&tr,sizeof(int)) == -1) {
    perror("Setsockopt ");
    exit(1);
  }


  local_addr.sin_family = AF_INET;
  local_addr.sin_port = htons(port);
  local_addr.sin_addr.s_addr = INADDR_ANY;
  memset(local_addr.sin_zero, '\0', sizeof(local_addr.sin_zero));

  int local_bind = bind(sockfd, (struct sockaddr *)&local_addr, sizeof(struct sockaddr));
  if(local_bind == -1){
    perror("Bind error ");
    return 1;
  }

  int slen = sizeof(remote_addr);
  unsigned long total_bytes = 0;
  int prints_i =0;
  while(1){
    if((received_bytes = recvfrom(sockfd, buffer, MAX_SIZE /*sizeof(buffer)*/, 0, (struct sockaddr *)&remote_addr, &slen)) == -1){
      perror("Recv ");
      exit(1);
    }

    //buffer[received_bytes] = '\0';
    total_bytes  += received_bytes;
    if ((total_bytes / 1024 /1024 ) > prints_i){
        printf("\rReceived: %10lu MB       ", prints_i++);
        fflush(stdout);
    }

  }

  free(buffer);
  return 0;

}
