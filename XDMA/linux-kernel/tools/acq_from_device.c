/*
* vim: set ts=8 sw=4 tw=0 et :
* This file is part of the Xilinx DMA IP Core driver tool for Linux
*
* Copyright (c) 2016-present,  Xilinx, Inc.
* All rights reserved.
*
* This source code is licensed under BSD-style license (found in the
* LICENSE file in the root directory of this source tree)
*/
#define _POSIX_C_SOURCE 200809L
#define _DEFAULT_SOURCE
#define _XOPEN_SOURCE 500
//#include <assert.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
//#include <string.h>
//#include <unistd.h>
#include <time.h>
#include <signal.h>
#include <libconfig.h>

#include <sys/time.h>

#include "dma_utils.c"
#include "atca-v2-daq-lib.h"

/*#define DEVICE_NAME_DEFAULT "/dev/adc_xdma0_c2h_0"*/

#define COUNT_DEFAULT 1
#define TIME_COLS  5
#define C2H1_DECIMATION 200

//#define FATAL do { fprintf(stderr, "Error at line %d, file %s (%d) [%s]\n", __LINE__, __FILE__, errno, strerror(errno)); exit(1); } while(0)
#define FATAL do { fprintf(stderr, "Error at line %d, file %s (%d) [%s]\n", __LINE__, __FILE__, errno, strerror(errno)); goto out; } while(0)

static volatile int keep_running = 1;
void sig_handler(int dummy) {
    keep_running = 0;
}

static struct option const long_opts[] = {
    {"device", required_argument, NULL, 'd'},
    //    {"aperture", required_argument, NULL, 'k'},
    {"size", required_argument, NULL, 's'},
    {"config", required_argument, NULL, 'g'},
    {"count", required_argument, NULL, 'c'},
    {"file", required_argument, NULL, 'f'},
    {"data_mode", no_argument, NULL, 'b'},
    {"eop_flush", no_argument, NULL, 'e'},
    /*{"decimaTe", no_argument, NULL, 't'},*/
    {"chopP", required_argument, NULL, 'p'},
    {"mask", required_argument, NULL, 'm'},
    {"help", no_argument, NULL, 'h'},
    {"verbose", no_argument, NULL, 'v'},
    {0, 0, 0, 0}
};

static int eop_flush = 0;
static int d_mode = DATA_MODE_DEFAULT;
static int chopp_en = 0;
static unsigned int chopper_period =0;
static void usage(const char *name)
{
    int i = 0;
    fprintf(stdout, "%s\n\n", name);
    fprintf(stdout, "usage: %s [OPTIONS]\n\n", name);
    fprintf(stdout, "Read ATCA adc data, optionally save output to a file\n\n");

    fprintf(stdout, "  -%c (--%s) <n> \t specifies device number, default is %d\n",
            long_opts[i].val, long_opts[i].name, DEVICE_NUM_DEFAULT);
    i++;
    /*fprintf(stdout, "  -%c (--%s) memory address aperture\n",*/
    /*long_opts[i].val, long_opts[i].name);*/
    /*i++;*/
    fprintf(stdout,
            "  -%c (--%s) <n> \t specifies size of a single transfer in bytes, default %d kB\n",
            long_opts[i].val, long_opts[i].name, SIZE_DEFAULT/1024);
    i++;
    fprintf(stdout,
            "  -%c (--%s) <file> \t specifies config file name\n",
            long_opts[i].val, long_opts[i].name);
    i++;
    fprintf(stdout, "  -%c (--%s) <n> \t specifies number of DMA transfers, default is %d\n",
            long_opts[i].val, long_opts[i].name, COUNT_DEFAULT);
    i++;
    fprintf(stdout,
            "  -%c (--%s) <file> \t specifies file to write the acquired data. Default is no write\n",
            long_opts[i].val, long_opts[i].name);
    i++;
    fprintf(stdout, "  -%c (--%s) <m> \t specifies data type, 0: 16-bit, 1:32-bit data. Default: %u\n",
            long_opts[i].val, long_opts[i].name, DATA_MODE_DEFAULT);
    i++;
    fprintf(stdout,
            "  -%c (--%s) \t end dma when ST end-of-packet(eop) is rcved\n",
            long_opts[i].val, long_opts[i].name);
    i++;
    fprintf(stdout, "  -%c (--%s) <n> \t enables and specifies chopping period. Default: no chopping\n",
            long_opts[i].val, long_opts[i].name);
    i++;
    fprintf(stdout, "  -%c (--%s) <hex> \t specifies channel mask. Default: 0xFFFF (all channels active)\n",
            long_opts[i].val, long_opts[i].name);
    i++;
    fprintf(stdout, "  -%c (--%s) \t print usage help and exit\n",
            long_opts[i].val, long_opts[i].name);
    i++;
    fprintf(stdout, "  -%c (--%s) \t verbose output\n",
            long_opts[i].val, long_opts[i].name);
    i++;

    fprintf(stdout, "\nExample:\n");
    fprintf(stdout, "./acq_from_device -b 1 -m 0xFF -p 2000 -s 0x400000 -c 2 -f data/out_32.bin\n");
    fprintf(stdout, "\nReturn code:\n");
    fprintf(stdout, "    0: all bytes were dma'ed successfully\n");
    fprintf(stdout, "       * with -e set, the bytes dma'ed could be smaller\n");
    fprintf(stdout, "  < 0: error\n\n");
}

int main(int argc, char *argv[])
{
    int cmd_opt;
    uint64_t aperture = 0;
    uint64_t size = SIZE_DEFAULT;
    uint32_t chan_mask = 0xFFFFFFFF;
    /*uint64_t offset = 0;*/
    uint64_t count = COUNT_DEFAULT;
    uint64_t nsamples;
    char *ofname = NULL;
    ssize_t rc = 0;
    size_t out_offset = 0;
    size_t bytes_done = 0;
    uint64_t i;
    /*char *buffer = NULL;*/
    char *allocated = NULL;
    struct timespec ts_start, ts_end;
    struct timespec ts_rd_start, ts_rd_end;
    FILE * out_fd;
    int fd_bar_0, fd_bar1_c2h_0;
    int fd_bar1_c2h_1 = 0;
    int  device = 0;
    long total_time = 0, t0;
    float temper[2];
    float result;
    float avg_time = 0;
    double dval=0.0;
    int underflow = 0;
    char *device_u = DEVICE_USER_NAME_DEFAULT;
    void *map_base;
    off_t target;
    uint32_t read_result, control_reg, fw_version, val;
    float * pfval =(float *) &val;
    uint64_t addr=0;
    void *pAdcData=NULL;
    void *pAdcDataWr = NULL;
    struct atca_eo_config eo_config;
    struct atca_wo_config wo_config;
    struct  atca_ilck_params ilck_params;
    uint32_t shapi_ver;
    shapi_device_info * pDevice;
    shapi_module_info * pModule;
    STATUS_FLDS * pStatusBits;
    uint64_t c2hcounter, next_count;
    config_t cfg;
    char *cfgname = NULL;
    config_setting_t *setting;
    int   dataCfg;
    /*int   dataDecim = 0;*/
    long (*time_array)[TIME_COLS];   // mind the parenthesis!

    signal(SIGINT, sig_handler);

    while ((cmd_opt = getopt_long(argc, argv, "vhed:p:s:g:c:f:m:b:", long_opts,
                    NULL)) != -1) {
        switch (cmd_opt) {
            case 0:
                /* long option */
                break;
            case 'd':
                device = getopt_integer(optarg);
                /*device = strdup(optarg);*/
                break;
            case 's':
                /* RAM size in bytes */
                size = getopt_integer(optarg);
                break;
            case 'g':
                cfgname = strdup(optarg);
                break;
            case 'c':
                count = getopt_integer(optarg);
                break;
            case 'f':
                ofname = strdup(optarg);
                break;
            case 'm':
                chan_mask = getopt_integer(optarg);
                break;
            case 'b':
                d_mode = getopt_integer(optarg);
                break;
            case 'e':
                eop_flush = 1;
                break;
            /*case 't':*/
                /*dataDecim = 1;*/
                /*break;*/
            case 'p':
                chopper_period = getopt_integer(optarg);
                // chopp_en = 1;

                break;
            case 'v':
                verbose = 1;
                break;
            case 'h':
                /* print usage help and exit */
            fault:
                usage(argv[0]);
                exit(0);
                break;
        }
    }
    if (verbose)
        fprintf(stdout,
                "dev %s, size 0x%lx,  "
                "count %lu\n",
                device, size, count);

    time_array = malloc(sizeof(long[count][TIME_COLS])); // explicit 2D array notation
    memset(&eo_config, 0,   sizeof(struct atca_eo_config));
    memset(&wo_config, 0,   sizeof(struct atca_wo_config));
    memset(&ilck_params, 0, sizeof(struct  atca_ilck_params));
    /*test if config file exists*/
    if (cfgname) {
        config_init(&cfg);
        if (!config_read_file(&cfg, cfgname)) {
            fprintf(stderr, "%s:%d - %s\n",  cfgname,
                    config_error_line(&cfg),
                    config_error_text(&cfg));
            config_destroy(&cfg);
            return(EXIT_FAILURE);
        }

        /*adc_eo_offset= (int *) calloc (adc_n_chans, sizeof(int));*/
        setting = config_lookup(&cfg, "adc_modules.eo_offsets");
        /*dsp_processing = {*/
        dataCfg = config_setting_length(setting);
        //  printf("data %d \n", data);

        if (dataCfg != INT_CHANNELS) {
            printf("adc_n_chans is not the same as number of eo offset params\n");
            return(EXIT_FAILURE);
        }
        for (i = 0; i < INT_CHANNELS; i++) {
            eo_config.offset[i] = config_setting_get_int_elem(setting, i);
            /*printf("\t#%d. %d\n",i, eo_config.offset[i]);*/
        }
        setting = config_lookup(&cfg, "adc_modules.wo_offsets");
        dataCfg = config_setting_length(setting);

        if (dataCfg != INT_CHANNELS) {
            printf("int_chans is not the same as number of wo offset params\n");
            return(EXIT_FAILURE);
        }
        //printf("W0: ");
        for (i = 0; i < INT_CHANNELS; i++) {
            wo_config.offset[i] = config_setting_get_float_elem(setting, i);
            //printf("\t#%d: %5.3f",i, wo_config.offset[i]);
        }
        printf("\n");
        setting = config_lookup(&cfg, "dsp_processing.chann_coeff");
        dataCfg = config_setting_length(setting);

        if (dataCfg != INT_CHANNELS) {
            printf("%u is not the same as number of dsp_processing.dsp_coeff params\n",
                    INT_CHANNELS);
            return(EXIT_FAILURE);
        }
        printf("ILCK CH: ");
        for (i = 0; i < INT_CHANNELS; i++) {
            ilck_params.fval[i] = config_setting_get_float_elem(setting, i);
            //printf("\t#%u: %5.3f",i, ilck_params.fval[i]);
        }
        printf("\n");

        setting = config_lookup(&cfg, "dsp_processing.adder_coeff");
        dataCfg = config_setting_length(setting);
        //printf("dataCfg len %d \n", dataCfg);
        if (dataCfg != 2) {
            printf("2 is not the same as number of dsp_processing.adder_coeffs params\n");
            return(EXIT_FAILURE);
        }
        for (i = 0; i < 2; i++) {
            ilck_params.fval[INT_CHANNELS + i] =
                config_setting_get_float_elem(setting, i);
            /*printf("\t#%d. %f\n",i, ilck_params.param[i]);*/
        }
    }
    if (ofname) {
        pAdcData =  malloc( count * size);
        pAdcDataWr = pAdcData;
    }

    posix_memalign((void **) &allocated, 4096 /*alignment */ , size + 4096);
    if (!allocated) {
        fprintf(stderr, "OOM %lu.\n", size + 4096);
        rc = -ENOMEM;
        fflush(stdout);
        goto out;
    }
    map_base = atca_init_device(device, chopper_period, &fd_bar_0, &fd_bar1_c2h_0);
    if (map_base == (void *)-1){
        printf("ATCA Device %d not opened\n", device);
        FATAL;
    }
    if(d_mode == DATA_MODE_DEC){
    /*if (dataDecim){*/
        if((fd_bar1_c2h_1 = atca_open_c2h_1(device)) < 0){
            printf("ATCA Device %d C2H_1 not opened\n", device);
            FATAL;
        }
    }
    //    printf("Memory mapped at address %p.\n", map_base);
    fflush(stdout);

    printf("\nWO: 0x%08X\t", val);
    printf("%5.3f\t", *pfval);
    write_fpga_reg(map_base, WO_REGS_OFF + 7*4, 0x40900000); // 4.5
    val = read_fpga_reg(map_base, WO_REGS_OFF + 7*4);
    printf("W7: 0x%08X\t", read_fpga_reg(map_base, WO_REGS_OFF + 7*4));
    printf("%5.3f\n", *pfval);
    atca_soft_reset(map_base); // Clear FPGA internal registers
    pDevice = (shapi_device_info *) map_base;
    shapi_ver =  pDevice->SHAPI_VERSION;
    if (shapi_ver == -1){
        FATAL;
    }
    printf("SHAPI VER: 0x%X\t", shapi_ver);
    pModule = (shapi_module_info *) (map_base +
            pDevice->SHAPI_FIRST_MODULE_ADDRESS);
    pStatusBits = (STATUS_FLDS *) & pModule->atca_status;
    printf("SHAPI MOD VER: 0x%08X\t", pModule->SHAPI_VERSION);
    printf("i2c_reg0: 0x%08X\n", pModule->i2c_reg0);
    /*printf("STA: 0x%X\n", pModule->atca_status);*/
    /*printf("CTR: 0x%X\n", pModule->atca_control);*/
    /*printf("CHO: 0x%.8X\n", pModule->atca_chop_period);*/
    write_control_reg(map_base, 0);
    usleep(10);
    if(d_mode == DATA_MODE_32){
        atca_use_data32(map_base, 1);
        /*printf("CTR Use: 0x%08X\n", pModule->atca_control);*/
        fflush(stdout);
    }
    else
        atca_use_data32(map_base, 0);

    if(chopper_period > 0) {
        enable_chopping(map_base);
        /*control_reg |= (1<<CHOP_EN_BIT);*/
        write_chopp_period(map_base, chopper_period);
    }
    printf("Control reg: 0x%08X\n", read_control_reg(map_base));

    //#define WO_SAMPL 256000
    /*wo_config.offset[0]=  (-3.10e6f/ WO_SAMPL );*/


    /*ilck_params.fval[4]= 7.5544e-05; //1.0;// -1.1f;*/
    /*ilck_params.param[9]= 0.0;//10000.0f; // "dF" subtract term*/
    write_eo_offsets(map_base, &eo_config);
    write_wo_offsets(map_base, &wo_config);
    write_ilck_params(map_base, &ilck_params);
    for (i = 0; i < INT_CHANNELS; i++)
        printf("E0 %u: %d\t", i, read_fpga_reg(map_base, EO_REGS_OFF + i*4));
    printf("\n");
    memset(&ilck_params, 0, sizeof(struct  atca_ilck_params));
    read_ilck_params(map_base, &ilck_params);
    for (i = 0; i < INT_CHANNELS; i++)
        printf("ILCK %u: %.3e  ", i, ilck_params.fval[i]);
    printf("\n");
    for (i = INT_CHANNELS; i < INT_CHANNELS + 2; i++)
        printf("ILCK %u: %.3e  ", i, ilck_params.fval[i]);
    printf("\n%s\n", read_fw_timestamp_str(map_base));
    /*printf("FW_TS: %ld\n", read_fw_timestamp(map_base));*/
    fw_version = read_fw_version(map_base);
    printf("FW Version: 0x%.8X, MAJOR: %d, MINOR: %d, PATCH:%d\n", fw_version,
            (fw_version & 0xFF000000)>>24, (fw_version & 0x00FF0000)>>16,
            (fw_version & 0xFFFF) );
    //     printf("FW Timestamp: %u\n", read_fpga_reg(map_base, TIME_STAMP_REG_OFF));
    printf("Status reg: 0x%08X, (expected 0x007F00CE, for IPP-HGW ATCA crate)\n", read_status_reg(map_base));
    //printf("StatBits SLOT:%d TE741_LCK:%d Master:%d\n", pStatusBits->SLOT_ID, pStatusBits->TE741_LCK, pStatusBits->MASTER);
    printf("StatBits SLOT:0x%02X  Master:%d CLK_LOCKS ATCA:%2u TE741:%2u RTM:%2u\n", pStatusBits->SLOT_ID,
		    pStatusBits->MASTER, pStatusBits->ATCA_LCK, pStatusBits->TE741_LCK, pStatusBits->RTM_LCK);
//                    pStatusBits->MAIN_LCK);
    usleep(10);
    printf("MAX : 0x%X\n", pModule->atca_max_dma_size);
    /*printf("TLP : 0x%X\n", pModule->atca_max_tlp_size);*/

//    printf("I2C_REG_0: 0x%08X\n", read_fpga_reg(map_base, I2C_REG_0_OFF));
    rc = read_fpga_reg(map_base, TMP101_0_1_OFF);
    printf("TMP101: 0x%08X\t", rc);
    get_tmp101_vals (map_base, temper);
    printf("0: %5.2f, 1: %5.2f\n", temper[0], temper[1]);
    printf("MCP9808: 0x%08X\t", rc);
    rc =get_mca9808_vals(map_base, 0, temper);
    printf("0: %5.2f, 1: %5.2f, ", temper[0], temper[1]);
    rc =get_mca9808_vals(map_base, 1, temper);
    printf("2: %5.2f, 3: %5.2f, ", temper[0], temper[1]);
    rc =get_mca9808_vals(map_base, 2, temper);
    printf("4: %5.2f, 5: %5.2f, ", temper[0], temper[1]);
    rc =get_mca9808_vals(map_base, 3, temper);
    printf("6: %5.2f, 7: %5.2f\n", temper[0], temper[1]);
//    printf("I2C_REG_1: 0x%08X\n", read_fpga_reg(map_base, I2C_REG_1_OFF));
    write_channel_mask(map_base, chan_mask);
    printf("Channel Mask: 0x%08X\n", read_channel_mask(map_base));

    /*buffer = allocated ;*/
    if (verbose)
        fprintf(stdout, "host buffer 0x%lx, %p.\n", size + 4096, allocated);

    atca_arm_acq(map_base);
    /*printf("Control reg: 0x%08X\n", read_control_reg(map_base));*/
    atca_soft_trigger(map_base);
    rc = clock_gettime(CLOCK_MONOTONIC, &ts_start);
    int stat_error = 0;
    for (i = 0; i < count; i++) {
        if(!keep_running){
            printf("\n OUCH!, you hit Ctrl-C!\n");
            break;
        }
        clock_gettime(CLOCK_MONOTONIC, &ts_rd_start);
        /* read data from file into memory buffer */
        if(d_mode == DATA_MODE_DEC)
            rc = read(fd_bar1_c2h_1, allocated, size);
        else
            rc = read(fd_bar1_c2h_0, allocated, size);
        if (rc < 0) {
            printf("Read error. Out rc = %d\n", rc);
            keep_running = 0;
        }
// after read
        clock_gettime(CLOCK_MONOTONIC, &ts_rd_end);
        if (rc < size)
            underflow++;
        timespec_sub(&ts_rd_end, &ts_rd_start);
        time_array[i][0] = 1e9*ts_rd_end.tv_sec + ts_rd_end.tv_nsec;
        if (ofname) {
            memcpy(pAdcDataWr, allocated, rc);
            pAdcDataWr += rc;
        }
// after memcpy
        clock_gettime(CLOCK_MONOTONIC, &ts_rd_end);
        timespec_sub(&ts_rd_end, &ts_rd_start);
        time_array[i][1] = 1e9*ts_rd_end.tv_sec + ts_rd_end.tv_nsec;
        if(d_mode == DATA_MODE_32){
            c2hcounter = get_counter_64bit( (uint32_t * ) (allocated + size - 16 * 8 ));
            if( (i > 0) && (c2hcounter != next_count))
                printf("Missed samples at pck: %ld c2hcounter: 0x%08X, %d\n", i, c2hcounter, c2hcounter);
            next_count = c2hcounter + size /  ADC_CHANNELS / sizeof(int);
            result += get_float_val((uint32_t *)(allocated + size - 2*PCKT_SIZE_32BIT), 12 );
        }
        bytes_done += rc;
        clock_gettime(CLOCK_MONOTONIC, &ts_rd_end);
        timespec_sub(&ts_rd_end, &ts_rd_start);
        rc = read_status_reg(map_base);
        time_array[i][2] = 1e9*ts_rd_end.tv_sec + ts_rd_end.tv_nsec;
        if((rc & OVERFLOW_MASK ) && (!stat_error) && (d_mode != DATA_MODE_DEC )) {// Check  bits 10-15 for errors
            printf("Status Error at pckt %d, 0x%X\n", i, (rc & OVERFLOW_MASK)) ;
            stat_error = -1;
        }
        //        if (!underflow && bytes_done < size)
        //            underflow = 1;
        //
        /* a bit less accurate but side-effects are accounted for */
        if (verbose){
            fprintf(stdout,
                    "#%lu: CLOCK_MONOTONIC %ld.%09ld sec. read %ld/%ld bytes\n",
                    i, ts_end.tv_sec, ts_end.tv_nsec, bytes_done, size);
        }
    }

    clock_gettime(CLOCK_MONOTONIC, &ts_end);
    printf("\nEND Status reg: 0x%08X\n", read_status_reg(map_base));
    write_control_reg(map_base, 0);
    nsamples = (size * count * C2H1_DECIMATION) / PCKT_SIZE_32BIT;
    if(d_mode == DATA_MODE_DEC){
    /*if (dataDecim) {*/
        printf("Nsamples: %ld ", nsamples);
        printf("LDvals: ");
        for(i = 0; i < INT_CHANNELS; i++) {
            dval = get_c2h1_double_val((uint32_t *)(allocated + size - 2*PCKT_SIZE_32BIT), 12 + i*2);
            printf("%lf ", dval);
        }
        printf("\n");
        for(i = 0; i < INT_CHANNELS; i++){
            printf("%lg, ", get_c2h1_double_val((uint32_t *)(allocated + size - 2*PCKT_SIZE_32BIT),
                        12 + i*2) / nsamples);
        }
        printf("\n");
    }
    /* subtract the start time from the end time */
    timespec_sub(&ts_end, &ts_start);
    total_time= 1e9*ts_end.tv_sec + ts_end.tv_nsec;
    //total_time += ts_end.tv_nsec;
    out_fd = fopen("time_data.csv", "w");
    for (i = 0; i < count; i++) {
        t0 = time_array[i][0];
        fprintf(out_fd, "%d, %d, ", t0, time_array[i][1]-t0);
        t0 = time_array[i][1];
        fprintf(out_fd, " %d\n", time_array[i][2]-t0);
    }
    fclose(out_fd);
    //    map_basmap_baseeprintf("rw read 0x%lx\n", read_result);
    if (ofname) {
        /* create file to write data to */
        out_fd = fopen(ofname, "wb");
        if (out_fd == NULL) {
            fprintf(stderr, "unable to open output file %s, %d.\n",
                    ofname, out_fd);
            perror("open output file");
            rc = -EINVAL;
            goto out;
        }
        pAdcDataWr = pAdcData;
        for (i = 0; i < count; i++) {
            fwrite(pAdcDataWr, 1, size, out_fd);
            pAdcDataWr += size;
        }
        fclose(out_fd);
    }

    if (!underflow) {
        avg_time = (float)total_time/(float)count;
        result = ((float)size)*1000/avg_time;
        if (verbose)
            printf("** Avg time  total time %ld nsec, avg_time = %f, size = %lu, BW = %f \n",
                    total_time, avg_time, size, result);
        printf(" ** Average BW = %lu, %f MB/s\n", size, result);
        rc = 0;
    } else if (eop_flush) {
        /* allow underflow with -e option */
        rc = 0;
        //   printf("Underflow %u\n", underflow);
    } else {
        rc = -EIO;
        printf("Underflow %u,  bytes_done %d\n", underflow, bytes_done);
    }
    printf("CLOCK_MONOTONIC %ld.%ld sec., total %ld MB, packet size: %ld kB\n",
            ts_end.tv_sec, ts_end.tv_nsec, bytes_done/1024/1024, size/1024);
    printf("** Average BW = %.2f MB/s\n", bytes_done*1000.0/((float)(total_time)));

out:
    close(fd_bar_0);
    close(fd_bar1_c2h_0);
    if(fd_bar1_c2h_1)
        close(fd_bar1_c2h_1);
    /*close(fd_bar_1);*/
    if (allocated)
        free(allocated);
    if (pAdcData)
        free(pAdcData );

    if (time_array)
        free(time_array);
    return rc;
    }
